#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#ifdef LP_TIMER
#include <sys/time.h>
#else /*HP_TIMER*/
#include <time.h>
#endif /*HP_TIMER*/
#include "defines.h"
#include "swkrls.h"
#include "knlms.h"
#include "fbkrls.h"
#include "aldkrls.h"

#ifdef MSE_TEST
void print_MSE(MATHTYPE_T * x, MATHTYPE_T * y, int length);
#endif

#define NUM_PARAMS 4
#define VALS_PER_PARAM 20
const MATHTYPE_T etas[2] = {1e-4, 5e-1};
const MATHTYPE_T epss[2] = {1e-6, 1e-1};
const MATHTYPE_T mu0s[2] = {0.1, 0.9};
const MATHTYPE_T sigmas[2] = {0.1, 10};

MATHTYPE_T getGridPoint(MATHTYPE_T low, MATHTYPE_T high, int i, int iMax) {
    MATHTYPE_T ratio = (MATHTYPE_T)i / (MATHTYPE_T)iMax;
    MATHTYPE_T diff = high - low;
    return low + ratio*diff;
}

//Pure C test for swkrls algorithm.
int main(int argc, char *argv[]) {
    MATHTYPE_T * sqrErr;
    int Ntrain,i;
    MATHTYPE_T *x_train;
    MATHTYPE_T total_time;
    int paramLoopSize;
    knlms_state_t *state;
    #include "mg30.h"
    #ifdef LP_TIMER
    struct timeval tic, toc;
    #else /*HP_TIMER*/
    struct timespec tic, toc;
    #endif /*HP_TIMER*/

    #ifdef MCA_ON
    MCALIB_MODE=1;//MCALIB_IEEE  //Turn off monte carlo arithmetic for function setup.
    MCALIB_T = 53;     //Default value for double precision.
    #endif

    paramLoopSize = pow(VALS_PER_PARAM, NUM_PARAMS);
    Ntrain = X_LENGTH-EMBED_LENGTH; //Find the length of the series.

    sqrErr = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*paramLoopSize);
    if (sqrErr == NULL) {
        printf("Allocation of sqrErr failed!");
        exit(0);
    }

    state = (knlms_state_t *)malloc(sizeof(knlms_state_t)*paramLoopSize);
    if (state == NULL) {
        printf("Allocation of state failed!");
        exit(0);
    }

    //Initialise the swkrls state object.
    #ifdef DYN_MEM
    for(int i = 0; i < VALS_PER_PARAM; i++) { // eta
        MATHTYPE_T eta = getGridPoint(etas[0], etas[1], i, VALS_PER_PARAM-1);
        for(int j = 0; j < VALS_PER_PARAM; j++) { // eps
            MATHTYPE_T eps = getGridPoint(epss[0], epss[1], j, VALS_PER_PARAM-1);
            for(int k = 0; k < VALS_PER_PARAM; k++) { // mu0s
                MATHTYPE_T mu0 = getGridPoint(mu0s[0], mu0s[1], k, VALS_PER_PARAM-1);
                for(int l = 0; l < VALS_PER_PARAM; l++) { // sigmas
                    MATHTYPE_T sigma = getGridPoint(sigmas[0], sigmas[1], l, VALS_PER_PARAM-1);
                    int index = l + k*VALS_PER_PARAM + j*VALS_PER_PARAM*VALS_PER_PARAM + i*VALS_PER_PARAM*VALS_PER_PARAM*VALS_PER_PARAM;
                    InitialiseKNLMSState(&state[index], WINDOW_LENGTH, EMBED_LENGTH, eta, eps, mu0, sigma);
                }
            }
        }
    }
    #else
    for(int i = 0; i < VALS_PER_PARAM; i++) { // eta
        MATHTYPE_T eta = getGridPoint(etas[0], etas[1], i, VALS_PER_PARAM-1);
        for(int j = 0; j < VALS_PER_PARAM; j++) { // eps
            MATHTYPE_T eps = getGridPoint(epss[0], epss[1], j, VALS_PER_PARAM-1);
            for(int k = 0; k < VALS_PER_PARAM; k++) { // mu0s
                MATHTYPE_T mu0 = getGridPoint(mu0s[0], mu0s[1], k, VALS_PER_PARAM-1);
                for(int l = 0; l < VALS_PER_PARAM; l++) { // sigmas
                    MATHTYPE_T sigma = getGridPoint(sigmas[0], sigmas[1], l, VALS_PER_PARAM-1);
                    int index = l + k*VALS_PER_PARAM + j*VALS_PER_PARAM*VALS_PER_PARAM + i*VALS_PER_PARAM*VALS_PER_PARAM*VALS_PER_PARAM;
                    InitialiseKNLMSState(&state[index], eta, eps, mu0, sigma);
                }
            }
        }
    }
    #endif

    #ifdef MCA_ON
        #ifdef _MP
        MCALIB_MODE=3;//MCALIB_MP
        #else
        MCALIB_MODE=2;//MCALIB_MCA
        #endif
    #endif

    #ifdef LP_TIMER
    gettimeofday(&tic, NULL);
    #else /*HP_TIMER*/
    clock_gettime(CLOCK_MONOTONIC, &tic);
    #endif /*HP_TIMER*/

    //Predict the time series using SW-KRLS with 1 step prediction.

    #pragma omp parallel for shared(state,sqrErr)
    for(int j = 0; j < paramLoopSize; j++) {
        knlms_state_t * curState = &state[j];
        MATHTYPE_T curSqrErr = 0.0;
        for(int i = 0; i < Ntrain-1; ++i) {
            //y[y_index+i+1] = knlms_ts_wrap(x[i], curState);
            knlms_train(&x[i], x[i+EMBED_LENGTH], curState);
            MATHTYPE_T err = x[i+1+EMBED_LENGTH] - knlms_predict(&x[i+1], curState);
            curSqrErr += err*err;
        }
        sqrErr[j] = curSqrErr;
    }

    #ifdef LP_TIMER
    gettimeofday(&toc, NULL);
    #else /*HP_TIMER*/
    clock_gettime(CLOCK_MONOTONIC, &toc);
    #endif /*HP_TIMER*/

    int bJ = 0;
    MATHTYPE_T bErr = 0.0;
    for (int j = 0; j < paramLoopSize; j++) {
        if (j == 0)
            bErr = sqrErr[j];
        else if (sqrErr[j] < bErr) {
            bJ = j;
            bErr = sqrErr[j];
        }
    }
    printf("Best error: %e, eta: %e, eps: %e, mu0: %e, sigma: %e\n", bErr, state[bJ].step, state[bJ].eps, state[bJ].mu, state[bJ].sigma);

    #ifdef OUTPUT_TEST
    //Print output pairs.
    printf("x, \ty, \terr\n");
    for(i = 0; i < Ntrain-1; ++i) {
        printf("%7e, \t%7e, \t%7e\n", x[i], y[i], x[i]-y[i]);
    }
    #endif

    #ifdef MSE_TEST
    print_MSE(&x[X_LENGTH-500], &y[X_LENGTH-500], 500);
    #endif

    #ifdef LP_TIMER
    total_time = ((MATHTYPE_T)(toc.tv_usec-tic.tv_usec))/((MATHTYPE_T)1000000) + (MATHTYPE_T)(toc.tv_sec-tic.tv_sec);
    #else /*HP_TIMER*/
    total_time = ((MATHTYPE_T)(toc.tv_nsec-tic.tv_nsec))/((MATHTYPE_T)1000000000) + (MATHTYPE_T)(toc.tv_sec-tic.tv_sec);
    #endif /*HP_TIMER*/
    printf("Elapsed time(s): %e. Time per prediction(s): %e.\n", total_time, total_time/((Ntrain-1)*paramLoopSize));

    #ifdef DYN_MEM
    for (int i = 0; i < paramLoopSize; i++) {
        FreeKNLMSState(&state[i]);
    }
    #endif

    free(state); // FREE STATE-O

    free(sqrErr);

    return 0;
}

#ifdef MSE_TEST
//Calculate and print Mean Squared Error.
void print_MSE(MATHTYPE_T * x, MATHTYPE_T * y, int length) {
    MATHTYPE_T N,MSE,err;
    int i;
    N = 1.0/(MATHTYPE_T)length;
    MSE = 0.0;
    for(i = 0; i < length; ++i) {
        err = x[i] - y[i];
        MSE = MSE + (err*err);
    }
    MSE = N*MSE;
    #ifdef MSE_VAL_ONLY
    printf("%.16e", MSE);
    #else
    printf("Mean Squared error: %e\n", MSE);
    #endif
}
#endif

#ifdef STATE_LAG
//Don't update K_inv until later.
void rotateState(swkrls_generic_state_t * state) {
    swkrls_generic_state_t tmp;
    int i,j;

    copySwkrlsState(&state[0], &tmp);

    for(i = 0; i < (LAG-1); ++i) {
        copySwkrlsState(&state[i+1], &state[i]);
    }

    copySwkrlsState(&tmp, &state[LAG-1]);
}
#endif

