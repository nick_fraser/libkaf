
%Run the tests for the sw-krls algorithm.

clear;
close all;

%Results to output results.
csv = 0;
mat = 0;

%Tests to run.
swkrls_hwfp = 1;
knlms_hwfp = 1;
fbkrls_hwfp = 1;
aldkrls_hwfp = 1;
st_hwfp = 0;
swkrls_mca = 0;
knlms_mca = 0;
fbkrls_mca = 0;
dmoss_test = 0;
generic_test = 1;
fpl2015_test = 1;

%precision_levels.
t = [2:1:53];
mca_sims = 100;

%Dictionary sizes.
Nvals = [16 32 64 128];

%Test strings.
mk = 'make swkrls_c.mex';
mklib = 'make kaf_c.mex';
outrd = '> /dev/null 2>&1';
if strcmp(getenv('OS'), 'Windows_NT') %Work around for windows environment.
    mk = sprintf('%s CC=x86_64-w64-mingw32-gcc', mk);
    mklib = sprintf('%s CC=x86_64-w64-mingw32-gcc', mklib);
    outrd = '> makelog.txt 2>&1';
end
mkcln = sprintf('make clean %s', outrd);

if swkrls_hwfp == 1
%Perform a fast test which just a single or double precision version of SW-KRLS using.
system(mkcln); system(sprintf('%s PRECISION=float EXP=orig %s', mk, outrd));
res = swkrls_sims('t', 24, 'mca_sims', 0, 'csv', csv, 'mat', mat, 'name', 'nexp', 'sng', 1);
system(mkcln); system(sprintf('%s PRECISION=double EXP=orig %s', mk, outrd));
res = swkrls_sims('t', 53, 'mca_sims', 0, 'csv', csv, 'mat', mat, 'name', 'nexp');
end

if knlms_hwfp == 1
%Perform a fast test which just a single or double precision version of KNLMS using.
system(mkcln); system(sprintf('%s PRECISION=float EXP=orig C_FLAGS=-DKNLMS_TRAIN %s', mk, outrd));
res = swkrls_sims('t', 24, 'mca_sims', 0, 'csv', csv, 'mat', mat, 'name', 'knlms-nexp', 'sng', 1, 'alg', 'knlms');
system(mkcln); system(sprintf('%s PRECISION=double EXP=orig C_FLAGS=-DKNLMS_TRAIN %s', mk, outrd));
res = swkrls_sims('t', 53, 'mca_sims', 0, 'csv', csv, 'mat', mat, 'name', 'knlms-nexp', 'alg', 'knlms');
end

if fbkrls_hwfp == 1
%Perform a fast test which just a single or double precision version of FBKRLS using.
system(mkcln); system(sprintf('%s PRECISION=float EXP=orig C_FLAGS=-DFBKRLS_TRAIN %s', mk, outrd));
res = swkrls_sims('t', 24, 'mca_sims', 0, 'csv', csv, 'mat', mat, 'name', 'fbkrls-nexp', 'sng', 1, 'alg', 'fbkrls');
system(mkcln); system(sprintf('%s PRECISION=double EXP=orig C_FLAGS=-DFBKRLS_TRAIN %s', mk, outrd));
res = swkrls_sims('t', 53, 'mca_sims', 0, 'csv', csv, 'mat', mat, 'name', 'fbkrls-nexp', 'alg', 'fbkrls');
end

if aldkrls_hwfp == 1
%Perform a fast test which just a single or double precision version of ALDKRLS using.
system(mkcln); system(sprintf('%s PRECISION=float EXP=orig C_FLAGS=-DALDKRLS_TRAIN %s', mk, outrd));
res = swkrls_sims('t', 24, 'mca_sims', 0, 'csv', csv, 'mat', mat, 'name', 'aldkrls-nexp', 'sng', 1, 'alg', 'aldkrls');
system(mkcln); system(sprintf('%s PRECISION=double EXP=orig C_FLAGS=-DALDKRLS_TRAIN %s', mk, outrd));
res = swkrls_sims('t', 53, 'mca_sims', 0, 'csv', csv, 'mat', mat, 'name', 'aldkrls-nexp', 'alg', 'aldkrls');
end

if st_hwfp == 1
%Perform compare the speed of different algorithms using different sizes for N.
%KNLMS
for N = Nvals
    system(mkcln); system(sprintf('%s PRECISION=double EXP=orig C_FLAGS="-DKNLMS_TRAIN -DWINDOW_LENGTH=%d" %s', mk, N, outrd));
    res = swkrls_sims('t', 53, 'mca_sims', 0, 'csv', csv, 'mat', mat, 'name', 'knlms-nexp', 'alg', 'knlms', 'N', N);
end

%SWKRLS
for N = Nvals
    system(mkcln); system(sprintf('%s PRECISION=double EXP=orig C_FLAGS=-DWINDOW_LENGTH=%d %s', mk, N-1, outrd));
    res = swkrls_sims('t', 53, 'mca_sims', 0, 'csv', csv, 'mat', mat, 'name', 'nexp', 'N', N-1);
end

%FBKRLS
for N = Nvals
    system(mkcln); system(sprintf('%s PRECISION=double EXP=orig C_FLAGS="-DFBKRLS_TRAIN -DWINDOW_LENGTH=%d" %s', mk, N, outrd));
    res = swkrls_sims('t', 53, 'mca_sims', 0, 'csv', csv, 'mat', mat, 'name', 'fbkrls-nexp', 'alg', 'fbkrls', 'N', N);
end

end

if swkrls_mca == 1
%Create some test data for the SW-KRLS algorithm.
%Using the original EXP function.
system(mkcln); system(sprintf('%s MCA=true EXP=orig %s', mk, outrd));
res = swkrls_sims('t', t, 'mca_sims', 0, 'csv', csv, 'mat', mat, 'name', 'nexp');
res = swkrls_sims('t', t, 'mca_sims', mca_sims, 'csv', csv, 'mat', mat, 'name', 'nexp');

%Using the 'nexp' function.
%Compile the mex library using the 'nexp' function.
system(mkcln); system(sprintf('%s MCA=true %s', mk, outrd));
res = swkrls_sims('t', t, 'mca_sims', 0, 'csv', csv, 'mat', mat, 'name', 'nexp');
res = swkrls_sims('t', t, 'mca_sims', mca_sims, 'csv', csv, 'mat', mat, 'name', 'nexp');

%Using the LUT method for exp.
%Generate LUT for algorithm.
for lut_size = [512 1024 2048]
    gen_exp_lut([-5 0], [lut_size 64], 'exp_lut.h');
    %Compile the mex library using the 'exp_lut' function.
    system(mkcln); system(sprintf('%s MCA=true EXP=lut %s', mk, outrd));
    res = swkrls_sims('t', t, 'mca_sims', 0, 'csv', csv, 'mat', mat, 'name', ['lut.-5.0.' int2str(lut_size)]);
    res = swkrls_sims('t', t, 'mca_sims', mca_sims, 'csv', csv, 'mat', mat, 'name', ['lut.-5.0.' int2str(lut_size)]);
end

%Using the LUT method with linear interpolation for exp.
%Generate LUT for algorithm.
for lut_size = [32 64 128]
    gen_exp_lut([-5 0], [1024 lut_size], 'exp_lut.h');
    %Compile the mex library using the 'exp_lut' function.
    system(mkcln); system(sprintf('%s MCA=true EXP=lutli %s', mk, outrd));
    res = swkrls_sims('t', t, 'mca_sims', 0, 'csv', csv, 'mat', mat, 'name', ['lutli.-5.0.' int2str(lut_size)]);
    res = swkrls_sims('t', [24], 'mca_sims', mca_sims, 'csv', csv, 'mat', mat, 'name', ['lutli.-5.0.' int2str(lut_size)]);
end
end

if knlms_mca == 1
%Create some test data for the KNLMS algorithm.
%Using the original EXP function.
system(mkcln); system(sprintf('%s MCA=true EXP=orig C_FLAGS="-DKNLMS_TRAIN -DRESET_TS_WRAP" %s', mk, outrd));
swkrls = swkrls_sims('t', t, 'mca_sims', 0, 'csv', csv, 'mat', mat, 'name', 'knlms-nexp', 'alg', 'knlms');
swkrls = swkrls_sims('t', t, 'mca_sims', mca_sims, 'csv', csv, 'mat', mat, 'name', 'knlms-nexp', 'alg', 'knlms');

%Using the 'nexp' function.
%Compile the mex library using the 'nexp' function.
system(mkcln); system(sprintf('%s MCA=true C_FLAGS="-DKNLMS_TRAIN -DRESET_TS_WRAP" %s', mk, outrd));
swkrls = swkrls_sims('t', t, 'mca_sims', 0, 'csv', csv, 'mat', mat, 'name', 'knlms-nexp', 'alg', 'knlms');
swkrls = swkrls_sims('t', t, 'mca_sims', mca_sims, 'csv', csv, 'mat', mat, 'name', 'knlms-nexp', 'alg', 'knlms');

%Using the LUT method for exp.
%Generate LUT for algorithm.
for lut_size = [128 256 512 1024 2048]
    gen_exp_lut([-5 0], [lut_size 64], 'exp_lut.h');
    %Compile the mex library using the 'exp_lut' function.
    system(mkcln); system(sprintf('%s MCA=true EXP=lut C_FLAGS="-DKNLMS_TRAIN -DRESET_TS_WRAP" %s', mk, outrd));
    swkrls = swkrls_sims('t', t, 'mca_sims', 0, 'csv', csv, 'mat', mat, 'name', ['knlms-lut.-5.0.' int2str(lut_size)], 'alg', 'knlms');
    swkrls = swkrls_sims('t', t, 'mca_sims', mca_sims, 'csv', csv, 'mat', mat, 'name', ['knlms-lut.-5.0.' int2str(lut_size)], 'alg', 'knlms');
end

%Using the LUT method with linear interpolation for exp.
%Generate LUT for algorithm.
for lut_size = [8 16 32 64 128]
    gen_exp_lut([-5 0], [1024 lut_size], 'exp_lut.h');
    %Compile the mex library using the 'exp_lut' function.
    system(mkcln); system(sprintf('%s MCA=true EXP=lutli C_FLAGS="-DKNLMS_TRAIN -DRESET_TS_WRAP" %s', mk, outrd));
    swkrls = swkrls_sims('t', t, 'mca_sims', 0, 'csv', csv, 'mat', mat, 'name', ['knlms-lutli.-5.0.' int2str(lut_size)], 'alg', 'knlms');
    swkrls = swkrls_sims('t', [24], 'mca_sims', mca_sims, 'csv', csv, 'mat', mat, 'name', ['knlms-lutli.-5.0.' int2str(lut_size)], 'alg', 'knlms');
end
end

if fbkrls_mca == 1
%Create some test data for the FBKRLS algorithm.
%Using the original EXP function.
system(mkcln); system(sprintf('%s MCA=true EXP=orig C_FLAGS="-DFBKRLS_TRAIN -DRESET_TS_WRAP" %s', mk, outrd));
swkrls = swkrls_sims('t', t, 'mca_sims', 0, 'csv', csv, 'mat', mat, 'name', 'fbkrls-nexp', 'alg', 'fbkrls');
swkrls = swkrls_sims('t', t, 'mca_sims', mca_sims, 'csv', csv, 'mat', mat, 'name', 'fbkrls-nexp', 'alg', 'fbkrls');

%Using the 'nexp' function.
%Compile the mex library using the 'nexp' function.
system(mkcln); system(sprintf('%s MCA=true C_FLAGS="-DFBKRLS_TRAIN -DRESET_TS_WRAP" %s', mk, outrd));
swkrls = swkrls_sims('t', t, 'mca_sims', 0, 'csv', csv, 'mat', mat, 'name', 'fbkrls-nexp', 'alg', 'fbkrls');
swkrls = swkrls_sims('t', t, 'mca_sims', mca_sims, 'csv', csv, 'mat', mat, 'name', 'fbkrls-nexp', 'alg', 'fbkrls');

%Using the LUT method for exp.
%Generate LUT for algorithm.
for lut_size = [128 256 512 1024 2048]
    gen_exp_lut([-5 0], [lut_size 64], 'exp_lut.h');
    %Compile the mex library using the 'exp_lut' function.
    system(mkcln); system(sprintf('%s MCA=true EXP=lut C_FLAGS="-DFBKRLS_TRAIN -DRESET_TS_WRAP" %s', mk, outrd));
    swkrls = swkrls_sims('t', t, 'mca_sims', 0, 'csv', csv, 'mat', mat, 'name', ['fbkrls-lut.-5.0.' int2str(lut_size)], 'alg', 'fbkrls');
    swkrls = swkrls_sims('t', t, 'mca_sims', mca_sims, 'csv', csv, 'mat', mat, 'name', ['fbkrls-lut.-5.0.' int2str(lut_size)], 'alg', 'fbkrls');
end

%Using the LUT method with linear interpolation for exp.
%Generate LUT for algorithm.
for lut_size = [8 16 32 64 128]
    gen_exp_lut([-5 0], [1024 lut_size], 'exp_lut.h');
    %Compile the mex library using the 'exp_lut' function.
    system(mkcln); system(sprintf('%s MCA=true EXP=lutli C_FLAGS="-DFBKRLS_TRAIN -DRESET_TS_WRAP" %s', mk, outrd));
    swkrls = swkrls_sims('t', t, 'mca_sims', 0, 'csv', csv, 'mat', mat, 'name', ['fbkrls-lutli.-5.0.' int2str(lut_size)], 'alg', 'fbkrls');
    swkrls = swkrls_sims('t', [24], 'mca_sims', mca_sims, 'csv', csv, 'mat', mat, 'name', ['fbkrls-lutli.-5.0.' int2str(lut_size)], 'alg', 'fbkrls');
end
end

if dmoss_test == 1
    Ndtrain = [3000 172800 3153600];
    Ndtest = [720 40320];
    M = 3;
    for Ntrain = Ndtrain;
        for N = Ndtest;
            system(mkcln); system(sprintf('%s PRECISION=double EXP=orig C_FLAGS="-DWINDOW_LENGTH=%d -DEMBED_LENGTH=%d -DDYN_MEM" %s', mk, N, M, outrd));
            res = swkrls_sims('t', 53, 'mca_sims', 0, 'csv', csv, 'mat', mat, 'name', 'nexp', 'N', N, 'embed', M, 'Ntrain', Ntrain);
        end
    end
end

if generic_test == 1
    %system(mkcln); system(sprintf('%s PRECISION=double EXP=orig C_FLAGS=-DDYN_MEM LIN=mwlapack %s', mklib, outrd));
    N = 5000;
    Ntest = 2000;
    el = 7;
    swkrls_pars.c = 1E-4; swkrls_pars.M = 20; swkrls_pars.kernel.type = 'gauss'; swkrls_pars.kernel.par = 0.6; swkrls_pars.algo = @kaf_swkrls; swkrls_pars.name = 'SWKRLS';
    fbkrls_pars.c = 1E-4; fbkrls_pars.M = 20; fbkrls_pars.kernel.type = 'gauss'; fbkrls_pars.kernel.par = 0.6; fbkrls_pars.algo = @kaf_fbkrls; fbkrls_pars.name = 'FBKRLS'; fbkrls_pars.mu = 0;
    knlms_pars.eta = 1E-1; knlms_pars.c = 1E-4; knlms_pars.M = 20; knlms_pars.kernel.type = 'gauss'; knlms_pars.kernel.par = 0.6; knlms_pars.algo = @kaf_knlms; knlms_pars.name = 'KNLMS'; knlms_pars.mu0 = 0.9;
    aldkrls_pars.thresh = 1E-2; aldkrls_pars.M = 20; aldkrls_pars.kernel.type = 'gauss'; aldkrls_pars.kernel.par = 0.6; aldkrls_pars.algo = @kaf_aldkrls; aldkrls_pars.name = 'ALDKRLS';
    x = gen_mg_series(N);
    [x_tmp,y_tmp] = setup_data(x, el, 'verbose', 1);
    x_train = x_tmp(1:(end-Ntest),:);
    y_train = y_tmp(1:(end-Ntest),1);
    x_test = x_tmp((end-Ntest+1):end,:);
    y_test = y_tmp((end-Ntest+1):end,1);
    clear x_tmp y_tmp;
    Ntrain = length(y_train);

    %SWKRLS
    vars = [];
    tic; for i = 1:Ntrain, vars.t = i; vars = swkrls_pars.algo(vars, swkrls_pars, x_train(i,:), y_train(i)); end; toc
    tic; c_vars = ckaf_wrap('swkrls', x_train, swkrls_pars, 'y', y_train); toc
    tic; y_est_ml = swkrls_pars.algo(vars, swkrls_pars, x_test); toc
    tic; y_est_c = ckaf_wrap('swkrls', x_test, swkrls_pars, 'vars', c_vars); toc
    fprintf('%s: MSE(ML) = %d, MSE(C) = %d, MSE(ML-C) = %d.\n', swkrls_pars.name, mean((y_est_ml - y_test).^2), mean((y_est_c - y_test).^2), mean((y_est_ml - y_est_c).^2));

    %FBKRLS
    vars = []; vars.mem = zeros(fbkrls_pars.M, el); vars.ymem = zeros(fbkrls_pars.M, 1); vars.K_inv = eye(fbkrls_pars.M, fbkrls_pars.M)/(1 + fbkrls_pars.c); vars.basis = zeros(1, fbkrls_pars.M);
    tic; for i = 1:Ntrain, vars.t = i; vars = fbkrls_pars.algo(vars, fbkrls_pars, x_train(i,:), y_train(i)); end; toc
    tic; c_vars = ckaf_wrap('fbkrls', x_train, fbkrls_pars, 'y', y_train); toc
    tic; y_est_ml = fbkrls_pars.algo(vars, fbkrls_pars, x_test); toc
    tic; y_est_c = ckaf_wrap('fbkrls', x_test, fbkrls_pars, 'vars', c_vars); toc
    fprintf('%s: MSE(ML) = %d, MSE(C) = %d, MSE(ML-C) = %d.\n', fbkrls_pars.name, mean((y_est_ml - y_test).^2), mean((y_est_c - y_test).^2), mean((y_est_ml - y_est_c).^2));

    %KNLMS
    vars = [];
    tic; for i = 1:Ntrain, vars.t = i; vars = knlms_pars.algo(vars, knlms_pars, x_train(i,:), y_train(i)); end; toc
    tic; c_vars = ckaf_wrap('knlms', x_train, knlms_pars, 'y', y_train); toc
    tic; y_est_ml = knlms_pars.algo(vars, knlms_pars, x_test); toc
    tic; y_est_c = ckaf_wrap('knlms', x_test, knlms_pars, 'vars', c_vars); toc
    fprintf('%s: MSE(ML) = %d, MSE(C) = %d, MSE(ML-C) = %d.\n', knlms_pars.name, mean((y_est_ml - y_test).^2), mean((y_est_c - y_test).^2), mean((y_est_ml - y_est_c).^2));

    %ALDKRLS
    vars = [];
    tic; for i = 1:Ntrain, vars.t = i; vars = aldkrls_pars.algo(vars, aldkrls_pars, x_train(i,:), y_train(i)); end; toc
    tic; c_vars = ckaf_wrap('aldkrls', x_train, aldkrls_pars, 'y', y_train); toc
    tic; y_est_ml = aldkrls_pars.algo(vars, aldkrls_pars, x_test); toc
    tic; y_est_c = ckaf_wrap('aldkrls', x_test, aldkrls_pars, 'vars', c_vars); toc
    fprintf('%s: MSE(ML) = %d, MSE(C) = %d, MSE(ML-C) = %d.\n', aldkrls_pars.name, mean((y_est_ml - y_test).^2), mean((y_est_c - y_test).^2), mean((y_est_ml - y_est_c).^2));

end

if fpl2015_test == 1,
    system(mkcln); system(sprintf('%s PRECISION=double EXP=orig C_FLAGS=-DDYN_MEM LIN=mwlapack %s', mklib, outrd));
    %system(mkcln); system(sprintf('%s PRECISION=double EXP=orig C_FLAGS=-DDYN_MEM LIN=mwlapack MATLAB_ROOT=/opt/MATLAB/R2014a/ %s', mklib, outrd));
    N = 5000;
    Ntest = 2000;
    el = 8;
    x = gen_mg_series(N);
    [x_tmp,y_tmp] = setup_data(x, el, 'verbose', 1);
    x_train = x_tmp(1:(end-Ntest),:);
    y_train = y_tmp(1:(end-Ntest),1);
    x_test = x_tmp((end-Ntest+1):end,:);
    y_test = y_tmp((end-Ntest+1):end,1);
    clear x_tmp y_tmp;
    Ntrain = length(y_train);

    % Algorithm parameters.
    knlms_pars.eta = 1E-2;
    knlms_pars.c = 1E-2;
    knlms_pars.M = 16;
    knlms_pars.kernel.type = 'gauss';
    %knlms_pars.kernel.par = sqrt(1/(2*0.7));
    knlms_pars.kernel.par = 0.7;
    knlms_pars.algo = @kaf_knlms;
    knlms_pars.name = 'KNLMS';
    knlms_pars.mu0 = 0.7;

    %KNLMS
    vars = [];
    tic; for i = 1:Ntrain, vars.t = i; vars = knlms_pars.algo(vars, knlms_pars, x_train(i,:), y_train(i)); end; kb_time = toc;
    tic; c_vars = ckaf_wrap('knlms', x_train, knlms_pars, 'y', y_train); ck_time = toc;
    tic; y_est_ml = knlms_pars.algo(vars, knlms_pars, x_test); toc;
    tic; y_est_c = ckaf_wrap('knlms', x_test, knlms_pars, 'vars', c_vars); toc;
    fprintf('%s: MSE(ML) = %d, MSE(C) = %d, MSE(ML-C) = %d.\n', knlms_pars.name, mean((y_est_ml - y_test).^2), mean((y_est_c - y_test).^2), mean((y_est_ml - y_est_c).^2));
    fprintf('%s: Time/Update(KAFBOX) = %d, Time/Update(C) = %d\n', knlms_pars.name, kb_time/Ntrain, ck_time/Ntrain);
end

%Clean project dir again.
system(mkcln);
system('rm -f makelog.txt');

