
#include "fbkrls.h"

#ifdef DYN_MEM
void InitialiseFBKRLSState(fbkrls_state_t * state, int wl, int el, const MATHTYPE_T c, const MATHTYPE_T sigma) { //Initialise the state object based on some parameters given in as function parameters.
    InitialiseGenericState(state, wl, el, c, sigma);
}

void FreeFBKRLSState(fbkrls_state_t * state) { //Free the members of the state variable.
    FreeGenericState(state);
}

#else /*STAT_MEM*/
void InitialiseFBKRLSState(fbkrls_state_t * state, const MATHTYPE_T c, const MATHTYPE_T sigma) { //Initialise the state object based on some parameters given in fbkrls.h.
    InitialiseGenericState(state, c, sigma);
}
#endif

void fbkrls_train(const MATHTYPE_T * const x, const MATHTYPE_T y, fbkrls_state_t * const state) { //Update the system state and calculate the next output in the time series.
    #ifdef DYN_MEM
    MATHTYPE_T * k;
    MATHTYPE_T * a_tilde;
    MATHTYPE_T * y_tilde;
    #else /*STAT_MEM*/
    MATHTYPE_T k[WINDOW_LENGTH+1];    
    MATHTYPE_T a_tilde[WINDOW_LENGTH+1];
    MATHTYPE_T y_tilde[WINDOW_LENGTH+1];
    #endif
    int i,j;
    int wl,el;
    int index;

    #ifdef DYN_MEM
    wl = state->wl;
    el = state->el;
    #else /*STAT_MEM*/
    wl = WINDOW_LENGTH;
    el = EMBED_LENGTH;
    #endif

    #ifdef DYN_MEM
    k = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(wl+1));
    a_tilde = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(wl+1));
    y_tilde = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(wl+1));
    #endif

    //Calculate the kernel vector.
    #ifdef DYN_MEM
    GaussianKernel(state->x, x, k, KERNEL_UPDATE, wl, el, state->sigma); //A generic version of the Guassian kernel.
    #else /*STAT_MEM*/
    GaussianKernel(state->x, x, k, KERNEL_UPDATE, state->sigma); //A generic version of the Guassian kernel.
    #endif

    //Regularise the kernel vector.
    k[wl] = k[wl] + state->c;

    //Add a row/col to the K_inv matrix.
    #ifdef DYN_MEM
    InverseAddRowCol(k, k[wl], state->K_inv, wl);
    #else /*STAT_MEM*/
    InverseAddRowCol(k, k[wl], state->K_inv);
    #endif

    //Copy y to y_tilde.
    CopyVector(state->y, 1, y_tilde, 1, wl);
    y_tilde[wl] = y;

    //Calculate a_tilde.
    //Calculate alpha. alpha = K_inv*y[n].
    MatrixVectorMult(state->K_inv, y_tilde, a_tilde, wl+1, wl+1, VEC_RIGHT);

    //Add test to find the best row/column to remove.
    VectorAbs(a_tilde, 1, a_tilde, 1, wl+1);
    ElementDivision(a_tilde, 1, state->K_inv, wl+2, a_tilde, 1, wl+1);
    index = MinIndex(a_tilde, 1, wl+1);

    //Remove the row/col from the K_inv matrix specified by index.
    #ifdef DYN_MEM
    InverseRemoveRowColAtIndex(state->K_inv, wl, index);
    #else /*STAT_MEM*/
    InverseRemoveRowColAtIndex(state->K_inv, index);
    #endif

    //Add input x to the input of the state matrix. Matrix is in column major format. Remove the top row and add input to the last.
    if(index < wl) {
        RemoveRow(state->x, wl, el, index);
        CopyVector(x, 1, (state->x + wl-1), wl, el);
    }

    //Add y to the output of the state vector.
    CopyVector(y_tilde, 1, state->y, 1, index);
    CopyVector(&y_tilde[index+1], 1, (state->y + index), 1, wl-index);

    //Calculate alpha. alpha = K_inv*y[n].
    MatrixVectorMult(state->K_inv, state->y, state->alpha, wl, wl, VEC_RIGHT);

    #ifdef DYN_MEM
    free(k);
    free(a_tilde);
    free(y_tilde);
    #endif
}

MATHTYPE_T fbkrls_predict(const MATHTYPE_T * const x, const fbkrls_state_t * const state) {
    return swkrls_predict(x, state);
}

MATHTYPE_T fbkrls_ts_wrap(const MATHTYPE_T x, fbkrls_state_t * const state) { //A wrapper for performing single step time series prediction.
    int i,el;
    #ifdef DYN_MEM
    #warning "fbkrls_ts_wrap function will cause a memory leak in dynamic mode."
    static MATHTYPE_T * x_train = NULL;
    el = state->el;
    if(x_train == NULL) {
        x_train = (MATHTYPE_T *)malloc(el*sizeof(MATHTYPE_T));
        for (i = 0; i < el; ++i)
            x_train[i] = 0.0;
    }
    #else /*STAT_MEM*/
    static MATHTYPE_T x_train[EMBED_LENGTH] = {0.0};
    el = EMBED_LENGTH;
    #endif

    #ifdef SUBLINEAR_TRAINING
    static int count = 0;
    #endif

    #ifdef RESET_TS_WRAP
    #ifdef SUBLINEAR_TRAINING
    count = 0;
    #endif
    #endif /*RESET_TS_WRAP*/

    #ifdef SUBLINEAR_TRAINING
    if(count == 0)
    #endif
    fbkrls_train(x_train, x ,state);
    #ifdef SUBLINEAR_TRAINING
    ++count;
    count %= PREDICTION_TRAINING_RATIO;
    #endif

    for(i = 0; i < (el-1); ++i) {
        x_train[i] = x_train[i+1];
    }
    x_train[el-1] = x;

    return fbkrls_predict(x_train, &state[0]);
}

#ifdef DYN_MEM
FUNCTION_PREFIX void InverseRemoveRowColAtIndex(MATHTYPE_T * const K_inv, const int wl, const int index) { //Remove a row and column from K^-1 matrix specified at index.
#else /*STAT_MEM*/
FUNCTION_PREFIX void InverseRemoveRowColAtIndex(MATHTYPE_T * const K_inv, const int index) { //Remove a row and column from K^-1 matrix specified at index.
#endif
    MATHTYPE_T e;
    int i,j;
    #ifdef DYN_MEM
    MATHTYPE_T * G;
    MATHTYPE_T * f;
    #else /*STAT_MEM*/
    _STAT MATHTYPE_T G[WINDOW_LENGTH*WINDOW_LENGTH];
    _STAT MATHTYPE_T f[WINDOW_LENGTH];
    int wl;
    #endif

    #ifdef DYN_MEM
    f = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*wl);
    G = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*wl*wl);
    #else /*STAT_MEM*/
    wl = WINDOW_LENGTH;
    #endif

    //Create the sub-matrix G.
    SubMatrix(K_inv, wl+1, wl+1, 0, G, index, index, wl); //Upper left quadrant of G.
    SubMatrix(K_inv, wl+1, wl+1, index+1, G + index, wl-index, index, wl); //Bottom left quadrant of G.
    SubMatrix(K_inv, wl+1, wl+1, (index+1)*(wl+2), G + index*(wl+1), wl-index, wl-index, wl); //Bottom right quadrant of G.
    SubMatrix(K_inv, wl+1, wl+1, (index+1)*(wl+1), G + index*(wl), index, wl-index, wl); //Top right quadrant of G.

    //Create sub-matrix f.
    SubMatrix(K_inv, wl+1, wl+1, index*(wl+1), f, index, 1, wl); //First segment of f.
    SubMatrix(K_inv, wl+1, wl+1, index*(wl+2) + 1, f + index, wl-index, 1, wl); //Second segment of f.

    e = K_inv[index*(wl+2)];

    //Set K_inv to scaled values of f.
    for(i = 0; i < wl; ++i) {
        ScaleVector(f, 1, f[i]/e, wl, K_inv + i*wl);
    }

    //Calculate K_inv. K_inv = G - K_inv.
    for(i = 0; i < wl; ++i) {
        for(j = 0; j < wl; ++j) {
            K_inv[i + j*wl] = G[i + j*wl] - K_inv[i + j*wl];
        }
    }
    #ifdef DYN_MEM
    free(f);
    free(G);
    #endif
}

