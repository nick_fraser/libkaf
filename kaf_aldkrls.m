function out = kaf_aldkrls(vars,pars,x,y)
% KAF_ALDKRLS implements the basic iteration of the kernel recursive least-
% squares (KRLS) algorithm with approximate linear dependency (ALD) novelty
% criterion.
% Input:	- vars: structure containing the used variables
%			- pars: structure containing kernel and algorithm parameters
%			- x: matrix containing input vectors as rows
%			- y: vector containing output values
% Output:	- out: contains the new vars if the algorithm is in learning
%			mode (when x and y are provided) and the estimated output when
%			the algorithm is being evaluated (when only x is given).
% Dependencies: kaf_kernel.
% USAGE: out = kaf_aldkrls(vars,pars,x,y)
%
% Author: Steven Van Vaerenbergh (steven *at* gtas.dicom.unican.es), 2012.
% Id: kaf_aldkrls.m v1.1 2012/09/10
% This file is part of the Kernel Adaptive Filtering Toolbox (KMBOX) for
% MATLAB and OCTAVE. http://sourceforge.net/p/kafbox
%
% The algorithm in this file is based on the following publication:
% Y. Engel, S. Mannor, and R. Meir. "The kernel recursive least-squares
% algorithm", IEEE Transactions on Signal Processing, volume 52, no. 8,
% pages 2275�2285, 2004.
%
% This implementation includes a slight modification: inclusion of a
% maximum dictionary size "M".
%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, version 3 (as included and available at
% http://www.gnu.org/licenses).

% read parameters
ktype = pars.kernel.type;
kpar = pars.kernel.par;
thresh = pars.thresh;	% ALD threshold
if (isfield(pars,'M'))
    M = pars.M;	% maximal dictionary length
else
    M = Inf;	% ever-growing
end

if nargin<4,
    mode = 'eval';
else
    if ~isfield(vars,'basis')
        mode = 'init';
    else
        mode = 'train';
    end
end

switch mode
    case 'init'		% initialize
        % initialize algorithm
        mem = x;
        basis = 1;
        P = 1;
        ktt = kaf_kernel(x,x,ktype,kpar);
        K_inv = 1/ktt;
        alpha = y*K_inv;
        
        % flops count
        %fl_Kinv = kaf_flops('div',1);
        %fl_alpha = kaf_flops('mult',1);
        %fl = fl_ktt + fl_Kinv + fl_alpha;
        
        % write variables
        vars.K_inv = K_inv;
        vars.alpha = alpha;
        vars.mem = mem;
        vars.basis = basis;
        vars.P = P;
        %vars.flops = fl;
        
        % bytes stored in memory
        %bytes_K_inv = kaf_bytes('double',1);
        %bytes_alpha = kaf_bytes('double',1);
        %bytes_mem = kaf_bytes('double',size(x,2)); % data is assumed to be double-precision
        %bytes_P = kaf_bytes('double',1);
        %vars.bytes = bytes_K_inv + bytes_alpha + bytes_mem + bytes_P;
        
        out = vars;
    case 'eval'		% evaluate
        mem = vars.mem;
        alpha = vars.alpha;
        
        K = kaf_kernel(x,mem,ktype,kpar);
        y = K*alpha;
        
        out = y;
    case 'train'		% train
        % read variables
        mem = vars.mem;
        alpha = vars.alpha;
        K_inv = vars.K_inv;
        P = vars.P;
        t = vars.t;		% temporal index
        basis = vars.basis;
        
        % algorithm
        ktt = kaf_kernel(x,x,ktype,kpar);        
        kt = kaf_kernel(mem,x,ktype,kpar);
        a_opt = K_inv*kt;
        delta = ktt - kt'*a_opt;
        m = size(mem,1);
        if (delta>thresh && m<M)	% expand dictionary ("full update")
            K_inv = 1/delta*[delta*K_inv + a_opt*a_opt', -a_opt; -a_opt', 1];
            Z = zeros(size(P,1),1);
            P = [P Z; Z' 1];
            ode = 1/delta*(y-kt'*alpha);
            alpha = [alpha - a_opt*ode; ode];
            mem = [mem; x];
            basis = [basis; t];
            m = m + 1;
            
            % flops count
            %fl_Kinv = kaf_flops('sum',m^2) + kaf_flops('mult',1+m*(m+1)) + kaf_flops('div',1); % K_inv and a'*a are symmetric
            %fl_ode = kaf_flops('sum',1) + kaf_flops('mult',1) + kaf_flops('mult',1,m,1);% 1/delta is already calculated
            %fl_alpha = kaf_flops('sum',m) + kaf_flops('mult',m);
            %fl_update = fl_Kinv + fl_ode + fl_alpha;
            
        else	% only update alpha ("reduced update")
            q = P*a_opt/(1+a_opt'*P*a_opt);
            P = P - q*(a_opt'*P);
            alpha = alpha + K_inv*q*(y-kt'*alpha);
            
            % flops count
            %fl_q = kaf_flops('sum',1) + kaf_flops('mult',m,m,1) + kaf_flops('mult',1,m,1) + kaf_flops('div',1);
            %fl_P = kaf_flops('sum',m*(m+1)/2) + kaf_flops('mult',m,1,m); % P is symmetric
            %fl_alpha = kaf_flops('sum',m+1) + kaf_flops('mult',m,m,1) + kaf_flops('mult',1,m,1);
            %fl_update = fl_q + fl_P + fl_alpha;
        end
        
        % flops count
        %fl_aopt = kaf_flops('mult',m,m,1);
        %fl_delta = kaf_flops('sum',1) + kaf_flops('mult',1,m,m);
        %fl_compare = kaf_flops('ltgt',1);
        %fl = fl_ktt + fl_kt + fl_aopt + fl_delta + fl_compare + fl_update;
        
        % write variables
        vars.K_inv = K_inv;
        vars.alpha = alpha;
        vars.mem = mem;
        vars.P = P;
        vars.basis = basis;
        %vars.flops = fl;
        
        % bytes stored in memory
        %bytes_K_inv = kaf_bytes('double',[m,m],'symm');
        %bytes_alpha = kaf_bytes('double',m);
        %bytes_mem = kaf_bytes('double',[size(x,2),m]); % data is assumed to be double-precision
        %bytes_P = kaf_bytes('double',[m,m],'symm');
        %vars.bytes = bytes_K_inv + bytes_alpha + bytes_mem + bytes_P;
        
        out = vars;
end
