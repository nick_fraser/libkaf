function varout = kaf_klms(vars,pars,x,y)
% KAF_KLMS implements the basic iteration of the KLMS algorithm (kernel
% LMS). This implementation includes a FLOPS and BYTES count for the 
% training procedure.
%
% Input:	- vars: structure containing the used variables
%			- pars: structure containing kernel and algorithm parameters
%			- x: matrix containing input vectors as rows
%			- y: vector containing output values
% Output:	- out: contains the new vars if the algorithm is in learning
%			mode (when x and y are provided) and the estimated output when
%			the algorithm is being evaluated (when only x is given).
% Dependencies: kaf_kernel.
% USAGE: out = kaf_klms(vars,pars,x,y)
%
% Author: Steven Van Vaerenbergh (steven *at* gtas.dicom.unican.es), 2011
% Id: kaf_klms.m v1.5 2012/09/10
% This file is part of the Kernel Adaptive Filtering Toolbox (KAFBOX) for
% MATLAB and OCTAVE. http://sourceforge.net/p/kafbox
%
% The algorithm in this file is based on the following publication:
% W. Liu, P.P. Pokharel and J.C. Principe. "The Kernel Least-Mean Squares
% Algorithm", IEEE Transactions on Signal Processing, volume 56, no. 2,
% pages 543-554, 2008.
%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, version 3 (as included and available at
% http://www.gnu.org/licenses).

ktype = pars.kernel.type;
kpar = pars.kernel.par;

if nargin<4,
    mode = 'eval';
else
    if ~isfield(vars,'basis')
        mode = 'init';
    else
        mode = 'train';
    end
end

switch mode
    case 'init'		% initialize
        % read parameters
        %mu = pars.mu;		% learning rate
        
        % initialize algorithm
        mem = x;
        basis = 1;
        y_est = 0;
        err = y-y_est;
        alpha = pars.eta / (pars.c + 1) * err;
        
        % flops count
        %fl_alpha = kaf_flops('mult',1);
        %fl = fl_alpha;
        
        % write variables
        vars.alpha = alpha;
        vars.mem = mem;
        vars.basis = basis;
        %vars.flops = fl;

        % bytes stored in memory
        %bytes_alfa = kaf_bytes('double',2);
        %bytes_mem = kaf_bytes('double',size(x,1)); % data is assumed to be double-precision
        %vars.bytes = bytes_alfa + bytes_mem;

        varout = vars;
    case 'eval'		% evaluate
        % read variables
        mem = vars.mem;
        alpha = vars.alpha;
        
        K = kaf_kernel(x,mem,ktype,kpar);
        y = K*alpha;
        
        varout = y;
    case 'train'		% train
        % read variables
        mem = vars.mem;
        alpha = vars.alpha;
        t = vars.t;
        basis = vars.basis;
        
        % algorithm
        m = length(alpha);
        if(m == 0)
            mem = x;
            alpha = 0;
        else
            kt = kaf_kernel(x,mem,ktype,kpar);
            if((m < pars.M) && (max(kt) <= pars.mu0)) % Avoid infinite growth.
                %fl_y = kaf_flops('mult',1,m,1);
                %fl_err = kaf_flops('sum',m);
                
                alpha = [alpha; 0.0];
                mem = [mem; x];
                basis = [basis;t];
                m = m + 1;
            end
        end
        
        h = kaf_kernel(x,mem,ktype,kpar);
        alpha = alpha + pars.eta / (pars.c + h*h') * (y - h*alpha) * h';

        % flops count
        %vars.flops = fl_k + fl_err;
            
        % write variables
        vars.alpha = alpha;
        vars.mem = mem;
        vars.basis = basis;
        
        % bytes stored in memory
        %bytes_alfa = kaf_bytes('double',m);
        %bytes_mem = kaf_bytes('double',[size(x,2),m]); % data is assumed to be double-precision
        %vars.bytes = bytes_alfa + bytes_mem;

        varout = vars;
end
