
#ifndef _FBKRLS_H
#define _FBKRLS_H

#include "defines.h"
#include "kernel.h"
#include "linear_math.h"
#include "swkrls.h" //Most of the required routines are in swkrls.h/c

typedef swkrls_generic_state_t fbkrls_state_t;

#ifdef DYN_MEM
void InitialiseFBKRLSState(fbkrls_state_t * state, int wl, int el, const MATHTYPE_T c, const MATHTYPE_T sigma); //Initialise the state object based on some parameters given in as function parameters.
void FreeFBKRLSState(fbkrls_state_t * state); //Free the members of the state variable.
FUNCTION_PREFIX void InverseRemoveRowColAtIndex(MATHTYPE_T * const K_inv, const int wl, const int index); //Remove the row and column from K^-1 matrix specified by index.
#else /*STAT_MEM*/
void InitialiseFBKRLSState(fbkrls_state_t * state, const MATHTYPE_T c, const MATHTYPE_T sigma); //Initialise the state object based on some parameters given in fbkrls.h.
FUNCTION_PREFIX void InverseRemoveRowColAtIndex(MATHTYPE_T * const K_inv, const int index); //Remove the row and column from K^-1 matrix specified by index.
#endif
void fbkrls_train(const MATHTYPE_T * const x, const MATHTYPE_T y, fbkrls_state_t * const state); //Update the system state and calculate the next output in the time series.
MATHTYPE_T fbkrls_predict(const MATHTYPE_T * const x, const fbkrls_state_t * const state);
MATHTYPE_T fbkrls_ts_wrap(const MATHTYPE_T x, fbkrls_state_t * const state); //A wrapper for performing single step time series prediction.

#endif /*_FBKRLS_H*/

