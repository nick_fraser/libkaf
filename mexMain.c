#include <mex.h>
#include <matrix.h>
#include "defines.h"
#include "swkrls.h"
#include "knlms.h"
#include "fbkrls.h"
#include "aldkrls.h"
#include <stdio.h>

#ifdef GENERIC_INTERFACE
#ifndef DYN_MEM
#error "DYN_MEM symbol must be defined to use the generic mex interface."
#endif

enum kaf_mode {
    PREDICT_OLD = 0,
    TRAIN_OLD,
    PREDICT_NEW,
    TRAIN_NEW
};

enum kaf_alg {
    SWKRLS = 0,
    FBKRLS,
    KNLMS,
    ALDKRLS
};

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    MATHTYPE_T *x, *y;
    MATHTYPE_T sigma, c, eta, epsilon, mu0, nu;
    long Nset,i,j,N,M;
    enum kaf_mode mode;
    enum kaf_alg alg;
    knlms_state_t knlms;
    aldkrls_state_t aldkrls;
    swkrls_generic_state_t swkrls;

    // nlhs -> number of outputs
    // nrhs -> number of inputs
    if(nrhs < 6)
        mexErrMsgTxt("Too few inputs.");
    if(nlhs < 1)
        mexErrMsgTxt("Too few outputs.");

    if(!mxIsInt32(prhs[0])) mexErrMsgTxt("Mode parameter is not of type Int32.");
    if(!mxIsInt32(prhs[1])) mexErrMsgTxt("Algorithm parameter is not of type Int32.");
    if(!mxIsInt32(prhs[3])) mexErrMsgTxt("M parameter is not of type Int32.");

    mode = *(int *)mxGetData(prhs[0]);
    alg = *(int *)mxGetData(prhs[1]);

    //Typecheck inputs.
    for(i = 2; i < nrhs; ++i) {
        if(i == 3) continue;
        #if SINGLE_MATH
        if(!mxIsSingle(prhs[i])) mexErrMsgTxt("Input (x) is not of type single.");
        #elif DOUBLE_MATH
        if(!mxIsDouble(prhs[i])) mexErrMsgTxt("Input (x) is not of type double.");
        #else
        mexErrMsgTxt("Cannot process input type.");
        #endif
    }

    M = mxGetM(prhs[2]);
    Nset = mxGetN(prhs[2]);
    N = *(int *)mxGetData(prhs[3]);
    x = (MATHTYPE_T *)mxGetData(prhs[2]);
    //Allocate output vector.
    if(mode == PREDICT_OLD || mode == PREDICT_NEW) {
        if(nrhs != 7) mexErrMsgTxt("Invalid input size for prediction mode."); //(mode, alg, x', sigma, M, D, alpha)
        if(nlhs != 1) mexErrMsgTxt("Invalid output size for prediction mode."); //y
        #if SINGLE_MATH
	    plhs[0] = mxCreateNumericMatrix(Nset, 1, mxSINGLE_CLASS, mxREAL);
        #elif DOUBLE_MATH
	    plhs[0] = mxCreateNumericMatrix(Nset, 1, mxDOUBLE_CLASS, mxREAL);
        #endif
        y = (MATHTYPE_T *)mxGetData(plhs[0]);
        switch(alg) {
        case SWKRLS:
        case FBKRLS: //All predictions are the same.
        case KNLMS:
        case ALDKRLS:
            swkrls.sigma = *(MATHTYPE_T *)mxGetData(prhs[4]);
            swkrls.wl = N;
            swkrls.el = M;
            swkrls.x = (MATHTYPE_T *)mxGetData(prhs[5]);
            swkrls.alpha = (MATHTYPE_T *)mxGetData(prhs[6]);
            for(i = 0; i < Nset; ++i) y[i] = swkrls_predict(x + i*M, &swkrls);
            break;
        default:
            mexErrMsgTxt("Unsupported algorithm.");
            break;
        }
    }
    else if(mode == TRAIN_OLD || mode == TRAIN_NEW) {
        switch(alg) {
        case SWKRLS:
        case FBKRLS:
            if(nrhs != 7) mexErrMsgTxt("Invalid input size for SWKRLS in training mode."); //(mode, alg, x', M, sigma, c, y)
            if(nlhs != 4) mexErrMsgTxt("Invalid output size for SWKRLS in training mode."); //[D, alpha, y, K_inv]
            #if SINGLE_MATH
	        plhs[0] = mxCreateNumericMatrix(N, M, mxSINGLE_CLASS, mxREAL);
	        plhs[1] = mxCreateNumericMatrix(N, 1, mxSINGLE_CLASS, mxREAL);
	        plhs[2] = mxCreateNumericMatrix(N, 1, mxSINGLE_CLASS, mxREAL);
	        plhs[3] = mxCreateNumericMatrix(N, N, mxSINGLE_CLASS, mxREAL);
            #elif DOUBLE_MATH
	        plhs[0] = mxCreateNumericMatrix(N, M, mxDOUBLE_CLASS, mxREAL);
	        plhs[1] = mxCreateNumericMatrix(N, 1, mxDOUBLE_CLASS, mxREAL);
	        plhs[2] = mxCreateNumericMatrix(N, 1, mxDOUBLE_CLASS, mxREAL);
	        plhs[3] = mxCreateNumericMatrix(N, N, mxDOUBLE_CLASS, mxREAL);
            #endif
            sigma = *(MATHTYPE_T *)mxGetData(prhs[4]);
            c = *(MATHTYPE_T *)mxGetData(prhs[5]);
            swkrls.x = (MATHTYPE_T *)mxGetData(plhs[0]);
            swkrls.alpha = (MATHTYPE_T *)mxGetData(plhs[1]);
            swkrls.y = (MATHTYPE_T *)mxGetData(plhs[2]);
            swkrls.K_inv = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(N+1)*(N+1));
            PartialInitialiseGenericState(&swkrls, N, M, c, sigma);
            y = (MATHTYPE_T *)mxGetData(prhs[6]);
            if(alg == SWKRLS) { 
                for(i = 0; i < Nset; ++i) swkrls_train(x + i*M, y[i], &swkrls);
            }
            if(alg == FBKRLS) {
                for(i = 0; i < Nset; ++i) fbkrls_train(x + i*M, y[i], &swkrls);
            }
            CopyMatrix(swkrls.K_inv, N, (MATHTYPE_T *)mxGetData(plhs[3]), N, N, N);
            free(swkrls.K_inv);
            break;
        case KNLMS:
            if(nrhs != 9) mexErrMsgTxt("Invalid input size for KNLMS in training mode."); //(mode, alg, x', M, sigma, epsilon, eta, mu0, y)
            if(nlhs != 2) mexErrMsgTxt("Invalid output size for KNLMS in training mode."); //[D, alpha]
            InitialiseKNLMSState(&knlms, N, M, *(MATHTYPE_T *)mxGetData(prhs[6]), *(MATHTYPE_T *)mxGetData(prhs[5]), *(MATHTYPE_T *)mxGetData(prhs[7]), *(MATHTYPE_T *)mxGetData(prhs[4]));
            y = (MATHTYPE_T *)mxGetData(prhs[8]);
            for(i = 0; i < Nset; ++i) knlms_train(x + i*M, y[i], &knlms);
            #if SINGLE_MATH
	        plhs[0] = mxCreateNumericMatrix(knlms.dict_size, M, mxSINGLE_CLASS, mxREAL);
	        plhs[1] = mxCreateNumericMatrix(knlms.dict_size, 1, mxSINGLE_CLASS, mxREAL);
            #elif DOUBLE_MATH
	        plhs[0] = mxCreateNumericMatrix(knlms.dict_size, M, mxDOUBLE_CLASS, mxREAL);
	        plhs[1] = mxCreateNumericMatrix(knlms.dict_size, 1, mxDOUBLE_CLASS, mxREAL);
            #endif
            CopyMatrix(knlms.x, N, (MATHTYPE_T *)mxGetData(plhs[0]), knlms.dict_size, knlms.dict_size, M);
            CopyVector(knlms.alpha, 1, (MATHTYPE_T *)mxGetData(plhs[1]), 1, knlms.dict_size);
            FreeKNLMSState(&knlms);
            break;
        case ALDKRLS:
            if(nrhs != 7) mexErrMsgTxt("Invalid input size for ALDKRLS in training mode."); //(mode, alg, x', M, sigma, nu, y)
            if(nlhs != 4) mexErrMsgTxt("Invalid output size for ALDKRLS in training mode."); //[D, alpha, K_inv, P]
            InitialiseALDKRLSState(&aldkrls, N, M, *(MATHTYPE_T *)mxGetData(prhs[5]), *(MATHTYPE_T *)mxGetData(prhs[4]));
            y = (MATHTYPE_T *)mxGetData(prhs[6]);
            for(i = 0; i < Nset; ++i) aldkrls_train(x + i*M, y[i], &aldkrls);
            #if SINGLE_MATH
	        plhs[0] = mxCreateNumericMatrix(aldkrls.dict_size, M, mxSINGLE_CLASS, mxREAL);
	        plhs[1] = mxCreateNumericMatrix(aldkrls.dict_size, 1, mxSINGLE_CLASS, mxREAL);
	        plhs[2] = mxCreateNumericMatrix(aldkrls.dict_size, aldkrls.dict_size, mxSINGLE_CLASS, mxREAL);
	        plhs[3] = mxCreateNumericMatrix(aldkrls.dict_size, aldkrls.dict_size, mxSINGLE_CLASS, mxREAL);
            #elif DOUBLE_MATH
	        plhs[0] = mxCreateNumericMatrix(aldkrls.dict_size, M, mxDOUBLE_CLASS, mxREAL);
	        plhs[1] = mxCreateNumericMatrix(aldkrls.dict_size, 1, mxDOUBLE_CLASS, mxREAL);
	        plhs[2] = mxCreateNumericMatrix(aldkrls.dict_size, aldkrls.dict_size, mxDOUBLE_CLASS, mxREAL);
	        plhs[3] = mxCreateNumericMatrix(aldkrls.dict_size, aldkrls.dict_size, mxDOUBLE_CLASS, mxREAL);
            #endif
            CopyMatrix(aldkrls.x, N, (MATHTYPE_T *)mxGetData(plhs[0]), aldkrls.dict_size, aldkrls.dict_size, M);
            CopyVector(aldkrls.alpha, 1, (MATHTYPE_T *)mxGetData(plhs[1]), 1, aldkrls.dict_size);
            CopyMatrix(aldkrls.K_inv, aldkrls.dict_size, (MATHTYPE_T *)mxGetData(plhs[2]), aldkrls.dict_size, aldkrls.dict_size, aldkrls.dict_size);
            CopyMatrix(aldkrls.P, aldkrls.dict_size, (MATHTYPE_T *)mxGetData(plhs[3]), aldkrls.dict_size, aldkrls.dict_size, aldkrls.dict_size);
            FreeALDKRLSState(&aldkrls);
            break;
        default:
            break;
        }
    }
    else mexErrMsgTxt("Unsupported mode");
}

#else /*!defined(GENERIC_INTERFACE)*/

#define REGULARIZATION_VAL 1E-4
#define ALD_THRESH 1E-2
#define SIGMA 0.6
#define ETA 0.1
#define EPS 1E-4
#define MU0 0.9

//Mex function version of a time series version of swkrls.
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    const mxArray *xmx;
    MATHTYPE_T *x;
    MATHTYPE_T *y;
    int Ntrain,i;
    #if defined (KNLMS_TRAIN)
    knlms_state_t state;
    #elif defined (FBKRLS_TRAIN)
    fbkrls_state_t state;
    #elif defined (ALDKRLS_TRAIN)
    aldkrls_state_t state;
    #else /*SWKRLS_TRAIN*/
    swkrls_ts_state_t state;
    #endif
    #ifdef MCA_ON
    int mca_on;
    #endif

    #ifdef MCA_ON
    mca_on = 1;
    MCALIB_MODE=1;//MCALIB_IEEE  //Turn off monte carlo arithmetic for function setup.
    MCALIB_T = 53;     //Default value for double precision.
    #endif

    // nlhs -> number of outputs
    // nrhs -> number of inputs
    if(nrhs < 1)
        mexErrMsgTxt("Too few inputs.");
    else if(nrhs > 3)
        mexErrMsgTxt("Too many inputs.");
    if(nlhs < 1)
        mexErrMsgTxt("Too few outputs.");
    else if(nlhs > 1)
        mexErrMsgTxt("Too many outputs.");

    // Get MX arrays from input.
    xmx = prhs[0];
    x = (MATHTYPE_T *)mxGetData(prhs[0]);

    // Get parameters for the latest input.
    Ntrain = mxGetM(xmx); //Find the length of the series.

    //Typecheck inputs.
    #if SINGLE_MATH
    if(!mxIsSingle(prhs[0]))
        mexErrMsgTxt("Input (x) is not of type single.");
    #elif DOUBLE_MATH
    if(!mxIsDouble(prhs[0]))
        mexErrMsgTxt("Input (x) is not of type double.");
    #else
    mexErrMsgTxt("Cannot process input type.");
    #endif

    #ifdef MCA_ON
    //A value for precision is given from matlab. Type check, then update.
    if(nrhs == 3) {
        //Get t value.
        if(!mxIsInt32(prhs[1]))
            mexErrMsgTxt("Input (t) is not of type int32.");
        if((mxGetM(prhs[1]) != 1) || (mxGetN(prhs[1]) != 1))
            mexErrMsgTxt("Input (t) is not a scalar.");
        MCALIB_T = *(int *)mxGetData(prhs[1]); //Get t.
        if(MCALIB_T < 1)
            mexErrMsgTxt("Invalid value for precision.");

        //Get MCA Value.
        if(!mxIsInt32(prhs[2]))
            mexErrMsgTxt("Input (mca) is not of type int32.");
        if((mxGetM(prhs[2]) != 1) || (mxGetN(prhs[1]) != 1))
            mexErrMsgTxt("Input (mca) is not a scalar.");
        mca_on = *(int *)mxGetData(prhs[2]); //Get t.
    }
    #endif

    //Allocate output vector.
    #if SINGLE_MATH
	plhs[0] = mxCreateNumericMatrix(Ntrain, 1, mxSINGLE_CLASS, mxREAL);
    #elif DOUBLE_MATH
	plhs[0] = mxCreateNumericMatrix(Ntrain, 1, mxDOUBLE_CLASS, mxREAL);
    #endif
    y = (MATHTYPE_T *)mxGetData(plhs[0]);

    //Initialise the swkrls state object.
    #if defined (KNLMS_TRAIN)
        #ifdef DYN_MEM
        InitialiseKNLMSState(&state, WINDOW_LENGTH, EMBED_LENGTH, ETA, EPS, MU0, SIGMA);
        #else
        InitialiseKNLMSState(&state, ETA, EPS, MU0, SIGMA);
        #endif
    #elif defined (FBKRLS_TRAIN)
        #ifdef DYN_MEM
        InitialiseFBKRLSState(&state, WINDOW_LENGTH, EMBED_LENGTH, REGULARIZATION_VAL, SIGMA);
        #else
        InitialiseFBKRLSState(&state, REGULARIZATION_VAL, SIGMA);
        #endif
    #elif defined (ALDKRLS_TRAIN)
        #ifdef DYN_MEM
        InitialiseALDKRLSState(&state, WINDOW_LENGTH, EMBED_LENGTH, ALD_THRESH, SIGMA);
        #else
        InitialiseALDKRLSState(&state, ALD_THRESH, SIGMA);
        #endif
    #else /*SWKRLS_TRAIN*/
        #ifdef DYN_MEM
        InitialiseState(&state, WINDOW_LENGTH, EMBED_LENGTH, REGULARIZATION_VAL, SIGMA);
        #else
        InitialiseState(&state, REGULARIZATION_VAL, SIGMA);
        #endif
    #endif

    #ifdef MCA_ON
    if(mca_on)
        MCALIB_MODE=2;//MCALIB_MCA
    else
        MCALIB_MODE=3;//MCALIB_MP
    #endif

    //Predict the time series using SW-KRLS with 1 step prediction.
    for(i = 0; i < Ntrain-1; ++i) {
        //Call the function to process the input.
        #if defined (KNLMS_TRAIN)
        y[i+1] = knlms_ts_wrap(x[i], &state);
        #elif defined (FBKRLS_TRAIN)
        y[i+1] = fbkrls_ts_wrap(x[i], &state);
        #elif defined (ALDKRLS_TRAIN)
        y[i+1] = aldkrls_ts_wrap(x[i], &state);
        #else
        y[i+1] = swkrls_ts(x[i], &state);
        #endif
    }

    #ifdef DYN_MEM
    #if defined (KNLMS_TRAIN)
    FreeKNLMSState(&state);
    #elif defined (FBKRLS_TRAIN)
    FreeFBKRLSState(&state);
    #elif defined (ALDKRLS_TRAIN)
    FreeALDKRLSState(&state);
    #else
    FreeState(&state);
    #endif
    #endif

}
#endif /*GENERIC_INTERFACE*/

