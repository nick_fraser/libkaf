
#ifndef _CKAF_H

#include "defines.h"
#include "swkrls.h"
#include "knlms.h"
#include "fbkrls.h"
#include "aldkrls.h"
#include "linear_math.h"

#ifndef DYN_MEM
#error "The ckaf command line program must be compiled using the DYN_MEM symbol. Clean directory and recompile using 'make ckaf C_FLAGS=-DDYN_MEM'"
#endif /*DYN_MEM*/

#define STRING_BUFFER_SIZE 256

enum CKAF_ALGORITHM {
    CKAF_SWKRLS = 0,
    CKAF_FBKRLS,
    CKAF_ALDKRLS,
    CKAF_KNLMS,
    CKAF_NULL
};

enum CKAF_INPUT_MODE {
    CKAF_STD = 0,
    CKAF_TS,
    CKAF_MDTS
};

enum CKAF_INPUT_TYPE {
    CKAF_TXT = 0,
    CKAF_BIN
};

struct ckaf_state {
    enum CKAF_ALGORITHM mode;
    void * state;
    void (*train)(MATHTYPE_T *, MATHTYPE_T, void *);
    MATHTYPE_T (*predict)(MATHTYPE_T *, void *);
    void (*free)(void *);
};

#endif /*_CKAF_H*/

