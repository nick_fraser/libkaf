function y = process_knlms(x, varargin)
%function which simulates how the mex file will process/predict the time series.

Ntrain = size(x,1)-1;
embed = 7;
y = zeros(Ntrain+1,1);         % Initialise estimation for y.

%%%%%%%%%%%%%%Initialise function and kernel parameters.
pars.kernel.type = 'gauss';	% ÂºÃÂµÃÃÃ Ã?Ã?kernel type
pars.kernel.par = .6;       % ÂºÃÂµÃÂ²ÃÃÃ½kernel parameter (width in case of Gaussian kernel)
pars.M = 20;                % ÃÃ®Â´Ã³Â´ÃÂµÃ¤Â¹Ã¦ÃÂ£maximum dictionary size

% ÃÂ¹ÃÃÃÂµÃÃ·Â£ÂºÂ½Ã¢ÃÂ¢ÃÃ?ÃÃÃ?ÃÃÃ¤ÃÃÃÃ?ÂµÃÃÂ»ÃÃÂ£Â¬Â²Â¢ÃÃÃÃÃ?Ã? INSTRUCTIONS: UNCOMMENT ONLY ONE OF THE FOLLOWING CONFIGURATIONS AND RUN

pars.name = 'KNLMS';  % ÃÂ¡Â¶Â¨Â»Â¬Â´Â°ÂµÃ?Â¹Ã©ÃÃ®Ã?Â¡Â¶Ã¾Â³Ã
pars.algo = @kaf_knlms;%
pars.c = 1E-4;          %
pars.eta = 0.1;
pars.mu0 = 0.9;

state_fifo_test = 0; %perform the state fifo test.

if length(varargin) ~= 0
    if rem(length(varargin),2) ~= 0
        error('nargin needs to be in ''option'',value pairs. length(varargin) = %d.', length(varargin));
    end
    for i = 1:2:length(varargin)
        switch(varargin{i})
            case 'N'
                pars.M = varargin{i+1};
            case 'embed'
                embed = varargin{i+1};
            otherwise
                %fprintf('Unrecognised command line option, %s, continuing.\n', varargin{i});
        end
    end
end

state.pars = pars;
state.vars = [];

if state_fifo_test == 1
    lag = 1;
    x_train = []; % Prepare the training data for a state-fifo test.
    for i=0:(embed-1)
        x_tmp = [zeros(embed-i,1); x(1:((Ntrain+1)-(embed-i)),1)];
        x_train = [x_train x_tmp];
    end
    states = repmat(state, lag, 1);
else
    state.x = zeros(1,embed); %Use the time series version.
end

for i=1:Ntrain,
	%if ~mod(i,Ntrain/10), fprintf('.'); end
	state.vars.t = i;
	
    if state_fifo_test == 1
        states(1).vars.t = i;
        states(1).vars = kaf_knlms(states(1).vars, states(1).pars, x_train(i,:), x(i));
        y(i+1) = kaf_knlms(states(1).vars, states(1).pars, x_train(i+1,:));
        if lag ~= 1
            states = [states(2:end,1); states(1)];
        end
    else
	    [y(i+1),state] = knlms_wrap(x(i),state);    %  ÃÂµÃ?Â· Â·ÂµÂ»Ã mem ymem basis K_inv alpha t Â³ÃµÃÂ¼Â»Â¯
    end

end


