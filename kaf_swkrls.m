function out = kaf_swkrls(vars,pars,x,y)
% KAF_SWKRLS implements the basic iteration of the sliding-window kernel recursive least-squares (SW-KRLS) algorithm. This implementation includes a FLOPS and BYTES count for the training procedure.
% KAF_SWKRLS实现了 滑窗核递归最小二乘 算法的基本迭代过程，此实现包括了训练过程的浮点、定点运算量
% Input:	- vars: structure containing the used variables
%			- pars: structure containing kernel and algorithm parameters
%			- x: matrix containing input vectors as rows
%			- y: vector containing output values
% 输入参数:	- vars: 包含使用的变量的结构体
%			- pars: 包含算法参数和核的结构体
%			- x: 包含输入向量的矩阵，以行的形式
%			- y: 包含输出值的向量
% Output:	- out: contains the new vars if the algorithm is in learning mode (when x and y are provided) and the estimated output when the algorithm is being evaluated (when only x is given).
% 输出:      - out:包含新的变量如果算法处于学习模式（当x和y都提供时）和估计输出当算法被评测时（当只提供x的时候）
% Dependencies: kaf_kernel.% 函数依赖: kaf_kernel.
% USAGE: out = kaf_swkrls(vars,pars,x,y)% 用法：out = kaf_swkrls(vars,pars,x,y)

% 读入参数 read parameters
ktype = pars.kernel.type;   % "Gauss"
kpar = pars.kernel.par;     % 0.6
len = pars.M;               % 20
reg = pars.c;               % 正则化常量 1E-4 regularization constant

if nargin<4,
    mode = 'eval';
else
    if ~isfield(vars,'basis')
        mode = 'init';
    else
        mode = 'train';
    end
end

switch mode
    case 'init'         % 初始化 initialize
        % 初始化算法 initialize algorithm
        mem = x;        % 1X7
        basis = 1;
        ymem = y;       % 1X1 1.2307
        k = kaf_kernel(mem,x,ktype,kpar);
        knn = k + reg;  % 1X1 + 1X1 1+0.00001
        K_inv = 1/knn;  % 0.999999
        alpha = K_inv*ymem;% 1.2306
        
        % 浮点数出 flops count
        %fl_reg = kaf_flops('sum',1);
        %fl_Kinv = kaf_flops('div',1);
        %fl_alpha = kaf_flops('mult',1);
        %fl = fl_k + fl_reg + fl_Kinv + fl_alpha;
        
        % 写变量 write variables
        vars.mem = mem;     % 1X7
        vars.basis = basis; % 1X1 1
        vars.ymem = y;      % 1X1 1.2307
        vars.K_inv = K_inv; % 1X1 0.9999
        vars.alpha = alpha; % 1X1 1.2306
        %vars.flops = fl;    %
        
        % 存储在内存的 bytes bytes stored in memory
        %bytes_mem = kaf_bytes('double',size(x,2)); % 数据被认为是双精度 data is assumed to be double-precision
        %bytes_ymem = kaf_bytes('double',1);
        %bytes_alpha = kaf_bytes('double',1);
        %bytes_K_inv = kaf_bytes('double',1);
        %vars.bytes = bytes_mem + bytes_ymem + bytes_alpha + bytes_K_inv;
        
        out = vars;
    case 'eval'		% 评测 evaluate
        mem = vars.mem;     %20X7
        alpha = vars.alpha; %20X1
        
        K = kaf_kernel(x,mem,ktype,kpar);	% kaf_kernel(100X7, 20X7, "Gauss", "0.6")k -> 100X20,可以被加速could be sped up
        y = K*alpha;                        % K 100X20 alpha 20x1 -> 100X1
        
        %s=size(K,1);                        %%%%%%%%%%%%%Vectorization start 
        %ynew=zeros(s,1);                    
        %for n=1:s
        %    ynew(n)=dot(K(n,:),alpha);
        %end                                 %%%%%%%%%%%%%Vectorization end 
        
        out = y; 
    case 'train'            % 训练 train
        % 读入变量 read variables
        mem = vars.mem;     % 20X7      
        ymem = vars.ymem;   % 20X1          
        K_inv = vars.K_inv; % 20X20
        basis = vars.basis; % 1X20
        t = vars.t;% 1x1
        
        % 扩展词典 expand dictionary
        mem = [mem; x];     % 20X7 -> 21X7 矩阵 x应该只包含一个采样 （列）x should contain only one sample (column)
        ymem = [ymem; y];	% 20X1 -> 21X1 向量 存储子系列标志 store subset labels
        basis = [basis t];  % 1X20 -> 1x21 向量
        
        k = kaf_kernel(mem,x,ktype,kpar);% kaf_kernel(21X7, 1X7, "Gauss", "0.6")k -> 21X1向量
        kn = k(1:size(mem,1)-1);                % k 21X1 -> kn 20X1 去掉了最后一行
        knn = k(end) + reg;                     % knn 数
        %fl_reg = kaf_flops('sum',1);            % f1_reg 数
        K_inv = inverse_addrowcol(kn,knn,K_inv);	% 实际给入 K 的逆 输出 K 输入 K_inv 20X20矩阵 输出 21x21 扩展核矩阵 extend kernel matrix
        
        m = size(mem,1);
        if (m>len)          %len=20,m=21
            % 修剪字典 prune dictionary
            mem(1,:) = [];  %21X7 -> 20X7
            ymem(1) = [];   %21X1 -> 20X1
            K_inv = inverse_removerowcol(K_inv);  % K_inv 21X21 -> K_inv 20X20
            %fl_inv = fl_inv + fl_inv2;
            basis(1) = [];  %1X21 -> 1X20 去掉了第一行
            m = m-1;
        end
        alpha = K_inv*ymem; %20x20 * 20x1 -> 20X1
        
        %s=size(K_inv,1);    %%%%%%%%%%%%%Vectorization start       
        %alphanew=zeros(s,1);
        %for n=1:s
        %    alphanew(n)=dot(K_inv(n,:),ymem);
        %end                 %%%%%%%%%%%%%Vectorization end 
        
        %fl_alpha = kaf_flops('mult',m,m,1);
        
        % 浮点数量 flops count
        %fl = fl_k + fl_reg + fl_inv + fl_alpha;
        
        % 写变量 write variables
        vars.K_inv = K_inv;     %每次训练之后更新K_inv  20X20
        vars.alpha = alpha;     %每次训练之后更新alpha  20X1
        vars.mem = mem;         %每次训练之后更新mem    20X7
        vars.ymem = ymem;       %每次训练之后更新ymem   20X1
        vars.basis = basis;     %每次训练之后更新basis  1X20
        %vars.flops = fl;       
        
        % 存储于内存的 bytes bytes stored in memory
        %bytes_K_inv = kaf_bytes('double',[m,m],'symm');
        %bytes_alpha = kaf_bytes('double',m);
        %bytes_mem = kaf_bytes('double',[size(x,2),m]); % data is assumed to be double-precision
        %bytes_ymem = kaf_bytes('double',m);
        %vars.bytes = bytes_K_inv + bytes_alpha + bytes_mem + bytes_ymem;
        
        out = vars;
end

function K_inv = inverse_addrowcol(b,d,A_inv)
% returns the inverse matrix of K = [A b;b' d] and the flops required http://www.squobble.com/academic/swkrls/node15.html
% 返回矩阵K = [A b;b' d]的逆矩阵，以及浮点运算次数 http://www.squobble.com/academic/swkrls/node15.html
g_inv = d - b'*A_inv*b; % 1X1 - 1x20 * 20x20 * 20x1 

%s=size(A_inv,1);            %%%%%%%%%%%%%Vectorization start 
%g_invm=zeros(s,1);
%for n=1:s;
%    g_invm(n)=dot(A_inv(n,:),b);
%end
%g_invnew=d-dot(b',g_invm);  %%%%%%%%%%%%%Vectorization end 

g = 1/g_inv;                % 1X1 
f = -A_inv*b*g;             % 20X20 * 20X1 * 1X1 -> 20X1

%fnew=-g_invm*g;             %%%%%%%%%%%%%Vectorization one line 

E = A_inv - A_inv*b*f';     % 20X20 - 20X20 * 20X1 * 1X20 -> 20X20

%Em=zeros(s,s);              %%%%%%%%%%%%%Vectorization start 
%for n=1:n
%    Em(:,n)=g_invm*f(n);
%end
%Enew =A_inv-Em;             %%%%%%%%%%%%%Vectorization end 

K_inv = [E f;f' g];         % K_inv 21X21

% 浮点运算次数 flops count 
m = size(A_inv,1);
%fl_ginv = kaf_flops('sum',1+m*(m+1)/2) + kaf_flops('mult',m*(m+1)); % multiplication is symmetric
%fl_g = kaf_flops('div',1);
%fl_f = kaf_flops('mult',m,m,1) + kaf_flops('mult',m);
%fl_E = kaf_flops('sum',m*(m+1)/2) + kaf_flops('mult',m*(m+1)/2); % multiplication is only for f, and mxm matrices are symmetric
%fl = fl_ginv + fl_g + fl_f + fl_E;

function D_inv = inverse_removerowcol(K_inv)
% calculates the inverse of D with K = [a b';b D] and the flops required http://www.squobble.com/academic/swkrls/node16.html
% 返回矩阵D的逆矩阵在K = [A b;b' D]的条件下，以及浮点运算次数 http://www.squobble.com/academic/swkrls/node16.html
m = size(K_inv,1);  % m 1X1 =21

G = K_inv(2:m,2:m); % G 20X20
f = K_inv(2:m,1);   % f 20X1 除首个元素 的第一列
e = K_inv(1,1);     % e 1X1

D_inv = G - f*f'/e; % D_inv 20X20

%s=size(f,1);        %%%%%%%%%%%%%Vectorization start 
%D_invm=zeros(s,s);
%for n=1:s
%    D_invm(:,n)=f*f(n);
%end
%D_invnew=G-D_invm/e;%%%%%%%%%%%%%Vectorization end 

% flops count浮点运算次数
%fl = kaf_flops('sum',m*(m+1)/2) + kaf_flops('mult',m*(m-1)/2) + kaf_flops('div',m-1); % G is symmetric

