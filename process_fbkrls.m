function y = process_fbkrls(x, varargin)
%function which simulates how the mex file will process/predict the time series.

Ntrain = size(x,1)-1;
embed = 7;
y = zeros(Ntrain+1,1);         % Initialise estimation for y.

%%%%%%%%%%%%%%Initialise function and kernel parameters.
pars.kernel.type = 'gauss';	% ºËµÄÀà�?�?kernel type
pars.kernel.par = .6;       % ºËµÄ²ÎÊýkernel parameter (width in case of Gaussian kernel)
pars.M = 20;                % ×î´ó´Êµä¹æÄ£maximum dictionary size

% Ê¹ÓÃËµÃ÷£º½â×¢Ê�?ÒÔ�?ÂÅäÖÃÖ�?µÄÒ»ÖÖ£¬²¢ÇÒÔË�?�? INSTRUCTIONS: UNCOMMENT ONLY ONE OF THE FOLLOWING CONFIGURATIONS AND RUN

pars.name = 'FB-KRLS';  % Ñ¡¶¨»¬´°µ�?¹é×î�?¡¶þ³Ë
pars.algo = @kaf_fbkrls;%
pars.c = 1E-4;          %
pars.mu = 0.0;

state_fifo_test = 0; %perform the state fifo test.

if length(varargin) ~= 0
    if rem(length(varargin),2) ~= 0
        error('nargin needs to be in ''option'',value pairs. nargin = %d.', nargin);
    end
    for i = 1:2:length(varargin)
        switch(varargin{i})
            case 'N'
                pars.M = varargin{i+1};
            case 'embed'
                embed = varargin{i+1};
            otherwise
                %fprintf('Unrecognised command line option, %s, continuing.\n', varargin{i});
        end
    end
end

state.pars = pars;
state.vars = [];
state.vars.mem = zeros(pars.M, embed);
state.vars.ymem = zeros(pars.M, 1);
state.vars.K_inv = eye(pars.M, pars.M)/(1 + pars.c);
state.vars.basis = zeros(1, pars.M);

if state_fifo_test == 1
    lag = 1;
    x_train = []; % Prepare the training data for a state-fifo test.
    for i=0:(embed-1)
        x_tmp = [zeros(embed-i,1); x(1:((Ntrain+1)-(embed-i)),1)];
        x_train = [x_train x_tmp];
    end
    states = repmat(state, lag, 1);
else
    state.x = zeros(1,embed); %Use the time series version.
end

for i=1:Ntrain,
	%if ~mod(i,Ntrain/10), fprintf('.'); end
	state.vars.t = i;
	
    if state_fifo_test == 1
        states(1).vars.t = i;
        states(1).vars = kaf_fbkrls(states(1).vars, states(1).pars, x_train(i,:), x(i));
        y(i+1) = kaf_fbkrls(states(1).vars, states(1).pars, x_train(i+1,:));
        if lag ~= 1
            states = [states(2:end,1); states(1)];
        end
    else
	    [y(i+1),state] = fbkrls_wrap(x(i),state);    %  Ñµ�?· ·µ»Ø mem ymem basis K_inv alpha t ³õÊ¼»¯
    end

end


