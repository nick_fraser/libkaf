function [output] = swkrls_sims(varargin)
%Run the sw-krls function. Allow several parameters to be controlled at runtime.
% Inputs come in 'option',value pairs. Below are a list of options as well as default values.
%   'mca_sims': Number of MCA simulations to run at each precision level. default = 1. Set to 0 to disable MCA.
%   't': Vector of different virtual precisions to use. default = [15:1:25]. Note that the mex file needs to be compiled with MCA enabled for this to work.
%   'csv': Print MSE to a csv file for a analysis in R? default = 0 (false).
%   'name': String to append to the csv filename. 'csv' option must be enabled. default = 'std'.

%%%% Set Default parameters.

%% Parameters for time series generation.

Ntrain = 3000;  % ѵ����ݵĸ���number of training data 
embed = 7;		% �������number of samples in time-embedding
step = 1;		% Ԥ�ⲽ��prediction step
N = 20; % Default window length.

%% Parameters for calling the mex file.
t = int32([15:1:25]);
mca_sims = 1;
mca = int32(1);
sim_type = 'mcasims';

%% Output parameters.
csv = 0;
mat = 0;
name = 'std';
alg='swkrls';
sng = 0;

%%%% Update defaults if specified by the user.
if nargin > 0
    if rem(nargin,2) ~= 0
        error('nargin needs to be in ''option'',value pairs. nargin = %d.', nargin);
    end
    for i = 1:2:nargin
        switch(varargin{i})
            case 't'
                t = int32(varargin{i+1});
            case 'mca_sims'
                mca_sims = varargin{i+1};
                if mca_sims == 0
                    mca = int32(0);
                    mca_sims = 1;
                    sim_type = 'norm';
                else
                    sim_type = 'mcasims';
                    mca = int32(1);
                end
            case 'csv'
                csv = varargin{i+1};
            case 'mat'
                mat = varargin{i+1};
            case 'name'
                name = varargin{i+1};
            case 'alg'
                alg = varargin{i+1};
            case 'sng'
                sng = varargin{i+1};
            case 'N'
                N = varargin{i+1};
            case 'embed'
                embed = varargin{i+1};
            case 'Ntrain'
                Ntrain = varargin{i+1};
            otherwise
                fprintf('Unrecognised command line option, %s, continuing.\n', varargin{i});
        end
    end
end

%Generate the time series.
tic
fprintf('Generating Mackey-Glass time series...\n');
x = gen_mg_series(Ntrain + embed);
fprintf('Mackey Glass Generation END.\n');
% Ӧ��KRLS apply KRLS
fprintf('Applying %s for %d-step prediction...\n', upper(alg), step);
y_est = zeros(Ntrain+1,1);         % Initialise estimation for y.
toc

tic;
switch(alg)
    case 'swkrls'
        y_est = process_swkrls(x(1:Ntrain+1), 'N', N, 'embed', embed);
    case 'fbkrls'
        y_est = process_fbkrls(x(1:Ntrain+1), 'N', N, 'embed', embed);
    case 'aldkrls'
        y_est = process_aldkrls(x(1:Ntrain+1), 'N', N, 'embed', embed);
    case 'knlms'
        y_est = process_knlms(x(1:Ntrain+1), 'N', N, 'embed', embed);
    otherwise
        error('%s is not a valid training algorithm.', alg);
end
%fprintf('\n');
toc;
if sng == 1
    x = single(x);
end
err = x(1:(Ntrain+1),1) - y_est;
mse = mean(err(end-500:end,1).^2);
fprintf('MATLAB version: Mean square error in the last 500 samples is %d.\n', mse);

%t = int32([15:1:25]);
%mca_sims = 40;
mca_mse = zeros(length(t),mca_sims);

filepref = ['output/' datestr(now, 'yyyymmddHHMM') '-t.' int2str(min(t)) '.' int2str(max(t)) '-' sim_type '.' int2str(int32(mca_sims)) '.' name];
filename = [filepref '.csv'];
if csv ~= 0 fid = fopen(filename, 'w'); end
if csv ~= 0 fprintf(fid, 'type, t, x\n'); end

for t_ind = 1:length(t)
    for i = 1:mca_sims
        tic;
        y_est_c = swkrls_c(x(1:Ntrain+1),t(1,t_ind),mca);
        toc;
        
        err_c = x(1:(Ntrain+1),1) - y_est_c;
        mse_c = mean(err_c(end-500:end,1).^2);
        
        fprintf('C Version: Mean square error in the last 500 samples is %d. t = %d.\n', mse_c, t(1,t_ind));
        if csv ~= 0 fprintf(fid, 'SW-KRLS, %d, %.20d\n', t(1,t_ind), mse_c); end
        mca_mse(t_ind,i) = mse_c;
    end
end

if csv ~= 0 fclose(fid); end

%%Prepare data to make scatter plot.
t_sca = repmat(t',mca_sims,1);

mse_sca = [];
for i = 1:mca_sims
    mse_sca = [mse_sca; mca_mse(:,i)];
end

%Find the change in mse for each precision.
dmse = abs(mse-mse_sca);

%% Print CSV data after the fact.
%fprintf(fid, 'SW-KRLS, %d, %.20d\n', [double(t_sca)'; mse_sca']);

%dmse_sca = [];
%for i = 1:mca_sims
%    dmse_sca = [dmse_sca; dmse(:,i)];
%end

%plot the output data.
%scatter(t_sca,log10(dmse_sca));

%% ������ OUTPUT

%% Return important values in a structure.

output.y_est = y_est;
output.x = x;
output.t = t;
output.mse = mse;
output.t_sca = t_sca;
output.mse_sca = mse_sca;
output.dmse_sca = dmse;

if mat ~= 0 save([filepref '.mat'], 'output'); end

end

%figure;plot(mg(1:500))
%title(sprintf('Mackey-Glass time series for tau=%d', tau));

%fprintf(1,'Mean MSE over last 500 steps = %.2d\n',mean(MSE(Ntrain-499:Ntrain)))
%fprintf(1,'Total flops over last 500 steps = %.2d\n\n',sum(all_fl(Ntrain-499:Ntrain)))

%figure;plot(all_fl);title('Flops')
%figure;plot(all_bytes);title('Bytes')

%h_mse=figure;semilogy(MSE)
%title('MSE')

%figure; hold on
%plot(y_test,'b')
%plot(y_est,'g')
%legend({'Test output','Estimated output'})

