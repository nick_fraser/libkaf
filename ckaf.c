
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ckaf.h"

//Explain how to ckaf.
void usage(void) {
    printf("Usage:\n");
    printf("ckaf -m=<feature length> -n=<dictionary length> -a=<swkrls|fbkrls|aldkrls|knlms> -s=<sigma> -l=<ts|std|mdts> [-e=<eta>] [-r=<regularisation>] [-t=<threshold>] [-i <input filename>] [-o <output filename>] [-d=<b|t>] [-c=<channels>]\n");
    exit(8);
}

//parse an int from the command line argument.
int parseInt(char * arg) {
    //Check that the string starts with an equals sign.
    #ifdef DEBUG_PARSING
    printf("%s\n", arg);
    #endif /*DEBUG_PARSING*/
    if(arg[0] != '=')
        return -1;
    return atoi(&arg[1]);
}

//parse a double from the command line argument.
double parseDouble(char * arg) {
    //Check that the string starts with an equals sign.
    #ifdef DEBUG_PARSING
    printf("%s\n", arg);
    #endif /*DEBUG_PARSING*/
    if(arg[0] != '=')
        return -1;
    return atof(&arg[1]);
}

//parse a string from the command line argument and return the correct enum.
enum CKAF_ALGORITHM getAlgorithm(char * arg) {
    //Check that the string starts with an equals sign.
    #ifdef DEBUG_PARSING
    printf("%s\n", arg);
    #endif /*DEBUG_PARSING*/
    if(arg[0] != '=')
        return CKAF_NULL;
    if(strcmp(&arg[1], "fbkrls") == 0)
        return CKAF_FBKRLS;
    else if(strcmp(&arg[1], "swkrls") == 0)
        return CKAF_SWKRLS;
    else if(strcmp(&arg[1], "aldkrls") == 0)
        return CKAF_ALDKRLS;
    else if(strcmp(&arg[1], "knlms") == 0)
        return CKAF_KNLMS;
    return CKAF_NULL;
}

int readInputs(MATHTYPE_T * x, unsigned long long length, FILE * input) {
    unsigned long long i;
    float y;
    char s[STRING_BUFFER_SIZE];
    char * tok;
    int ret = fscanf(input, "%s\n", s);
    if(ret < 1)
        return 0;
    for(i = 0; i < length; i++) {
        if(i == 0)
            tok = strtok(s, " ");
        else
            tok = strtok(NULL, " ");
        if(tok == NULL)
            return i;
        sscanf(tok, "%f", &y);
        x[i] = y;
    }
    return length;
}

//A command line interface for ckaf.
int main(int argc, char *argv[]) {
    //Algorithm parameters - default values.
    int dict_size = -1;
    int feature_length = -1;
    int channels = -1;
    MATHTYPE_T sigma = -1;
    MATHTYPE_T eta = -1;
    MATHTYPE_T epsilon = -1;
    MATHTYPE_T thresh = -1;

    //Program commands and switches.
    enum CKAF_INPUT_MODE input_mode = CKAF_STD;
    enum CKAF_ALGORITHM mode = CKAF_NULL;
    enum CKAF_INPUT_TYPE input_type = CKAF_TXT;
    FILE * input = stdin;
    FILE * output = stdout;
    char * file_in = NULL;
    char * file_out = NULL;

    //Print out usage if no command line arguments are provided.
    if(argc == 1)
        usage();

    //Parse the inputs.
    int argi = 1;
    while(argi < argc) {
        #ifdef DEBUG_PARSING
        printf("%s\n", argv[argi]);
        #endif /*DEBUG_PARSING*/
        if(argv[argi][0] != '-')
            usage();
        switch(argv[argi][1]) {
            case 'm':
                feature_length = parseInt(&argv[argi][2]);
                if(feature_length < 1) {
                    printf("Unable to parse argument '%s'\n", &argv[argi][0]);
                    usage();
                }
                break;
            case 'n':
                dict_size = parseInt(&argv[argi][2]);
                if(dict_size < 1) {
                    printf("Unable to parse argument '%s'\n", &argv[argi][0]);
                    usage();
                }
                break;
            case 'c':
                channels = parseInt(&argv[argi][2]);
                if(channels < 1) {
                    printf("Unable to parse argument '%s'\n", &argv[argi][0]);
                    usage();
                }
                break;
            case 'a':
                mode = getAlgorithm(&argv[argi][2]);
                if(mode == CKAF_NULL) {
                    printf("Unable to parse argument '%s'\n", &argv[argi][0]);
                    usage();
                }
                break;
            case 's':
                sigma = parseDouble(&argv[argi][2]);
                if(sigma < 0) {
                    printf("Unable to parse argument '%s'\n", &argv[argi][0]);
                    usage();
                }
                break;
            case 'l':
                if(argv[argi][2] == '=') {
                    if(strcmp(&argv[argi][3], "ts") == 0) {
                        input_mode = CKAF_TS;
                        break;
                    }
                    else if(strcmp(&argv[argi][3], "std") == 0) {
                        input_mode = CKAF_STD;
                        break;
                    }
                    else if(strcmp(&argv[argi][3], "mdts") == 0) {
                        input_mode = CKAF_MDTS;
                        break;
                    }
                }
                printf("Unable to parse argument '%s'\n", &argv[argi][0]);
                usage();
                break;
            case 'e':
                eta = parseDouble(&argv[argi][2]);
                if(eta < 0) {
                    printf("Unable to parse argument '%s'\n", &argv[argi][0]);
                    usage();
                }
                break;
            case 'r':
                epsilon = parseDouble(&argv[argi][2]);
                if(epsilon < 0) {
                    printf("Unable to parse argument '%s'\n", &argv[argi][0]);
                    usage();
                }
                break;
            case 't':
                thresh = parseDouble(&argv[argi][2]);
                if(thresh < 0) {
                    printf("Unable to parse argument '%s'\n", &argv[argi][0]);
                    usage();
                }
                break;
            case 'd':
                if(argv[argi][2] != '=') {
                    printf("Unable to parse argument '%s'\n", &argv[argi][0]);
                    usage();
                }
                if('t' == argv[argi][3])
                    input_type = CKAF_TXT;
                else if('b' == argv[argi][3])
                    input_type = CKAF_BIN;
                else {
                    printf("Unable to parse argument '%s'\n", &argv[argi][0]);
                    usage();
                }
                break;
            case 'i':
                argi++;
                file_in = &argv[argi][0];
                break;
            case 'o':
                argi++;
                file_out = &argv[argi][0];
                break;
            default:
                printf("Unable to parse argument '%s'\n", &argv[argi][0]);
                usage();
                break;
        }
        argi++;
    }

    //Open input and output files.
    if(input_type == CKAF_TXT)
        input = freopen(file_in, "r", input);
    else /*if(input_type == CKAF_BIN)*/
        input = freopen(file_in, "rb", input);
    if(input == NULL) {
        printf("Unable to open input file: %s", file_in);
        exit(8);
    }
    if(input_type == CKAF_TXT)
        output = freopen(file_out, "w", output);
    else /*if(input_type == CKAF_BIN)*/
        output = freopen(file_out, "wb", output);
    if(output == NULL) {
        printf("Unable to open output file: %s", file_out);
        exit(8);
    }

    struct ckaf_state state;
    //Initialise algorithm states.
    switch(mode) {
        case CKAF_SWKRLS:
            if(dict_size > 0 && feature_length > 0 && sigma >= 0 && epsilon >= 0) {
                state.mode = mode;
                state.train = swkrls_train;
                state.predict = swkrls_predict;
                state.free = FreeGenericState;
                state.state = malloc(sizeof(swkrls_generic_state_t));
                InitialiseGenericState(state.state, dict_size, feature_length, epsilon, sigma);
            }
            else {
                printf("Parameters not valid for SWKRLS algorithm.\n");
                usage();
            }
            break;
        case CKAF_FBKRLS:
            if(dict_size > 0 && feature_length > 0 && sigma >= 0 && epsilon >= 0) {
                state.mode = mode;
                state.train = fbkrls_train;
                state.predict = fbkrls_predict;
                state.free = FreeFBKRLSState;
                state.state = malloc(sizeof(fbkrls_state_t));
                InitialiseFBKRLSState(state.state, dict_size, feature_length, epsilon, sigma);
            }
            else {
                printf("Parameters not valid for FBKRLS algorithm.\n");
                usage();
            }
            break;
        case CKAF_ALDKRLS:
            if(dict_size > 0 && feature_length > 0 && sigma >= 0 && thresh >= 0) {
                state.mode = mode;
                state.train = aldkrls_train;
                state.predict = aldkrls_predict;
                state.free = FreeALDKRLSState;
                state.state = malloc(sizeof(aldkrls_state_t));
                InitialiseALDKRLSState(state.state, dict_size, feature_length, thresh, sigma);
            }
            else {
                printf("Parameters not valid for ALDKRLS algorithm.\n");
                usage();
            }
            break;
        case CKAF_KNLMS:
            if(dict_size > 0 && feature_length > 0 && sigma >= 0 && epsilon >= 0 && eta > 0 && thresh >= 0) {
                state.mode = mode;
                state.train = knlms_train;
                state.predict = knlms_predict;
                state.free = FreeKNLMSState;
                state.state = malloc(sizeof(knlms_state_t));
                InitialiseKNLMSState(state.state, dict_size, feature_length, eta, epsilon, thresh, sigma);
            }
            else {
                printf("Parameters not valid for KNLMS algorithm.\n");
                usage();
            }
            break;
        default:
            printf("Unsupported algorithm.\n");
            usage();
            break;
    }

    //Read single lines containing floating point numbers, and write prediction to output.
    unsigned long long i = 0;
    MATHTYPE_T * x;

    //GENERIC MODE.
    if(input_mode == CKAF_STD) {
        x = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(feature_length+1));
        if(input_type == CKAF_TXT) {
            while(1) {
                int ret = readInputs(x, feature_length+1, input);
                MATHTYPE_T y;
                if(ret != (feature_length+1)) {
                    break;
                }
                y = state.predict(x, state.state);
                state.train(x, x[feature_length], state.state);
                fprintf(output, "%f\n", y);
                i++;
            }
        }
        else /*if(input_type == CKAF_BIN)*/ {
            while(1) {
                int ret = fread(x, sizeof(MATHTYPE_T), feature_length+1, input);
                MATHTYPE_T y = 0.0;
                if(ret != (feature_length+1)) {
                    break;
                }
                y = state.predict(x, state.state);
                state.train(x, x[feature_length], state.state);
                fwrite(&y, sizeof(y), 1, output);
                i++;
            }
        }
    }

    //MULTICHANNEL TIME SERIES MODE.
    else if(input_mode == CKAF_MDTS) {
        int delays = feature_length/channels;
        if(feature_length%channels || channels < 1) {
            printf("Number of channels must be a positive value. Feature length must be divisible by channels.\n");
            usage();
        }
        x = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(feature_length+channels));
        MATHTYPE_T * xbar = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(channels));
        if(input_type == CKAF_TXT) {
            while(1) {
                MATHTYPE_T y;
                int ret = readInputs(xbar, channels, input);
                //fprintf(stderr, "here.\n");
                if(ret != channels) {
                    break;
                }
                ShiftVecIn(x, feature_length+channels, xbar, channels);
                if(i < delays)
                    y = 0.0;
                else {
                    state.train(x, x[feature_length], state.state);
                    y = state.predict(&x[channels], state.state);
                }
                fprintf(output, "%f\n", y);
                i++;
            }
        }
        else /*if(input_type == CKAF_BIN)*/ {
            while(1) {
                MATHTYPE_T y;
                int ret = fread(xbar, sizeof(y), channels, input);
                if(ret != channels) {
                    break;
                }
                ShiftVecIn(x, feature_length+channels, xbar, channels);
                if(i < delays)
                    y = 0.0;
                else {
                    state.train(x, x[feature_length], state.state);
                    y = state.predict(&x[channels], state.state);
                }
                fwrite(&y, sizeof(y), 1, output);
                i++;
            }
        }
        free(xbar);
    }

    //TIME SERIES MODE.
    else /*if(input_mode == CKAF_TS)*/ {
        x = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(feature_length+1));
        if(input_type == CKAF_TXT) {
            while(1) {
                float y;
                int ret = fscanf(input, "%f\n", &y);
                if(ret < 1) {
                    break;
                }
                ShiftVec(x, feature_length+1, y); //Shift x by one.
                if(i < feature_length) //We haven't built a single training example yet.
                    fprintf(output, "%f\n", 0.0);
                else {
                    state.train(x, x[feature_length], state.state);
                    fprintf(output, "%f\n", state.predict(&x[1], state.state));
                }
                i++;
            }
        }
        else /*if(input_type == CKAF_BIN)*/ {
            while(1) {
                MATHTYPE_T y;
                int ret = fread(&y, sizeof(y), 1, input);
                if(ret != 1) {
                    break;
                }
                ShiftVec(x, feature_length+1, y); //Shift x by one.
                y = 0.0;
                if(i < feature_length) { //We haven't built a single training example yet.
                    #ifdef PRINT_RESIDUAL
                    #ifdef SINGLE_MATH
                    int * yptr = (int *)&y;
                    int * xptr = (int *)&x[feature_length];
                    int res = *yptr ^ *xptr;
                    #else /*DOUBLE_MATH*/
                    long * yptr = (long *)&y;
                    long * xptr = (long *)&x[feature_length];
                    long res = *yptr ^ *xptr;
                    #endif /*DOUBLE_MATH*/
                    fwrite(&res, sizeof(res), 1, output);
                    #else /*PRINT_PREDICTION*/
                    fwrite(&y, sizeof(y), 1, output);
                    #endif /*PRINT_RESIDUAL*/
                }
                else {
                    state.train(x, x[feature_length], state.state);
                    y = state.predict(&x[1], state.state);
                    #ifdef PRINT_RESIDUAL
                    #ifdef SINGLE_MATH
                    int * yptr = (int *)&y;
                    int * xptr = (int *)&x[feature_length];
                    int res = *yptr ^ *xptr;
                    #else /*DOUBLE_MATH*/
                    long * yptr = (long *)&y;
                    long * xptr = (long *)&x[feature_length];
                    long res = *yptr ^ *xptr;
                    #endif /*DOUBLE_MATH*/
                    fwrite(&res, sizeof(res), 1, output);
                    #else /*PRINT_PREDICTION*/
                    fwrite(&y, sizeof(y), 1, output);
                    #endif /*PRINT_RESIDUAL*/
                }
                i++;
            }
        }
    }

    //Free unused variables.
    state.free(state.state);
    free(x);
    fclose(input);
    fclose(output);

    return 0;
}

