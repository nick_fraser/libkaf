#/bin/bash
#Test using single precision floating point, fbkrls for prediction, libmath for the exponential function, standalone functions for linear algebra and dynamic memory for the state variable.
make clean; make swkrls_c PRECISION=float EXP=orig C_FLAGS='-DMSE_TEST -DFBKRLS_TRAIN -DDYN_MEM'
