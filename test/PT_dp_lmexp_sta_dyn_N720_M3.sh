#!/bin/bash
echo "Test using double precision floating point, libmath for the exponential, standanlone function for linear algebra."
echo "WINDOW_LENGTH=720, EMBED_LENGTH=3"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=double EXP=orig PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=720 -DEMBED_LENGTH=3 -DDYN_MEM -DNUM_LOOPS=1' > /dev/null 2>&1
./swkrls_c

