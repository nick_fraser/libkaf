#!/bin/bash
echo "Test using double precision floating point, libmath for the exponential, lapack for linear algebra."
make clean > /dev/null 2>&1
make swkrls_c PRECISION=double EXP=orig LIN=lapack PERF_TEST=true > /dev/null 2>&1
./swkrls_c

echo "Test using double precision floating point, libmath for the exponential, standanlone function for linear algebra. Also uses dynamic memory."
make clean > /dev/null 2>&1
make swkrls_c PRECISION=double EXP=orig PERF_TEST=true C_FLAGS='-DDYN_MEM' > /dev/null 2>&1
./swkrls_c

echo "Test using double precision floating point, libmath for the exponential, standanlone function for linear algebra."
make clean > /dev/null 2>&1
make swkrls_c PRECISION=double EXP=orig PERF_TEST=true > /dev/null 2>&1
./swkrls_c

echo "Test KNLMS for training, using double precision floating point, libmath for the exponential, standanlone function for linear algebra."
make clean > /dev/null 2>&1
make swkrls_c PRECISION=double EXP=orig PERF_TEST=true C_FLAGS='-DKNLMS_TRAIN' > /dev/null 2>&1
./swkrls_c

echo "Test KNLMS for training, using double precision floating point, libmath for the exponential, standanlone function for linear algebra. Also uses dynamic memory."
make clean > /dev/null 2>&1
make swkrls_c PRECISION=double EXP=orig PERF_TEST=true C_FLAGS='-DKNLMS_TRAIN -DDYN_MEM' > /dev/null 2>&1
./swkrls_c

echo "Test using single precision floating point, libmath for the exponential, atlas for linear algebra."
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true LIN=atlas > /dev/null 2>&1
./swkrls_c

echo "Test using single precision floating point, libmath for the exponential, lapack for linear algebra."
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true LIN=lapack > /dev/null 2>&1
./swkrls_c

echo "Test using single precision floating point, libmath for the exponential, atlas with multithreading for linear algebra."
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true LIN=ptatlas > /dev/null 2>&1
./swkrls_c

echo "Test using single precision floating point, libmath for the exponential, standanlone function for linear algebra. Also uses dynamic memory."
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true C_FLAGS='-DDYN_MEM' > /dev/null 2>&1
./swkrls_c

echo "Test using single precision floating point, libmath for the exponential, standanlone function for linear algebra."
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true > /dev/null 2>&1
./swkrls_c

echo "Test KNLMS for training, using single precision floating point, libmath for the exponential, standanlone function for linear algebra. Also uses dynamic memory."
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true C_FLAGS='-DDYN_MEM -DKNLMS_TRAIN' > /dev/null 2>&1
./swkrls_c

echo "Test KNLMS for training, using single precision floating point, libmath for the exponential, standanlone function for linear algebra."
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true C_FLAGS='-DKNLMS_TRAIN' > /dev/null 2>&1
./swkrls_c

echo "Test using single precision floating point, direct look up table for the exponential, standanlone function for linear algebra."
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=lut PERF_TEST=true > /dev/null 2>&1
./swkrls_c

echo "Test using single precision floating point, look up table with linear interpolation for the exponential, standanlone function for linear algebra."
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=lutli PERF_TEST=true > /dev/null 2>&1
./swkrls_c

echo "Test using single precision floating point, standalone polynomial for the exponential, standanlone function for linear algebra."
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=poly PERF_TEST=true > /dev/null 2>&1
./swkrls_c

