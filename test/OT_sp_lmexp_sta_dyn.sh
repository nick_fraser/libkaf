#/bin/bash
#Test using single precision floating point, libmath for the exponential, standanlone function for linear algebra. Also uses dynamic memory. Need to test also with valgrind.
make clean; make swkrls_c PRECISION=float EXP=orig C_FLAGS='-DMSE_TEST -DDYN_MEM'
