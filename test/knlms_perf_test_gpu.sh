#!/bin/bash
cat /proc/cpuinfo | grep "model name" | head -n 1
cat /proc/meminfo | grep "MemTotal:"
cat /proc/version
lshw -numeric -C display

make clean > /dev/null 2>&1
make paramSearchGpu LIN=sta PRECISION=float EXP=orig PERF_TEST=true TIMER=lp C_FLAGS="-DWINDOW_LENGTH=16 -DEMBED_LENGTH=8 -lcuda" > /dev/null 2>&1
echo "Test using LIN=sta"
./paramSearchGpu

