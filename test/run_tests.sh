#!/bin/bash

fail_count=0
test_count=0

for f in test/OT*.sh
do
    test_count=$((test_count + 1))
    out_file=${f%.*}.out
    ref_file=${f%.*}.ref
    diff_file=${f%.*}.diff
    echo -ne $test_count". Running test $f... "
    ret=$($f > /dev/null 2>&1)
    if [ ${?} -ne 0 ]; then
        echo "COMPILATION FAILED"
        fail_count=$((fail_count + 1))
    else
        ./swkrls_c > $out_file 2> /dev/null
        diff $out_file $ref_file > $diff_file 2> /dev/null
        if [ ${?} -eq 0 ]; then
            echo "PASSED"
        else
            echo "OUTPUT FAILED"
            fail_count=$((fail_count + 1))
        fi
    fi
done
echo
if [ $fail_count -eq 0 ]; then
    echo "-----------------"
    echo "All tests passed."
else
    echo "-----------------"
    echo $fail_count " tests failed."
fi

