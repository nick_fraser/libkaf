#/bin/bash
#Test using single precision floating point, libmath for the exponential, lapack for linear algebra.
make clean; make swkrls_c PRECISION=float EXP=orig LIN=lapack C_FLAGS='-DMSE_TEST'
