#!/bin/bash
#Helper script to check the system info on multiple platforms.
echo "System Info"
if [ "$(uname)" = "Darwin" ]; then
    system_profiler SPHardwareDataType
elif [ "$(expr substr $(uname -s) 1 10)" = "MINGW32_NT" ]; then
    systeminfo
elif [ "$(expr substr $(uname -s) 1 5)" = "Linux" ]; then
    cat /proc/cpuinfo | grep "model name" | head -n 1
    cat /proc/meminfo | grep "MemTotal:"
    cat /proc/version
else
    echo "Can't determine sysinfo - unknown operating system."
fi

