#/bin/bash
#Test using double precision floating point, libmath for the exponential, standanlone function for linear algebra.
#baseline test.
OUTFILE=test/LT_dp_lmexp_sta_uniform.csv
#make clean; make swkrls_c PRECISION=double EXP=orig C_FLAGS='-DMSE_TEST' #> /dev/null 2>&1
#./swkrls_c > $OUTFILE

echo "Training LAG test on a series of repeated uniform samples." > $OUTFILE
echo "REP, WL, LAG, MSE" >> $OUTFILE
for k in {1..20}
do
    octave --silent --eval "gen_uniform_series(3000, $k, 'mg30.h');"
    for j in {2..15}
    do
        echo -n "$k, $j, 0, " >> $OUTFILE
        make clean; make swkrls_c PRECISION=double EXP=orig C_FLAGS='-DMSE_TEST -DMSE_VAL_ONLY -DEMBED_LENGTH=5 -DWINDOW_LENGTH='$j #> /dev/null 2>&1
        ./swkrls_c >> $OUTFILE
        echo "" >> $OUTFILE
        for i in {1..20}
        do
            make clean; make swkrls_c PRECISION=double EXP=orig C_FLAGS='-DMSE_TEST -DMSE_VAL_ONLY -DEMBED_LENGTH=5 -DSTATE_LAG -DLAG='$i' -DWINDOW_LENGTH='$j #> /dev/null 2>&1
            echo -n "$k, $j, $i, " >> $OUTFILE
            ./swkrls_c >> $OUTFILE
            echo "" >> $OUTFILE
        done
    done
done
