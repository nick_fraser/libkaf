#!/bin/bash
echo "Test using single precision floating point, libmath for the exponential, atlas for linear algebra."
echo "WINDOW_LENGTH=15"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig LIN=atlas PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=15' > /dev/null 2>&1
./swkrls_c

echo "WINDOW_LENGTH=31"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig LIN=atlas PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=31' > /dev/null 2>&1
./swkrls_c

echo "WINDOW_LENGTH=63"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig LIN=atlas PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=63' > /dev/null 2>&1
./swkrls_c

echo "WINDOW_LENGTH=127"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig LIN=atlas PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=127' > /dev/null 2>&1
./swkrls_c

