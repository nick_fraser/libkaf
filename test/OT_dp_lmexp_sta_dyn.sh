#/bin/bash
#Test using double precision floating point, libmath for the exponential, standanlone function for linear algebra. Also uses dynamic memory. Need to test also with valgrind.
make clean; make swkrls_c PRECISION=double EXP=orig C_FLAGS='-DMSE_TEST -DDYN_MEM'
