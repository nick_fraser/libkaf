#/bin/bash
#Test using double precision floating point, aldkrls for prediction, libmath for the exponential function, standalone functions for linear algebra.
make clean; make swkrls_c PRECISION=double EXP=orig C_FLAGS='-DMSE_TEST -DALDKRLS_TRAIN'
