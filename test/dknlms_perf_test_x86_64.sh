#!/bin/bash
cat /proc/cpuinfo | grep "model name" | head -n 1
cat /proc/meminfo | grep "MemTotal:"
cat /proc/version

echo "Test KNLMS training using single precision floating point, libmath for the exponential, standanlone function for linear algebra."
echo "WINDOW_LENGTH=30"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=29 -DKNLMS_TRAIN' > /dev/null 2>&1
./swkrls_c

echo "WINDOW_LENGTH=46"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=45 -DKNLMS_TRAIN' > /dev/null 2>&1
./swkrls_c

echo "Test KNLMS training using single precision floating point, libmath for the exponential, atlas for linear algebra."
echo "WINDOW_LENGTH=30"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig LIN=atlas PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=29 -DKNLMS_TRAIN' > /dev/null 2>&1
./swkrls_c

echo "WINDOW_LENGTH=46"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig LIN=atlas PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=45 -DKNLMS_TRAIN' > /dev/null 2>&1
./swkrls_c

echo "Test KNLMS training using single precision floating point, libmath for the exponential, atlas with multithreading for linear algebra."
echo "WINDOW_LENGTH=30"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig LIN=ptatlas PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=29 -DKNLMS_TRAIN' > /dev/null 2>&1
./swkrls_c

echo "WINDOW_LENGTH=46"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig LIN=ptatlas PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=45 -DKNLMS_TRAIN' > /dev/null 2>&1
./swkrls_c

echo "Test KNLMS training using single precision floating point, libmath for the exponential, openblas for linear algebra."
echo "WINDOW_LENGTH=30"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig LIN=openblas PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=29 -DKNLMS_TRAIN' > /dev/null 2>&1
./swkrls_c

echo "WINDOW_LENGTH=46"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig LIN=openblas PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=45 -DKNLMS_TRAIN' > /dev/null 2>&1
./swkrls_c

