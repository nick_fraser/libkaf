#/bin/bash
#Test using single precision floating point, libmath for the exponential, atlas with multithreading for linear algebra.
make clean; make swkrls_c PRECISION=float EXP=orig LIN=ptatlas C_FLAGS='-DMSE_TEST'
