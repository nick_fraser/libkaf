#/bin/bash
#Test using single precision floating point, libmath for the exponential, atlas for linear algebra.
make clean; make swkrls_c PRECISION=float EXP=orig LIN=atlas C_FLAGS='-DMSE_TEST'
