#!/bin/bash
cat /proc/cpuinfo | grep "model name" | head -n 1
cat /proc/meminfo | grep "MemTotal:"
cat /proc/version

MAX_THREADS=$1

for lin in sta lapack atlas ptatlas openblas; do
    make clean > /dev/null 2>&1
    make param_search LIN=$lin PRECISION=float EXP=orig PERF_TEST=true TIMER=lp C_FLAGS="-DWINDOW_LENGTH=16 -DEMBED_LENGTH=8 -std=c99 -fopenmp" > /dev/null 2>&1
    echo "Test using LIN=$lin"
    for ((i = 1 ; i <= MAX_THREADS ; i++)); do
        export OMP_NUM_THREADS=$i
        echo -n "#num_threads=$OMP_NUM_THREADS: "
        ./param_search
    done
done

