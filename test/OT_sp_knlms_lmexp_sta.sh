#/bin/bash
#Test using single precision floating point, knlms for prediction, libmath for the exponential function, standalone functions for linear algebra.
make clean; make swkrls_c PRECISION=float EXP=orig C_FLAGS='-DMSE_TEST -DKNLMS_TRAIN'
