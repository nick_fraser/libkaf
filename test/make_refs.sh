#!/bin/bash

fail_count=0
test_count=0

for f in test/OT*.sh
do
    test_count=$((test_count + 1))
    ref_file=${f%.*}.ref
    echo -ne $test_count". Creating reference file $ref_file... "
    ret=$($f > /dev/null 2>&1)
    if [ ${?} -ne 0 ]; then
        echo "COMPILATION FAILED"
        fail_count=$((fail_count + 1))
    else
        ./swkrls_c > $ref_file
        echo "DONE."
    fi
done
echo
if [ $fail_count -eq 0 ]; then
    echo "-----------------"
    echo "All references created successfully."
else
    echo "-----------------"
    echo "Failed to create $fail_count references."
fi

