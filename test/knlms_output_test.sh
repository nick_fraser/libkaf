#!/bin/bash
echo "Test KNLMS training using single precision floating point, libmath for the exponential, standanlone function for linear algebra."
echo "WINDOW_LENGTH=15"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig C_FLAGS='-DOUTPUT_TEST -DWINDOW_LENGTH=15 -DKNLMS_TRAIN' > /dev/null 2>&1
./swkrls_c > output/OT_sp_knlms_mgts_wl15.csv

echo "WINDOW_LENGTH=31"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig C_FLAGS='-DOUTPUT_TEST -DWINDOW_LENGTH=31 -DKNLMS_TRAIN' > /dev/null 2>&1
./swkrls_c > output/OT_sp_knlms_mgts_wl31.csv

echo "WINDOW_LENGTH=63"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig C_FLAGS='-DOUTPUT_TEST -DWINDOW_LENGTH=63 -DKNLMS_TRAIN' > /dev/null 2>&1
./swkrls_c > output/OT_sp_knlms_mgts_wl63.csv

echo "WINDOW_LENGTH=127"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig C_FLAGS='-DOUTPUT_TEST -DWINDOW_LENGTH=127 -DKNLMS_TRAIN' > /dev/null 2>&1
./swkrls_c > output/OT_sp_knlms_mgts_wl127.csv

echo "Test KNLMS training using double precision floating point, libmath for the exponential, standanlone function for linear algebra."
echo "WINDOW_LENGTH=15"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=double EXP=orig C_FLAGS='-DOUTPUT_TEST -DWINDOW_LENGTH=15 -DKNLMS_TRAIN' > /dev/null 2>&1
./swkrls_c > output/OT_dp_knlms_mgts_wl15.csv

echo "WINDOW_LENGTH=31"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=double EXP=orig C_FLAGS='-DOUTPUT_TEST -DWINDOW_LENGTH=31 -DKNLMS_TRAIN' > /dev/null 2>&1
./swkrls_c > output/OT_dp_knlms_mgts_wl31.csv

echo "WINDOW_LENGTH=63"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=double EXP=orig C_FLAGS='-DOUTPUT_TEST -DWINDOW_LENGTH=63 -DKNLMS_TRAIN' > /dev/null 2>&1
./swkrls_c > output/OT_dp_knlms_mgts_wl63.csv

echo "WINDOW_LENGTH=127"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=double EXP=orig C_FLAGS='-DOUTPUT_TEST -DWINDOW_LENGTH=127 -DKNLMS_TRAIN' > /dev/null 2>&1
./swkrls_c > output/OT_dp_knlms_mgts_wl127.csv

