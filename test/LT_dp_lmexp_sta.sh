#/bin/bash
#Test using double precision floating point, libmath for the exponential, standanlone function for linear algebra.
#baseline test.
OUTFILE=test/LT_dp_lmexp_sta.csv
#make clean; make swkrls_c PRECISION=double EXP=orig C_FLAGS='-DMSE_TEST' #> /dev/null 2>&1
#./swkrls_c > $OUTFILE

echo "Training LAG test" > $OUTFILE
echo "WL, LAG, MSE" >> $OUTFILE
for j in 15 31 63 127
do
    echo -n "$j, 0, " >> $OUTFILE
    make clean; make swkrls_c PRECISION=double EXP=orig C_FLAGS='-DMSE_TEST -DMSE_VAL_ONLY -DWINDOW_LENGTH='$j #> /dev/null 2>&1
    ./swkrls_c >> $OUTFILE
    echo "" >> $OUTFILE
    for i in {1..20}
    do
        make clean; make swkrls_c PRECISION=double EXP=orig C_FLAGS='-DMSE_TEST -DMSE_VAL_ONLY -DSTATE_LAG -DLAG='$i' -DWINDOW_LENGTH='$j #> /dev/null 2>&1
        echo -n "$j, $i, " >> $OUTFILE
        ./swkrls_c >> $OUTFILE
        echo "" >> $OUTFILE
    done
done
