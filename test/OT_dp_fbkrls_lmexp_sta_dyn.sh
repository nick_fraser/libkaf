#/bin/bash
#Test using double precision floating point, fbkrls for prediction, libmath for the exponential function, standalone functions for linear algebra and dynamic memory for the state variable.
make clean; make swkrls_c PRECISION=double EXP=orig C_FLAGS='-DMSE_TEST -DFBKRLS_TRAIN -DDYN_MEM'
