#/bin/bash
#Test using single precision floating point, libmath for the exponential, openblas for linear algebra.
make clean; make swkrls_c PRECISION=float EXP=orig LIN=openblas C_FLAGS='-DMSE_TEST'
