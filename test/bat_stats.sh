#!/bin/bash
#Helper script to check the battery energy on multiple platforms.
if [ "$(uname)" = "Darwin" ]; then
    system_profiler SPPowerDataType
elif [ "$(expr substr $(uname -s) 1 10)" = "MINGW32_NT" ]; then
    echo "This doesn't work on Windows yet."
elif [ "$(expr substr $(uname -s) 1 5)" = "Linux" ]; then
    upower -i /org/freedesktop/UPower/devices/battery_BAT0
else
    echo "Can't check battery stats - unknown operating system."
fi

