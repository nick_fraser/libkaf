#/bin/bash
#Test using double precision floating point, libmath for the exponential, standalone function for linear algebra. Test lag=0. Uses sublinear training.
make clean; make swkrls_c PRECISION=double EXP=orig C_FLAGS='-DMSE_TEST -DSUBLINEAR_TRAINING -DPREDICTION_TRAINING_RATIO=2'
