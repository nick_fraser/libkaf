#!/bin/bash
#Makes skeleton test files for a new algorithm.
#example usage: test/test_skel.sh knlms fbkrls

for f in test/*$1*.sh
do
    t=${f/$1/$2}
    ttmp=$t.tmp
    O=${1^^}
    T=${2^^}
    cp $f $ttmp
    sed -e s/$O/$T/ -e s/$1/$2/ < $ttmp > $t
    chmod 755 $t
    rm $ttmp
done
