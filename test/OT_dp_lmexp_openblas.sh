#/bin/bash
#Test using double precision floating point, libmath for the exponential, openblas for linear algebra.
make clean; make swkrls_c PRECISION=double EXP=orig LIN=openblas C_FLAGS='-DMSE_TEST'
