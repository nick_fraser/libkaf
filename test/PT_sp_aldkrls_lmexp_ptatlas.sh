#!/bin/bash
echo "Test ALDKRLS training using single precision floating point, libmath for the exponential, atlas with multithreading for linear algebra."
echo "WINDOW_LENGTH=15"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig LIN=ptatlas PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=15 -DALDKRLS_TRAIN' > /dev/null 2>&1
./swkrls_c

echo "WINDOW_LENGTH=31"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig LIN=ptatlas PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=31 -DALDKRLS_TRAIN' > /dev/null 2>&1
./swkrls_c

echo "WINDOW_LENGTH=63"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig LIN=ptatlas PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=63 -DALDKRLS_TRAIN' > /dev/null 2>&1
./swkrls_c

echo "WINDOW_LENGTH=127"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig LIN=ptatlas PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=127 -DALDKRLS_TRAIN' > /dev/null 2>&1
./swkrls_c

