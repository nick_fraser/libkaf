#!/bin/bash
cat /proc/cpuinfo | grep "model name" | head -n 1
cat /proc/meminfo | grep "MemTotal:"
cat /proc/version

# Pynq Z1
echo "WINDOW_LENGTH=30"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=29 -DKNLMS_TRAIN' > /dev/null 2>&1
./swkrls_c

# Ultra96
echo "WINDOW_LENGTH=46"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=45 -DKNLMS_TRAIN' > /dev/null 2>&1
./swkrls_c

