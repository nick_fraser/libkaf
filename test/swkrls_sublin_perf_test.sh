#!/bin/bash
cat /proc/cpuinfo | grep "model name" | head -n 1
cat /proc/meminfo | grep "MemTotal:"
cat /proc/version

#Baseline tests.
echo "Test using single precision floating point, libmath for the exponential, standanlone function for linear algebra. Single step update/prediction."
echo "WINDOW_LENGTH=15"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=15' > /dev/null 2>&1
./swkrls_c

echo "WINDOW_LENGTH=31"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=31' > /dev/null 2>&1
./swkrls_c

echo "WINDOW_LENGTH=63"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=63' > /dev/null 2>&1
./swkrls_c

echo "Test using single precision floating point, libmath for the exponential, ATLAS for linear algebra. Single step update/prediction."
echo "WINDOW_LENGTH=15"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true LIN=atlas C_FLAGS='-DWINDOW_LENGTH=15' > /dev/null 2>&1
./swkrls_c

echo "WINDOW_LENGTH=31"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true LIN=atlas C_FLAGS='-DWINDOW_LENGTH=31' > /dev/null 2>&1
./swkrls_c

echo "WINDOW_LENGTH=63"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true LIN=atlas C_FLAGS='-DWINDOW_LENGTH=63' > /dev/null 2>&1
./swkrls_c

echo "Test using single precision floating point, libmath for the exponential, PTATLAS for linear algebra. Single step update/prediction."
echo "WINDOW_LENGTH=15"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true LIN=ptatlas C_FLAGS='-DWINDOW_LENGTH=15' > /dev/null 2>&1
./swkrls_c

echo "WINDOW_LENGTH=31"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true LIN=ptatlas C_FLAGS='-DWINDOW_LENGTH=31' > /dev/null 2>&1
./swkrls_c

echo "WINDOW_LENGTH=63"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true LIN=ptatlas C_FLAGS='-DWINDOW_LENGTH=63' > /dev/null 2>&1
./swkrls_c

#Sublinear tests.
SUBLIN=1000
echo "Test using single precision floating point, libmath for the exponential, standanlone function for linear algebra. Prediction/update ratio = $SUBLIN."
echo "WINDOW_LENGTH=15"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=15 -DSUBLINEAR_TRAINING -DPREDICTION_TRAINING_RATIO='$SUBLIN > /dev/null 2>&1
./swkrls_c

echo "WINDOW_LENGTH=31"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=31 -DSUBLINEAR_TRAINING -DPREDICTION_TRAINING_RATIO='$SUBLIN > /dev/null 2>&1
./swkrls_c

echo "WINDOW_LENGTH=63"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true C_FLAGS='-DWINDOW_LENGTH=63 -DSUBLINEAR_TRAINING -DPREDICTION_TRAINING_RATIO='$SUBLIN > /dev/null 2>&1
./swkrls_c

echo "Test using single precision floating point, libmath for the exponential, ATLAS for linear algebra."
echo "WINDOW_LENGTH=15"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true LIN=atlas C_FLAGS='-DWINDOW_LENGTH=15 -DSUBLINEAR_TRAINING -DPREDICTION_TRAINING_RATIO='$SUBLIN > /dev/null 2>&1
./swkrls_c

echo "WINDOW_LENGTH=31"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true LIN=atlas C_FLAGS='-DWINDOW_LENGTH=31 -DSUBLINEAR_TRAINING -DPREDICTION_TRAINING_RATIO='$SUBLIN > /dev/null 2>&1
./swkrls_c

echo "WINDOW_LENGTH=63"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true LIN=atlas C_FLAGS='-DWINDOW_LENGTH=63 -DSUBLINEAR_TRAINING -DPREDICTION_TRAINING_RATIO='$SUBLIN > /dev/null 2>&1
./swkrls_c

echo "Test using single precision floating point, libmath for the exponential, PTATLAS for linear algebra."
echo "WINDOW_LENGTH=15"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true LIN=ptatlas C_FLAGS='-DWINDOW_LENGTH=15 -DSUBLINEAR_TRAINING -DPREDICTION_TRAINING_RATIO='$SUBLIN > /dev/null 2>&1
./swkrls_c

echo "WINDOW_LENGTH=31"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true LIN=ptatlas C_FLAGS='-DWINDOW_LENGTH=31 -DSUBLINEAR_TRAINING -DPREDICTION_TRAINING_RATIO='$SUBLIN > /dev/null 2>&1
./swkrls_c

echo "WINDOW_LENGTH=63"
make clean > /dev/null 2>&1
make swkrls_c PRECISION=float EXP=orig PERF_TEST=true LIN=ptatlas C_FLAGS='-DWINDOW_LENGTH=63 -DSUBLINEAR_TRAINING -DPREDICTION_TRAINING_RATIO='$SUBLIN > /dev/null 2>&1
./swkrls_c

