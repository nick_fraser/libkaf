#!/bin/bash
TEST_SCRIPT=$1
ITER=$2
IDLE=$3
test/sys_info.sh
echo ""
echo "Battery Level before test"
test/bat_stats.sh
echo ""
if [ "$IDLE" = 1 ]; then
    echo "Measuring system idle power usage"
    date
    sleep 600 #Sleep for 10mins.
    date
    test/bat_stats.sh
else
    echo "Skipping idle power measurement."
fi
echo ""
echo "Running test using $TEST_SCRIPT. Iterations=$ITER"
date
echo ""
for i in `seq 1 $ITER`
do
    $TEST_SCRIPT
done
echo ""
date
test/bat_stats.sh

