#/bin/bash
#Test using double precision floating point, libmath for the exponential, standalone function for linear algebra. Test lag=5.
make clean; make swkrls_c PRECISION=double EXP=orig C_FLAGS='-DMSE_TEST -DSTATE_LAG -DLAG=5'
