/*							expf.c
 *
 *	Exponential function
 *
 *
 *
 * SYNOPSIS:
 *
 * float x, y, expf();
 *
 * y = expf( x );
 *
 *
 *
 * DESCRIPTION:
 *
 * Returns e (2.71828...) raised to the x power.
 *
 * Range reduction is accomplished by separating the argument
 * into an integer k and fraction f such that
 *
 *     x    k  f
 *    e  = 2  e.
 *
 * A polynomial is used to approximate exp(f)
 * in the basic range [-0.5, 0.5].
 *
 *
 * ACCURACY:
 *
 *                      Relative error:
 * arithmetic   domain     # trials      peak         rms
 *    IEEE      +- MAXLOG   100000      1.7e-7      2.8e-8
 *
 *
 * Error amplification in the exponential function can be
 * a serious matter.  The error propagation involves
 * exp( X(1+delta) ) = exp(X) ( 1 + X*delta + ... ),
 * which shows that a 1 lsb error in representing X produces
 * a relative error of X times 1 lsb in the function.
 * While the routine gives an accurate result for arguments
 * that are exactly represented by a double precision
 * computer number, the result contains amplified roundoff
 * error for large arguments not exactly represented.
 *
 *
 * ERROR MESSAGES:
 *
 *   message         condition      value returned
 * expf underflow    x < MINLOGF         0.0
 * expf overflow     x > MAXLOGF         MAXNUMF
 *
 */

/*
Cephes Math Library Release 2.2:  June, 1992
Copyright 1984, 1987, 1989 by Stephen L. Moshier
Direct inquiries to 30 Frost Street, Cambridge, MA 02140
*/

/* Single precision exponential function.
 * test interval: [-0.5, +0.5]
 * trials: 80000
 * peak relative error: 7.6e-8
 * rms relative error: 2.8e-8
 */

#include "expf.h"
#ifdef LUT_EXP
#include "exp_lut.h"
#endif

FUNCTION_PREFIX MATHTYPE_T nexpf(const MATHTYPE_T xx) {
MATHTYPE_T x, z, C1, C2;
int n;

x = xx;

//Remove error checking for now.
//if( x > MAXLOGF)
//	{
//	mtherr( "expf", OVERFLOW );
//	return( MAXNUMF );
//	}
//
//if( x < MINLOGF )
//	{
//	mtherr( "expf", UNDERFLOW );
//	return(0.0);
//	}

/* Express e**x = e**g 2**n
 *   = e**g e**( n loge(2) )
 *   = e**( g + n loge(2) )
 */
//z = floorf( LOG2EF * x + 0.5 ); /* floor() truncates toward -infinity. */
z = ( LOG2EF * x + 0.5 );
if(z < 0)
    z = (int)z-1;
else
    z = (int)z;

x -= z * _C1;
x -= z * _C2;
n = z;

z = x * x;
/* Theoretical peak relative error in [-0.5, +0.5] is 4.2e-9. */
z =
((((( 1.9875691500E-4  * x
   + 1.3981999507E-3) * x
   + 8.3334519073E-3) * x
   + 4.1665795894E-2) * x
   + 1.6666665459E-1) * x
   + 5.0000001201E-1) * z
   + x
   + 1.0;

/* multiply by power of 2 */
//x = ldexpf( z, n );

x = ldpow2( z, n);

return( x );
}

FUNCTION_PREFIX MATHTYPE_T ldpow2(const MATHTYPE_T xx, int n) {
    int i;
    MATHTYPE_T x,mul;
    x = 1.0;
    mul = 2.0;
    if(n == 0)
        return xx;
    else if(n < 0) {
        mul = 0.5;
        n = -n;
    }

    for(i = 0; i < n; ++i) {
        x = x*mul;
    }

    return xx*x;
}

#ifdef LUT_EXP
#ifdef LUT_LI /*LOOK UP TABLE WITH LINEAR INTERPOLATION*/
FUNCTION_PREFIX MATHTYPE_T exp_lut( const MATHTYPE_T xx ) {
    int i;
    MATHTYPE_T x,rem;
    x = (xx+EXP_LUT_B)*EXP_LUT_M;
    i = (int)x;
    rem = x - (MATHTYPE_T)i;
    if(i < 0)
        return exp_table[0];
    else if(i >= (EXP_LUT_N-1))
        return exp_table[EXP_LUT_N-1];
    x = (1.0-rem)*exp_table[i] + rem*exp_table[i+1];
    return x;
}

#else /*DIRECT LOOK UP TABLE - NO INTERPOLATION*/

FUNCTION_PREFIX MATHTYPE_T exp_lut( const MATHTYPE_T xx ) {
    int i;
    MATHTYPE_T x,rem;
    x = (xx+EXP_LUT_B)*EXP_LUT_M;
    i = (int)x;
    rem = x - (MATHTYPE_T)i;
    if(rem > 0.5) //Round to nearest entry.
        ++i;
    if(i < 0)
        return exp_table[0];
    else if(i >= EXP_LUT_N)
        return exp_table[EXP_LUT_N-1];
    return exp_table[i];
}

#endif /*LUT_LI*/
#endif /*LUT_EXP*/

