# LibKAF: A C Library For Kernel Adaptive Filters

This repository contains the start of a C library for implementing several kernel adapative filters.
Note, this is still incomplete and needs a significant cleanup/restructure.

## Installation

This repository requires installation of the following prerequesites:

1. [LAPACK](http://www.netlib.org/lapack/)
2. [OpenBlas](http://www.openblas.net/)
3. [ATLAS](http://math-atlas.sourceforge.net/)

## Measuring CPU Performance

If you're trying to reproduce the number from a publication currently under review,
please follow the instructions below:

1. Install the prerequesites
2. Clone the repository on your target device
3. Run `./test/dknlms_perf_test.sh`

## Acknowledgements

Several files in this repository are heavily based on, or taken directly from [KAFBOX](https://github.com/steven2358/kafbox).
