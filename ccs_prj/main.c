
#ifndef _TMS320_DSP

#include <stdlib.h>
#include <time.h>
#include "defines.h"
#include "swkrls.h"

#ifdef MSE_TEST
#include <stdio.h>
void print_MSE(MATHTYPE_T * x, MATHTYPE_T * y, int length);
#endif

//Pure C test for swkrls algorithm.
int main(int argc, char *argv[]) {
    MATHTYPE_T *y;
    int Ntrain,i;
    swkrls_ts_state_t state;
    struct timespec tic, toc;
    MATHTYPE_T total_time;
    #include "mg30.h"
    #ifdef PERF_TEST
    int Nloops,j;
    #endif

    #ifdef MCA_ON
    MCALIB_MODE=1;//MCALIB_IEEE  //Turn off monte carlo arithmetic for function setup.
    MCALIB_T = 53;     //Default value for double precision.
    #endif

    Ntrain = X_LENGTH; //Find the length of the series.

    y = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*X_LENGTH);

    //Initialise the swkrls state object.
    #ifdef DYN_MEM
    InitialiseState(&state, 20, 7);
    #else
    InitialiseState(&state);
    #endif

    #ifdef MCA_ON
        #ifdef _MP
        MCALIB_MODE=3;//MCALIB_MP
        #else
        MCALIB_MODE=2;//MCALIB_MCA
        #endif
    #endif

    #ifdef PERF_TEST
    Nloops = 1000;
    #endif

    clock_gettime(CLOCK_MONOTONIC, &tic);

    #ifdef PERF_TEST
    for(j = 0; j < Nloops; ++j) {
    #endif

    //Predict the time series using SW-KRLS with 1 step prediction.
    for(i = 0; i < Ntrain-1; ++i) {
        //Call the function to process the input.
        y[i+1] = swkrls_ts(x[i], &state);
    }

    #ifdef PERF_TEST
    } 
    #endif

    clock_gettime(CLOCK_MONOTONIC, &toc);

    #ifdef OUTPUT_TEST
    //Print output pairs.
    printf("x, \ty, \terr\n");
    for(i = 0; i < Ntrain-1; ++i) {
        printf("%7e, \t%7e, \t%7e\n", x[i], y[i], x[i]-y[i]);
    }
    #endif

    #ifdef MSE_TEST
    print_MSE(&x[X_LENGTH-50], &y[X_LENGTH-50], 50);
    #endif

    #ifdef PERF_TEST
    total_time = ((MATHTYPE_T)(toc.tv_nsec-tic.tv_nsec))/((MATHTYPE_T)1000000000) + (MATHTYPE_T)(toc.tv_sec-tic.tv_sec);
    printf("Elapsed time(s): %e. Time per prediction(s): %e.\n", total_time, total_time/((Ntrain-1)*Nloops));
    #endif

    #ifdef DYN_MEM
    FreeState(&state);
    #endif

    free(y);

    return 0;
}

#else /*_TMS320_DSP*/

//Normal includes.
#include "defines.h"
#include "swkrls.h"
#include "knlms.h"
#include "fbkrls.h"
#include "aldkrls.h"

//Extra includes for the UART interface and DSP hardware.
#include "hw_psc_C6748.h"
#include "soc_C6748.h"
#include "interrupt.h"
#include "lcdkC6748.h"
#include "hw_types.h"
#include "uart.h"
#include "psc.h"
#include "uartStdio.h"
#include "timer.h"

//Functions required for the UART setup.
static void ConfigureIntUART(void);
static void SetupInt(void);
static void UARTIsr(void);
void hw_init(void);
void print_str(const char * str, int length);
static void TimerSetUp64Bit(void);

#ifdef MSE_TEST
void print_MSE(MATHTYPE_T * x, MATHTYPE_T * y, int length);
#endif

//GLOBAL VARIABLES.
//char txArray[] = "SW-KRLS Timing Application\n";
//char str[50];

//Define for the timer.
#define TMR_PERIOD_LSB32               (0xFFFFFFFF)
#define TMR_PERIOD_MSB32               (0xFFFFFFFF)
#define INTERNAL_CLOCK_RATE 150.0 //Mhz

#define REGULARIZATION_VAL 1.0E-4
#define ALD_THRESH 1.0E-2
#define SIGMA 0.6
#define ETA 0.1
#define EPS 1.0E-4
#define MU0 0.9

//Main function for the DSP branch.
int main(void) {
    int Ntrain,i,j;
    #if defined (SUBLINEAR_TRAINING)
    _STAT swkrls_generic_state_t state;
    #elif defined (FBKRLS_TRAIN)
    _STAT fbkrls_state_t state;
    #elif defined (KNLMS_TRAIN)
    _STAT knlms_state_t state;
    #else /*SWKRLS_TRAIN*/
    _STAT swkrls_ts_state_t state;
    #endif
    unsigned int tic, toc, total_time;
	#include "mg30.h" //Define the x variable and X_LENGTH.
    _STAT MATHTYPE_T y[X_LENGTH];
    #if defined (FBKRLS_TRAIN)
    char * alg = "FBKRLS";
    #elif defined (KNLMS_TRAIN)
    char * alg = "KNLMS";
    #else /*SWKRLS_TRAIN*/
    char * alg = "SWKRLS";
    #endif
    #ifdef SINGLE_MATH
    char * precision = "float";
    #else /*defined (DOUBLE_MATH)*/
    char * precision = "double";
    #endif

    #ifdef MCA_ON
    MCALIB_MODE=1;//MCALIB_IEEE  //Turn off monte carlo arithmetic for function setup.
    MCALIB_T = 53;     //Default value for double precision.
    #endif

    hw_init(); //Setup the UART interrupts and any other hardware.

    TimerSetUp64Bit(); //Setup the 64bit timer.

    //print_str(txArray, sizeof(txArray));
    UARTprintf("%s (%s precision) Timing Application, window length = %d\n", alg, precision, WINDOW_LENGTH);

    Ntrain = X_LENGTH; //Find the length of the series.

    //Initialise the swkrls state object.
    #if defined (SUBLINEAR_TRAINING)
    InitialiseGenericState(&state, REGULARIZATION_VAL, SIGMA);
    #elif defined (FBKRLS_TRAIN)
    InitialiseFBKRLSState(&state, REGULARIZATION_VAL, SIGMA);
    #elif defined (KNLMS_TRAIN)
    InitialiseKNLMSState(&state, ETA, EPS, MU0, SIGMA);
    #else /*SWKRLS_TRAIN*/
    InitialiseState(&state, REGULARIZATION_VAL, SIGMA);
    #endif

    #ifdef MCA_ON
        #ifdef _MP
        MCALIB_MODE=3;//MCALIB_MP
        #else
        MCALIB_MODE=2;//MCALIB_MCA
        #endif
    #endif

    y[0] = 0;

    for(j = 0; j < 10; ++j) {
		//tic = clock(); //Doesn't work - try using a timer.
    	//toc = 0;
    	TimerCounterSet(SOC_TMR_2_REGS, TMR_TIMER12, 0);
    	TimerEnable(SOC_TMR_2_REGS, TMR_TIMER12, TMR_ENABLE_CONT);
    	tic = TimerCounterGet(SOC_TMR_2_REGS, TMR_TIMER12);

		//Predict the time series using SW-KRLS with 1 step prediction.
		for(i = 0; i < Ntrain-1; ++i) {
			//Call the function to process the input.
            #if defined (SUBLINEAR_TRAINING)
            y[i+1] = swkrls_generic_ts_wrap(x[i], &state);
            #elif defined (KNLMS_TRAIN)
            y[i+1] = knlms_ts_wrap(x[i], &state);
            #elif defined (FBKRLS_TRAIN)
            y[i+1] = fbkrls_ts_wrap(x[i], &state);
            #else
			y[i+1] = swkrls_ts(x[i], &state);
            #endif
		}

		//toc = clock(); //Doesn't work - try using a timer.
		toc = TimerCounterGet(SOC_TMR_2_REGS, TMR_TIMER12);
		TimerDisable(SOC_TMR_2_REGS, TMR_TIMER12);

		//Print output pairs.
		//printf("x, \ty, \terr\n");
		//for(i = 0; i < Ntrain-1; ++i) {
		//    printf("%7e, \t%7e, \t%7e\n", x[i], y[i], x[i]-y[i]);
		//}

		total_time = ((MATHTYPE_T)(toc-tic))/INTERNAL_CLOCK_RATE;
        #if defined (KNLMS_TRAIN)
		UARTprintf("Elapsed time(us): %d. Time per prediction(us): %d. Dictionary size: %d.\n", (int)total_time, (int)(total_time/(Ntrain-1)), state.dict_size);
        #else
		UARTprintf("Elapsed time(us): %d. Time per prediction(us): %d.\n", (int)total_time, (int)(total_time/(Ntrain-1)));
        #endif
    }

    while(1);

    return 0;
}

void print_str(const char * inp_str, int length) {
	int i,count;
	count = 0;
	for(i = 0; i < length; ++i) {
		if(inp_str[i] == '\0')
			break;
		else {
			UARTCharPut(SOC_UART_2_REGS, inp_str[count]);
			++count;
		}
	}
}

/*
** Setup the timer for 64 bit mode
*/
static void TimerSetUp64Bit(void)
{
    /* Configuration of Timer */
    TimerConfigure(SOC_TMR_2_REGS, TMR_CFG_64BIT_CLK_INT);

    /* Set the 64 bit timer period */
    TimerPeriodSet(SOC_TMR_2_REGS, TMR_TIMER12, TMR_PERIOD_LSB32);
    TimerPeriodSet(SOC_TMR_2_REGS, TMR_TIMER34, TMR_PERIOD_MSB32);
}

void hw_init(void)
{
    unsigned int intFlags = 0;
    unsigned int config = 0;

    /* Enabling the PSC for UART2.*/
    PSCModuleControl(SOC_PSC_1_REGS, HW_PSC_UART2, PSC_POWERDOMAIN_ALWAYS_ON,
		     PSC_MDCTL_NEXT_ENABLE);

    /* Setup PINMUX */
    UARTPinMuxSetup(2, FALSE);

    /* Enabling the transmitter and receiver*/
    UARTEnable(SOC_UART_2_REGS);

    /* 1 stopbit, 8-bit character, no parity */
    config = UART_WORDL_8BITS;

    /* Configuring the UART parameters*/
    UARTConfigSetExpClk(SOC_UART_2_REGS, SOC_UART_2_MODULE_FREQ,
                        BAUD_115200, config,
                        UART_OVER_SAMP_RATE_16);

    /* Enabling the FIFO and flushing the Tx and Rx FIFOs.*/
    UARTFIFOEnable(SOC_UART_2_REGS);

    /* Setting the UART Receiver Trigger Level*/
    UARTFIFOLevelSet(SOC_UART_2_REGS, UART_RX_TRIG_LEVEL_1);

#if 0
    /*
    ** Enable AINTC to handle interrupts. Also enable IRQ interrupt in ARM
    ** processor.
    */
    SetupInt();

    /* Configure AINTC to receive and handle UART interrupts. */
    ConfigureIntUART();

    /* Preparing the 'intFlags' variable to be passed as an argument.*/
    intFlags |= (UART_INT_LINE_STAT  |  \
                 UART_INT_TX_EMPTY |    \
                 UART_INT_RXDATA_CTI);

    /* Enable the Interrupts in UART.*/
    UARTIntEnable(SOC_UART_2_REGS, intFlags);
#endif
}

/*
** \brief   Interrupt Service Routine(ISR) to be executed on UART interrupts.
**          Depending on the source of interrupt, this
**          1> writes to the serial communication console, or
**          2> reads from the serial communication console, or
**          3> reads the byte in RBR if receiver line error has occured.
*/

#if 0
static void UARTIsr()
{
    static unsigned int length = sizeof(txArray);
    static unsigned int count = 0;
    unsigned char rxData = 0;
    unsigned int int_id = 0;

    /* This determines the cause of UART2 interrupt.*/
    int_id = UARTIntStatus(SOC_UART_2_REGS);

#ifdef _TMS320C6X
    // Clear UART2 system interrupt in DSPINTC
    IntEventClear(SYS_INT_UART2_INT);
#else
    /* Clears the system interupt status of UART2 in AINTC. */
    IntSystemStatusClear(SYS_INT_UARTINT2);
#endif

    /* Checked if the cause is transmitter empty condition.*/
    if(UART_INTID_TX_EMPTY == int_id)
    {
        if(0 < length)
        {
            /* Write a byte into the THR if THR is free. */
            UARTCharPutNonBlocking(SOC_UART_2_REGS, txArray[count]);
            length--;
            count++;
        }
        if(0 == length)
        {
            /* Disable the Transmitter interrupt in UART.*/
            UARTIntDisable(SOC_UART_2_REGS, UART_INT_TX_EMPTY);
        }
     }

    /* Check if the cause is receiver data condition.*/
    if(UART_INTID_RX_DATA == int_id)
    {
        rxData = UARTCharGetNonBlocking(SOC_UART_2_REGS);
        UARTCharPutNonBlocking(SOC_UART_2_REGS, rxData);
    }


    /* Check if the cause is receiver line error condition.*/
    if(UART_INTID_RX_LINE_STAT == int_id)
    {
        while(UARTRxErrorGet(SOC_UART_2_REGS))
        {
            /* Read a byte from the RBR if RBR has data.*/
            UARTCharGetNonBlocking(SOC_UART_2_REGS);
        }
    }

    return;
}

/*
** \brief   This function invokes necessary functions to configure the ARM
**          processor and ARM Interrupt Controller(AINTC) to receive and
**          handle interrupts.
*/


static void SetupInt(void)
{
#ifdef _TMS320C6X
	// Initialize the DSP INTC
	IntDSPINTCInit();

	// Enable DSP interrupts globally
	IntGlobalEnable();
#else
    /* Initialize the ARM Interrupt Controller(AINTC). */
    IntAINTCInit();

    /* Enable IRQ in CPSR.*/
    IntMasterIRQEnable();

    /* Enable the interrupts in GER of AINTC.*/
    IntGlobalEnable();

    /* Enable the interrupts in HIER of AINTC.*/
    IntIRQEnable();
#endif
}

/*
** \brief  This function confiugres the AINTC to receive UART interrupts.
*/
static void ConfigureIntUART(void)
{
#ifdef _TMS320C6X
	IntRegister(C674X_MASK_INT4, UARTIsr);
	IntEventMap(C674X_MASK_INT4, SYS_INT_UART2_INT);
	IntEnable(C674X_MASK_INT4);
#else
    /* Registers the UARTIsr in the Interrupt Vector Table of AINTC. */
    IntRegister(SYS_INT_UARTINT2, UARTIsr);

    /* Map the channel number 2 of AINTC to UART2 system interrupt. */
    IntChannelSet(SYS_INT_UARTINT2, 2);

    IntSystemEnable(SYS_INT_UARTINT2);
#endif
}

#endif

#endif

#ifdef MSE_TEST
//Calculate and print Mean Squared Error.
void print_MSE(MATHTYPE_T * x, MATHTYPE_T * y, int length) {
    MATHTYPE_T N,MSE,err;
    int i;
    N = 1.0/(MATHTYPE_T)length;
    MSE = 0.0;
    for(i = 0; i < length; ++i) {
        err = x[i] - y[i];
        MSE = MSE + (err*err);
    }
    MSE = N*MSE;
    printf("Mean Squared error: %e\n", MSE);
}
#endif

