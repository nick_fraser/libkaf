
#ifndef _KERNEL_H
#define _KERNEL_H

#include "defines.h"
#include "linear_math.h"
#ifndef ORIG_EXP
#include "expf.h"
#else /*ORIG_EXP*/
#if defined (SINGLE_MATH) && defined (_TMS320_DSP)
#include <mathf.h>
#else
#include <math.h>
#endif /*_TMS320_DSP*/
#endif /*ORIG_EXP*/

#ifdef DYN_MEM
#include <stdlib.h>
#endif

enum kernel_switch {KERNEL_UPDATE, KERNEL_PREDICT};

typedef enum kernel_switch kernel_switch_t;

// Gaussian kernel for SWKRLS function.
#ifdef DYN_MEM
FUNCTION_PREFIX void GaussianKernelTS(const MATHTYPE_T * const x, MATHTYPE_T * const k, const kernel_switch_t s, const int wl_1, const int el, const MATHTYPE_T sigma); //A time series version of the Guassian kernel.
#else /*STAT_MEM*/
DEVICE_PREFIX FUNCTION_PREFIX void GaussianKernelTS(const MATHTYPE_T * const x, MATHTYPE_T * const k, const kernel_switch_t s, const MATHTYPE_T sigma); //A time series version of the Guassian kernel.
#endif

//Generic Gaussian kernel.
#ifdef DYN_MEM
void GaussianKernel(const MATHTYPE_T * const x1, const MATHTYPE_T * const x2, MATHTYPE_T * const k, const kernel_switch_t s, const int wl_1, const int el, const MATHTYPE_T sigma); //A time series version of the Guassian kernel.
#else /*STAT_MEM*/
DEVICE_PREFIX void GaussianKernel(const MATHTYPE_T * const x1, const MATHTYPE_T * const x2, MATHTYPE_T * const k, const kernel_switch_t s, const MATHTYPE_T sigma); //A time series version of the Guassian kernel.
#endif

//Generic Gaussian kernel.
#ifdef DYN_MEM
void GaussianKernelVar(const MATHTYPE_T * const x1, const MATHTYPE_T * const x2, MATHTYPE_T * const k, const kernel_switch_t s, const int dict_size, const int wl_1, const int el, const MATHTYPE_T sigma); //A time series version of the Guassian kernel with a variable dictionary size.
#else /*STAT_MEM*/
DEVICE_PREFIX void GaussianKernelVar(const MATHTYPE_T * const x1, const MATHTYPE_T * const x2, MATHTYPE_T * const k, const kernel_switch_t s, const int dict_size, const MATHTYPE_T sigma); //A time series version of the Guassian kernel with a variable dictionary size.
#endif

#endif /*_KERNEL_H*/

