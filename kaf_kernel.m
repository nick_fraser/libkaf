function [K] = kaf_kernel(X1,X2,ktype,kpar)
% KAF_KERNEL calculates the kernel matrix between two data sets.
% KAF_KERNEL 计算两个数据集的核矩阵
% Input:	- X1, X2: data matrices in row format (data as rows)
%           - ktype: string representing kernel type
%           - kpar: vector containing the kernel parameters
% Input:	- X1, X2: 数据矩阵以行的形式 (数据作为行)
%           - ktype: 代表核类型的字串
%           - kpar: 包含核参数的向量
% Output:	- K: kernel matrix
%           - fl: flops used for calculation
% 输出:	- K: 核矩阵
%           - fl: 计算用到的浮点次数
% USAGE: K = kaf_kernel(X1,X2,ktype,kpar)
% 用法：K = kaf_kernel(X1,X2,ktype,kpar)

[N1,M] = size(X1); N2 = size(X2,1);
%训练 X1 21X7,X2 1X7,N1=21,M=7,N2=1
%预测 X1 100X7,X2 20X7,N1=100,M=7,N2=20
%初始化 X1 1X7,X2 1X7,N1=1,M=7,N2=1
switch ktype
    case 'gauss'	% 高斯核 Gaussian kernel
        sgm = kpar;	% 核宽度 kernel width
        
        norms1 = sum(X1.^2,2);      % 以矩阵X1.^2的每一行为对象，对一行内的数字求和,训练21x1,预测100x1
        
        %norms1new=zeros(N1,1);      %%%%%%%%%%%%%Vectorization start 
        %for n=1:N1
        %    norms1new(n)=dot(X1(n,:),X1(n,:));
        %end                         %%%%%%%%%%%%%Vectorization end 
        
        norms2 = sum(X2.^2,2);      % 以矩阵X2.^2的每一行为对象，对一行内的数字求和,训练1x1,预测 20x1
        
        %norms2new=zeros(N2,1);      %%%%%%%%%%%%%Vectorization start 
        %for n=1:N2
        %    norms2new(n)=dot(X2(n,:),X2(n,:));
        %end                         %%%%%%%%%%%%%Vectorization end 
        
        mat1 = repmat(norms1,1,N2); % 将norm1复制1XN2块，训练21x1,预测100x20
        mat2 = repmat(norms2',N1,1);% 将norm2复制N1X1块，训练21x1,预测100x20
        
        distmat = mat1 + mat2 - 2*X1*X2';	% 训练21x1,预测100x20,全距离矩阵 full distance matrix
       
        %dismatm=zeros(N1,N2);               %%%%%%%%%%%%%Vectorization start 
        %for n=1:N2
        %    for m=1:N1
        %        dismatm(m,n)=dot(X1(m,:),X2(n,:));
        %    end
        %end
        %
        %dismatnew=mat1+mat2-2*dismatm;      %%%%%%%%%%%%%Vectorization end 
        
        K = exp(-distmat/(2*sgm^2));        % 训练21x1,预测100x20
        if nargin>1
%            fl = kaf_flops('sum',N1*N2*M) + ...     % 所有X1(i,:)-X(2(j,:) all X1(i,:)-X(2(j,:)
%                kaf_flops('mult',N1*N2*M) + ...     % 平方每一个元素 square all elements
%                kaf_flops('sum',N1*N2*(M-1)) + ...	% 每次加上M-1元素 sum M-1 elements per entry
%                kaf_flops('mult',N1*N2) + ...       % 乘以1/(2*sgm^2）multiply by 1/(2*sgm^2)
%                kaf_flops('exp',N1*N2);
        end
        
    otherwise	% default case
        error ('unknown kernel type')
end


