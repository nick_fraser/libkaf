
#ifndef _ALDKRLS_H
#define _ALDKRLS_H

#include "defines.h"
#include "kernel.h"
#include "linear_math.h"

#ifdef DYN_MEM
struct aldkrls_state {
    MATHTYPE_T * x;
    MATHTYPE_T * alpha;
    MATHTYPE_T * K_inv;
    MATHTYPE_T * P;
    MATHTYPE_T thresh;
    MATHTYPE_T sigma;
    int dict_size;
    int wl;
    int el;
};
#else /*STAT_MEM*/
struct aldkrls_state {
    MATHTYPE_T x[EMBED_LENGTH*WINDOW_LENGTH];
    MATHTYPE_T alpha[WINDOW_LENGTH];
    MATHTYPE_T K_inv[WINDOW_LENGTH*WINDOW_LENGTH];
    MATHTYPE_T P[WINDOW_LENGTH*WINDOW_LENGTH];
    MATHTYPE_T thresh;
    MATHTYPE_T sigma;
    int dict_size;
};
#endif

typedef struct aldkrls_state aldkrls_state_t;

#ifdef DYN_MEM
void InitialiseALDKRLSState(aldkrls_state_t * state, int wl, int el, const MATHTYPE_T thresh, const MATHTYPE_T sigma); //Initialise the state object based on some parameters given in as function parameters.
void FreeALDKRLSState(aldkrls_state_t * state); //Free the members of the state variable.
#else /*STAT_MEM*/
void InitialiseALDKRLSState(aldkrls_state_t * state, const MATHTYPE_T thresh, const MATHTYPE_T sigma); //Initialise the state object based on some parameters given in aldkrls.h.
#endif
void aldkrls_train(const MATHTYPE_T * const x, const MATHTYPE_T y, aldkrls_state_t * const state); //Update the system state and calculate the next output in the time series.
MATHTYPE_T aldkrls_predict(const MATHTYPE_T * const x, const aldkrls_state_t * const state);
MATHTYPE_T aldkrls_ts_wrap(const MATHTYPE_T x, aldkrls_state_t * const state); //A wrapper for performing single step time series prediction.

#endif /*_ALDKRLS_H*/

