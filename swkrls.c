
#include "swkrls.h"

//All matrices are assumed to be in column major format!

#ifdef DYN_MEM
void InitialiseState(swkrls_ts_state_t * state, int wl, int el, const MATHTYPE_T c, const MATHTYPE_T sigma) {
    int i,j;
    //Placeholder for when there is a version with dynamic memory allocation.

    //Initialise length parameters.
    state->wl = wl;
    state->el = el;

    //Allocate memory for x,alpha,K_inv.
    state->x = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(wl+el));
    state->K_inv = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*((wl+1)*(wl+1)));
    state->alpha = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*wl);
    state->k = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*wl);
    state->c = c;
    state->sigma = sigma;

    //Initialise the internal variables to a 'good' default value.
    //Initialise x.
    for(i = 0; i < (el+wl); ++i) {
        state->x[i] = 0;
    }

    //Initialise alpha.
    for(i = 0; i < wl; ++i) {
        state->alpha[i] = 0;
    }

    //Initialise K_inv to a scaled indentity matrix.
    for(i = 0; i < wl; ++i) {
        for(j = 0; j < wl; ++j) {
            if(i == j)
                state->K_inv[j*(wl)+i] = 1.0/(1.0 + c);
            else
                state->K_inv[j*(wl)+i] = 0.0;
        }
    }

    //Initialise k.
    for(i = 0; i < wl; ++i) {
        state->k[i] = 1;
    }
}

void FreeState(swkrls_ts_state_t * state) {//Free the members of the state variable.
    free(state->K_inv);
    free(state->x);
    free(state->alpha);
    free(state->k);
}

#else /*STAT_MEM*/
void InitialiseState(swkrls_ts_state_t * state, const MATHTYPE_T c, const MATHTYPE_T sigma) {
    int i,j;
    //Placeholder for when there is a version with dynamic memory allocation.

    state->c = c;
    state->sigma = sigma;

    //Initialise the internal variables to a 'good' default value.
    //Initialise x.
    for(i = 0; i < (EMBED_LENGTH+WINDOW_LENGTH); ++i) {
        state->x[i] = 0;
    }

    //Initialise alpha.
    for(i = 0; i < WINDOW_LENGTH; ++i) {
        state->alpha[i] = 0;
    }

    //Initialise K_inv to a scaled indentity matrix.
    for(i = 0; i < WINDOW_LENGTH; ++i) {
        for(j = 0; j < WINDOW_LENGTH; ++j) {
            if(i == j)
                state->K_inv[j*(WINDOW_LENGTH)+i] = 1.0/(1.0 + c);
            else
                state->K_inv[j*(WINDOW_LENGTH)+i] = 0.0;
        }
    }

    //Initialise k.
    for(i = 0; i < WINDOW_LENGTH; ++i) {
        state->k[i] = 1;
    }
}
#endif

//Initialise and copy state. Doesn't work for DYN_MEM.
void copySwkrlsTsState(const swkrls_ts_state_t * const source, swkrls_ts_state_t * const dest) {
    int i,j;
    int wl,el;

    #ifdef DYN_MEM
    FreeState(dest);
    wl = source->wl;
    el = source->el;
    dest->wl = wl;
    dest->el = el;
    dest->x = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(wl+el));
    dest->K_inv = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*((wl+1)*(wl+1)));
    dest->alpha = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*wl);
    dest->c = source->c;
    dest->sigma = source->sigma;
    #else /*STAT_MEM*/
    wl = WINDOW_LENGTH;
    el = EMBED_LENGTH;
    #endif

    for(i = 0; i < (wl+el); ++i) {
        dest->x[i] = source->x[i];
    }

    for(i = 0; i < wl; ++i) {
        dest->alpha[i] = source->alpha[i];
    }

    for(i = 0; i < wl; ++i) {
        for(j = 0; j < wl; ++j) {
            dest->K_inv[j*(wl)+i] = source->K_inv[j*(wl)+i];
        }
    }
}

MATHTYPE_T swkrls_ts(const MATHTYPE_T x, swkrls_ts_state_t * const state) { //Update the system state and calculate the next output in the time series.
    MATHTYPE_T y;

    CalculateAlpha(x, state); //Update alpha based on the new input.

    y = PredictStep(state); //Predict the next output.

    return y;
}

//Run the training step of the sw-krls.
FUNCTION_PREFIX void CalculateAlpha(const MATHTYPE_T x, swkrls_ts_state_t * const state) { //Perform the update step of the swkrls algorithm.
    int i;
    int wl,el;

    #ifdef DYN_MEM
    wl = state->wl;
    el = state->el;
    #else /*STAT_MEM*/
    wl = WINDOW_LENGTH;
    el = EMBED_LENGTH;
    #endif

    //Add a row/col to the K_inv matrix.
    #ifdef DYN_MEM
    InverseAddRowCol(state->k, 1.0 + state->c, state->K_inv, wl);
    #else /*STAT_MEM*/
    InverseAddRowCol(state->k, 1.0 + state->c, state->K_inv);
    #endif

    //Remove a row/col from the K_inv matrix.
    #ifdef DYN_MEM
    InverseRemoveRowCol(state->K_inv, wl);
    #else /*STAT_MEM*/
    InverseRemoveRowCol(state->K_inv);
    #endif

    //Add input x to the state vector.
    for(i = 0; i < wl+el-1; ++i) {
        state->x[i] = state->x[i+1];
    }
    state->x[wl+el-1] = x;

    //Calculate alpha. alpha = K_inv*y[n].
    MatrixVectorMult(state->K_inv, &state->x[el], state->alpha, wl, wl, VEC_RIGHT);
}

//Predict the next output in the sequence.
FUNCTION_PREFIX MATHTYPE_T PredictStep(swkrls_ts_state_t * const state) { //Predict the next output.
    MATHTYPE_T y;
    int wl;

    #ifdef DYN_MEM
    wl = state->wl;
    #else /*STAT_MEM*/
    wl = WINDOW_LENGTH;
    #endif

    //Find the kernel matrix for the latest inputs.
    #ifdef DYN_MEM
    GaussianKernelTS(state->x, state->k, KERNEL_PREDICT, wl, state->el, state->sigma); //A time series version of the Guassian kernel.
    #else /*STAT_MEM*/
    GaussianKernelTS(state->x, state->k, KERNEL_PREDICT, state->sigma); //A time series version of the Guassian kernel.
    #endif

    //Predict the next value in the series.
    y = DotProductNoStride(state->alpha, state->k, wl);

    return y;
}

#ifdef DYN_MEM
FUNCTION_PREFIX void InverseAddRowCol(const MATHTYPE_T * const k, const MATHTYPE_T knn, MATHTYPE_T * const K_inv, const int wl) { //Add a row and a column to the K^-1 matrix.
#else /*STAT_MEM*/
FUNCTION_PREFIX void InverseAddRowCol(const MATHTYPE_T * const k, const MATHTYPE_T knn, MATHTYPE_T * const K_inv) { //Add a row and a column to the K^-1 matrix.
#endif
    MATHTYPE_T g_inv;
    MATHTYPE_T g;
    #ifdef DYN_MEM
    MATHTYPE_T * gm;
    MATHTYPE_T * f;
    MATHTYPE_T * Em_trans;
    MATHTYPE_T * E;
    #else /*STAT_MEM*/
    _STAT MATHTYPE_T gm[WINDOW_LENGTH];
    _STAT MATHTYPE_T f[WINDOW_LENGTH];
    _STAT MATHTYPE_T Em_trans[WINDOW_LENGTH*WINDOW_LENGTH];
    _STAT MATHTYPE_T E[WINDOW_LENGTH*WINDOW_LENGTH];
    int wl;
    #endif
    int i,j;

    #ifdef DYN_MEM
    gm = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*wl);
    f = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*wl);
    Em_trans = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*wl*wl);
    E = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*wl*wl);
    #else /*STAT_MEM*/
    wl = WINDOW_LENGTH;
    #endif

    //Calculate g_inv by first calculating the intermediate value gm. g_inv = d - k*(K_inv*k);
    MatrixVectorMult(K_inv, k, gm, wl, wl, VEC_RIGHT);
    
    //Calculate g_inv.
    g_inv = knn - DotProductNoStride(gm, k, wl);

    g = 1/g_inv;

    //Calculate f.
    ScaleVector(gm, 1, -g, wl, f);

    //Set Em to entries of gm scaled by f[i].
    for(i = 0; i < wl; ++i) {
        ScaleVector(gm, 1, f[i], wl, Em_trans + i*wl);
    }

    //Calculate E matrix. E = K_inv - Em_trans';
    for(i = 0; i < wl; ++i) {
        for(j = 0; j < wl; ++j) {
            E[i + j*wl] = K_inv[i + j*wl] - Em_trans[j + i*wl];
        }
    }

    //Calculate K_inv matrix.
    //K_inv = [E f; f' g];
    for(i = 0; i < wl+1; ++i) {
        for(j = 0; j < wl+1; ++j) {
            if((i == wl) && (j == wl))
                K_inv[i + j*(wl+1)] = g;
            else if(i == wl)
                K_inv[i + j*(wl+1)] = f[j];
            else if(j == wl)
                K_inv[i + j*(wl+1)] = f[i];
            else
                K_inv[i + j*(wl+1)] = E[i + j*wl];
        }
    }
    #ifdef DYN_MEM
    free(gm);
    free(f);
    free(Em_trans);
    free(E);
    #endif
}

#ifdef DYN_MEM
FUNCTION_PREFIX void InverseRemoveRowCol(MATHTYPE_T * const K_inv, const int wl) { //Remove a row and column from K^-1 matrix.
#else /*STAT_MEM*/
FUNCTION_PREFIX void InverseRemoveRowCol(MATHTYPE_T * const K_inv) { //Remove a row and column from K^-1 matrix.
#endif
    MATHTYPE_T e;
    int i,j;
    #ifdef DYN_MEM
    MATHTYPE_T * G;
    MATHTYPE_T * f;
    #else /*STAT_MEM*/
    _STAT MATHTYPE_T G[WINDOW_LENGTH*WINDOW_LENGTH];
    _STAT MATHTYPE_T f[WINDOW_LENGTH];
    int wl;
    #endif

    #ifdef DYN_MEM
    f = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*wl);
    G = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*wl*wl);
    #else /*STAT_MEM*/
    wl = WINDOW_LENGTH;
    #endif

    //Create the sub-matrix G.
    SubMatrix(K_inv, wl+1, wl+1, wl+2, G, wl, wl, wl);

    //Create sub-matrix f.
    SubMatrix(K_inv, wl+1, wl+1, 1, f, wl, 1, wl);

    e = K_inv[0];

    //Set K_inv to scaled values of f.
    for(i = 0; i < wl; ++i) {
        ScaleVector(f, 1, f[i]/e, wl, K_inv + i*wl);
    }

    //Calculate K_inv. K_inv = G - K_inv.
    for(i = 0; i < wl; ++i) {
        for(j = 0; j < wl; ++j) {
            K_inv[i + j*wl] = G[i + j*wl] - K_inv[i + j*wl];
        }
    }
    #ifdef DYN_MEM
    free(f);
    free(G);
    #endif
}

#ifdef DYN_MEM
void InitialiseGenericState(swkrls_generic_state_t * state, int wl, int el, const MATHTYPE_T c, const MATHTYPE_T sigma) { //Initialise the state object based on some parameters given in as function parameters.
    int i,j;
    //Placeholder for when there is a version with dynamic memory allocation.

    //Initialise length parameters.
    state->wl = wl;
    state->el = el;

    //Allocate memory for x,alpha,K_inv.
    state->x = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(wl*el));
    state->K_inv = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*((wl+1)*(wl+1)));
    state->alpha = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*wl);
    state->y = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*wl);
    state->c = c;
    state->sigma = sigma;

    //Initialise the internal variables to a 'good' default value.
    //Initialise x.
    for(i = 0; i < (el*wl); ++i) {
        state->x[i] = 0;
    }

    //Initialise alpha.
    for(i = 0; i < wl; ++i) {
        state->alpha[i] = 0;
    }

    //Initialise y.
    for(i = 0; i < wl; ++i) {
        state->y[i] = 0;
    }

    //Initialise K_inv to a scaled indentity matrix.
    for(i = 0; i < wl; ++i) {
        for(j = 0; j < wl; ++j) {
            if(i == j)
                state->K_inv[j*(wl)+i] = 1.0/(1.0 + c);
            else
                state->K_inv[j*(wl)+i] = 0.0;
        }
    }
}

//Assume all memory has been defined and set to zero.
void PartialInitialiseGenericState(swkrls_generic_state_t * state, int wl, int el, const MATHTYPE_T c, const MATHTYPE_T sigma) {
    int i,j;

    //Initialise length parameters.
    state->wl = wl;
    state->el = el;
    state->c = c;
    state->sigma = sigma;

    //Initialise K_inv to a scaled indentity matrix.
    for(i = 0; i < wl; ++i) {
        for(j = 0; j < wl; ++j) {
            if(i == j)
                state->K_inv[j*(wl)+i] = 1.0/(1.0 + c);
            else
                state->K_inv[j*(wl)+i] = 0.0;
        }
    }
}

void FreeGenericState(swkrls_generic_state_t * state) { //Free the members of the state variable.
    free(state->K_inv);
    free(state->x);
    free(state->y);
    free(state->alpha);
}

#else /*STAT_MEM*/
void InitialiseGenericState(swkrls_generic_state_t * state, const MATHTYPE_T c, const MATHTYPE_T sigma) { //Initialise the state object based on some parameters given in swkrls.h.
    int i,j;
    //Placeholder for when there is a version with dynamic memory allocation.

    state->c = c;
    state->sigma = sigma;

    //Initialise the internal variables to a 'good' default value.
    //Initialise x.
    for(i = 0; i < (EMBED_LENGTH*WINDOW_LENGTH); ++i) {
        state->x[i] = 0;
    }

    for(i = 0; i < WINDOW_LENGTH; ++i) {
        state->y[i] = 0;
    }

    //Initialise alpha.
    for(i = 0; i < WINDOW_LENGTH; ++i) {
        state->alpha[i] = 0;
    }

    //Initialise K_inv to a scaled indentity matrix.
    for(i = 0; i < WINDOW_LENGTH; ++i) {
        for(j = 0; j < WINDOW_LENGTH; ++j) {
            if(i == j)
                state->K_inv[j*(WINDOW_LENGTH)+i] = 1.0/(1.0 + state->c);
            else
                state->K_inv[j*(WINDOW_LENGTH)+i] = 0.0;
        }
    }
}

#endif

void copySwkrlsState(const swkrls_generic_state_t * const source, swkrls_generic_state_t * const dest) {
    int i,j;
    int wl,el;

    #ifdef DYN_MEM
    FreeGenericState(dest);
    wl = source->wl;
    el = source->el;
    dest->wl = wl;
    dest->el = el;
    dest->x = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(wl*el));
    dest->y = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*wl);
    dest->K_inv = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*((wl+1)*(wl+1)));
    dest->alpha = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*wl);
    dest->c = source->c;
    dest->sigma = source->sigma;
    #else /*STAT_MEM*/
    wl = WINDOW_LENGTH;
    el = EMBED_LENGTH;
    #endif

    for(i = 0; i < (wl*el); ++i) {
        dest->x[i] = source->x[i];
    }

    for(i = 0; i < wl; ++i) {
        dest->y[i] = source->y[i];
    }

    for(i = 0; i < wl; ++i) {
        dest->alpha[i] = source->alpha[i];
    }

    for(i = 0; i < wl; ++i) {
        for(j = 0; j < wl; ++j) {
            dest->K_inv[j*(wl)+i] = source->K_inv[j*(wl)+i];
        }
    }
}

void swkrls_train(const MATHTYPE_T * const x, const MATHTYPE_T y, swkrls_generic_state_t * const state) { //Update the system state and calculate the next output in the time series.
    #ifdef DYN_MEM
    MATHTYPE_T * k;    
    #else /*STAT_MEM*/
    MATHTYPE_T k[WINDOW_LENGTH+1];    
    #endif
    int i,j;
    int wl,el;

    #ifdef DYN_MEM
    wl = state->wl;
    el = state->el;
    #else /*STAT_MEM*/
    wl = WINDOW_LENGTH;
    el = EMBED_LENGTH;
    #endif

    #ifdef DYN_MEM
    k = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(wl+1));
    #endif

    //Calculate the kernel vector.
    #ifdef DYN_MEM
    GaussianKernel(state->x, x, k, KERNEL_UPDATE, wl, el, state->sigma); //A generic version of the Guassian kernel.
    #else /*STAT_MEM*/
    GaussianKernel(state->x, x, k, KERNEL_UPDATE, state->sigma); //A generic version of the Guassian kernel.
    #endif

    //Regularise the kernel vector.
    k[wl] = k[wl] + state->c;

    //Add a row/col to the K_inv matrix.
    #ifdef DYN_MEM
    InverseAddRowCol(k, k[wl], state->K_inv, wl);
    #else /*STAT_MEM*/
    InverseAddRowCol(k, k[wl], state->K_inv);
    #endif

    //Remove a row/col from the K_inv matrix.
    #ifdef DYN_MEM
    InverseRemoveRowCol(state->K_inv, wl);
    #else /*STAT_MEM*/
    InverseRemoveRowCol(state->K_inv);
    #endif

    //Add input x to the input of the state matrix. Matrix is in column major format. Remove the top row and add input to the last.
    for(i = 0; i < wl; ++i) {
        for(j = 0; j < el; ++j) {
            if(i != (wl-1))
                state->x[i+wl*j] = state->x[i+wl*j+1];
            else //(i == (wl-1))
                state->x[i+wl*j] = x[j];
        }
    }

    //Add y to the output of the state vector.
    for(i = 0; i < wl-1; ++i) {
        state->y[i] = state->y[i+1];
    }
    state->y[wl-1] = y;

    //Calculate alpha. alpha = K_inv*y[n].
    MatrixVectorMult(state->K_inv, state->y, state->alpha, wl, wl, VEC_RIGHT);

    #ifdef DYN_MEM
    free(k);
    #endif
}

MATHTYPE_T swkrls_predict(const MATHTYPE_T * const x, const swkrls_generic_state_t * const state) {
    MATHTYPE_T y;
    int wl;
    #ifdef DYN_MEM
    MATHTYPE_T * k;
    #else /*STAT_MEM*/
    MATHTYPE_T k[WINDOW_LENGTH];    
    #endif

    #ifdef DYN_MEM
    wl = state->wl;
    k = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*wl);
    #else /*STAT_MEM*/
    wl = WINDOW_LENGTH;
    #endif

    //Find the kernel matrix for the latest inputs.
    #ifdef DYN_MEM
    GaussianKernel(state->x, x, k, KERNEL_PREDICT, wl, state->el, state->sigma); //A time series version of the Guassian kernel.
    #else /*STAT_MEM*/
    GaussianKernel(state->x, x, k, KERNEL_PREDICT, state->sigma); //A time series version of the Guassian kernel.
    #endif

    //Predict the next value in the series.
    y = DotProductNoStride(state->alpha, k, wl);

    #ifdef DYN_MEM
    free(k);
    #endif

    return y;
}

MATHTYPE_T swkrls_generic_ts_wrap(const MATHTYPE_T x, swkrls_generic_state_t * const state) { //A wrapper for performing single step time series prediction.
    int i,el;
    #ifdef DYN_MEM
    #warning "swkrls_generic_ts_wrap function will cause a memory leak in dynamic mode."
    static MATHTYPE_T * x_train = NULL;
    el = state->el;
    if(x_train == NULL) {
        x_train = (MATHTYPE_T *)malloc(el*sizeof(MATHTYPE_T));
        for (i = 0; i < el; ++i)
            x_train[i] = 0.0;
    }
    #else /*STAT_MEM*/
    static MATHTYPE_T x_train[EMBED_LENGTH] = {0.0};
    el = EMBED_LENGTH;
    #endif

    #ifdef SUBLINEAR_TRAINING
    static int count = 0;
    #endif

    #ifdef RESET_TS_WRAP
    #ifdef SUBLINEAR_TRAINING
    count = 0;
    #endif
    #endif /*RESET_TS_WRAP*/

    #ifdef SUBLINEAR_TRAINING
    if(count == 0)
    #endif
    swkrls_train(x_train, x ,state);
    #ifdef SUBLINEAR_TRAINING
    ++count;
    count %= PREDICTION_TRAINING_RATIO;
    #endif

    for(i = 0; i < (el-1); ++i) {
        x_train[i] = x_train[i+1];
    }
    x_train[el-1] = x;

    return swkrls_predict(x_train, &state[0]);
}

