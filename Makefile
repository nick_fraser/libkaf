CC?=gcc
#CC=x86_64-w64-mingw32-gcc
MEX=mex
HEADER_FILES=$(wildcard *.h)
PRECISION?=double
C_FLAGS?=
C_OPT=
MEX_OPT=
C_LIBS=
OBJS_EXT=o
DEBUG?=false
PROFILE?=false
EXP?=poly
MCA?=false
MEX_EXT:=$(OBJS_EXT)
OUT_FLG=-o
PERF_TEST?=false
MATLAB_ROOT?=/opt/MATLAB/R2012a/
ML_INC=$(MATLAB_ROOT)/extern/include/
TIMER?=hp

#If the current OS is windows, the output extension is .exe and call mex.bat instead of mex.
ifeq ($(OS), Windows_NT)
MEX=mex.bat
MEX_EXT:=obj
OUT_FLG=-output
endif

#Override the HP timer if we're using MacOSX.
ifeq ($(shell uname -s), Darwin)
TIMER=lp
endif

#Enable debugging flags or add compiler optimization.
ifeq ($(DEBUG), true)
C_OPT:=-g
MEX_OPT:=-g
else
C_OPT:=-O3 -march=native -mtune=native -finline -funsafe-loop-optimizations -ffast-math
#C_OPT:=-O3 -march=native -falign-functions=32 -falign-loops=32 -fopt-info-vec -mtune=native -msse -msse2 -mssse3 -msse4.1 -msse4.2 -mmmx -mavx -funroll-loops -fomit-frame-pointer -finline -funsafe-loop-optimizations -ffast-math  #-mvzeroupper doesn't improve performance
MEX_OPT:=-O
endif

#Enable profiling. Doesn't seem to work since matlab does not support gprof.
ifeq ($(PROFILE), true)
override C_OPT:=-pg
endif

#Determine which precision to compile the mex file to.
ifeq ($(PRECISION), double)
override C_FLAGS:=$(C_FLAGS) -DDOUBLE_MATH
endif
ifeq ($(PRECISION), float)
override C_FLAGS:=$(C_FLAGS) -DSINGLE_MATH
endif

#Determine which exponential function to use.
ifeq ($(EXP), poly)
override C_FLAGS:=$(C_FLAGS) -DPOLY_EXP
endif
ifeq ($(EXP), lut)
override C_FLAGS:=$(C_FLAGS) -DLUT_EXP
endif
ifeq ($(EXP), lutli)
override C_FLAGS:=$(C_FLAGS) -DLUT_EXP -DLUT_LI
endif
ifeq ($(EXP), orig)
override C_FLAGS:=$(C_FLAGS) -DORIG_EXP
C_LIBS:=$(C_LIBS) -lm
endif

ifeq ($(LIN), atlas)
override C_FLAGS:=$(C_FLAGS) -DLAPACKE
#C_LIBS:=$(C_LIBS) -llapacke -llapack -lf77blas -latlas -lgfortran
C_LIBS:=$(C_LIBS) -lcblas -lf77blas -latlas -lgfortran
endif
ifeq ($(LIN), ptatlas)
override C_FLAGS:=$(C_FLAGS) -DLAPACKE
#C_LIBS:=$(C_LIBS) -llapacke -lptlapack -lptf77blas -latlas -lgfortran -lpthread
C_LIBS:=$(C_LIBS) -lptcblas -lptf77blas -latlas -lgfortran -lpthread
endif
ifeq ($(LIN), lapack)
override C_FLAGS:=$(C_FLAGS) -DLAPACKE
#C_LIBS:=$(C_LIBS) -llapacke -llapack -lblas -lgfortran
C_LIBS:=$(C_LIBS) -lblas -lgfortran
endif
ifeq ($(LIN), openblas)
override C_FLAGS:=$(C_FLAGS) -DLAPACKE
#C_LIBS:=$(C_LIBS) -llapacke -llapack -lblas -lgfortran
C_LIBS:=$(C_LIBS) -lopenblas -lgfortran -lpthread
endif
ifeq ($(LIN), mwlapack)
override C_FLAGS:=$(C_FLAGS) -DMWBLAS -I=$(ML_INC)
#override C_FLAGS:=$(C_FLAGS) -DMWBLAS
#override C_OPT:=-O3 -I $(ML_INC)
#override MEX_OPT:=-O -I=$(ML_INC)
C_LIBS:=$(C_LIBS) -largeArrayDims -lmwblas
endif

#Determine whether to compile with MCA or not.
ifeq ($(MCA), true) #If we're using MCA then we need to use the cilly source to source compiler and link extra libraries.
C_LIBS:=$(C_LIBS) -lmcalib -lgmp -lmpfr -lm
SSC=cilly
CILLY_FLAGS=--save-temps --dosimplify --dooneRet --dofpsimplify --dofphooks -fPIC
CILLY_EXT=cil.c
override C_FLAGS:=$(C_FLAGS) -DMCA_ON
#Determine whether to use mixed precision or MCA.
ifeq ($(MP), true)
override C_FLAGS:=$(C_FLAGS) -D_MP
endif
else #Otherwise, just use the regular compiler.
SSC:=$(CC)
CILLY_FLAGS:=
endif

ifeq ($(PERF_TEST), true)
ifeq ($(TIMER), hp)
C_LIBS:=$(C_LIBS) -lrt
else #lp
override C_FLAGS:=$(C_FLAGS) -DLP_TIMER
endif
override C_FLAGS:=$(C_FLAGS) -DPERF_TEST
endif

FPIC_SRC=swkrls.c knlms.c
FPIC_OBJS=$(FPIC_SRC:.c=.OBJS_EXT)

OBJECTS=$(wildcard *.$(OBJS_EXT)) $(wildcard *.$(MEX_EXT))

all: swkrls_c swkrls_c.mex kaf_c.mex

swkrls_c: $(filter-out mexMain.o ckaf.o param_search.o, $(addsuffix .$(OBJS_EXT), $(basename $(wildcard *.c))))
	$(CC) $^ -o swkrls_c $(C_LIBS) $(C_FLAGS) $(C_OPT)

ckaf: $(filter-out mexMain.o main.o param_search.o, $(addsuffix .$(OBJS_EXT), $(basename $(wildcard *.c))))
	$(CC) $^ -o ckaf $(C_LIBS) $(C_FLAGS) $(C_OPT)

param_search: $(filter-out mexMain.o main.o ckaf.o, $(addsuffix .$(OBJS_EXT), $(basename $(wildcard *.c))))
	$(CC) $^ -o param_search $(C_LIBS) $(C_FLAGS) $(C_OPT) -lm

paramSearchGpu: expf.c kernel.c knlms.c linear_math.c param_search.cu
	nvcc --x cu $^ -o $@ $(C_LIBS) $(C_FLAGS) $(C_OPT) -DCUDA_BUILD -rdc=true

kaf_c.mex: mexGen.$(MEX_EXT) $(filter-out main.o mexMain.o ckaf.o param_search.o, $(addsuffix .$(OBJS_EXT), $(basename $(wildcard *.c))))
	$(MEX) $^ $(OUT_FLG) kaf_c $(C_LIBS) $(C_FLAGS) $(MEX_OPT)

swkrls_c.mex: mexMain.$(MEX_EXT) $(filter-out main.o mexMain.o ckaf.o param_search.o, $(addsuffix .$(OBJS_EXT), $(basename $(wildcard *.c))))
	$(MEX) $^ $(OUT_FLG) swkrls_c $(C_LIBS) $(C_FLAGS) $(MEX_OPT)

mexMain.$(MEX_EXT): mexMain.c $(HEADER_FILES)
	$(MEX) -c $< $(C_FLAGS) $(MEX_OPT)

mexGen.$(MEX_EXT): mexMain.c $(HEADER_FILES)
	$(MEX) -c $< $(C_FLAGS) -DGENERIC_INTERFACE $(MEX_OPT)
	@mv mexMain.$(MEX_EXT) mexGen.$(MEX_EXT)

main.o: main.c $(HEADER_FILES)
	$(CC) -c $*.c $(C_FLAGS) $(C_OPT)

ckaf.o: ckaf.c $(HEADER_FILES)
	$(CC) -c $*.c $(C_FLAGS) $(C_OPT)

param_search.o: param_search.c $(HEADER_FILES)
	$(CC) -c $*.c $(C_FLAGS) $(C_OPT)

$(FPIC_OBJS): $(FPIC_SRC) $(HEADER_FILES)
	$(SSC) $(CILLY_FLAGS) -c $*.c $(C_FLAGS) -fPIC $(C_OPT)
	@rm -f $*.cil.i $*.i $*.cil.c

%.$(OBJS_EXT): %.c $(HEADER_FILES)
	$(SSC) $(CILLY_FLAGS) -c $*.c $(C_FLAGS) -fPIC $(C_OPT)
	@rm -f $*.cil.i $*.i $*.cil.c

.PHONY: test
test: test/run_tests.sh
	@test/run_tests.sh

.PHONY: mkrefs
mkrefs:
	@test/make_refs.sh

.PHONY: test_clean
test_clean:
	rm -f test/*.diff test/*.out

.PHONY: clean
clean:
	rm -f $(OBJECTS) *.exe swkrls_c ckaf swkrls_c.mexa64 swkrls_c.mexa32 swkrls_c.mexw64 swkrls_c.mexw32 mex.def kaf_c.mexa64 kaf_c.mexa32 kaf_c.mexw64 kaf_c.mexw32 param_search paramSearchGpu

.PHONY: cleanall
cleanall: clean test_clean
