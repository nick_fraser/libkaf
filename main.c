#include <stdlib.h>
#include <stdio.h>
#include "defines.h"
#include "swkrls.h"
#include "knlms.h"
#include "fbkrls.h"
#include "aldkrls.h"
#ifdef PERF_TEST
#ifdef LP_TIMER
#include <sys/time.h>
#else /*HP_TIMER*/
#include <time.h>
#endif /*HP_TIMER*/
#endif

#ifdef MSE_TEST
void print_MSE(MATHTYPE_T * x, MATHTYPE_T * y, int length);
#endif

#ifdef STATE_LAG
#ifndef LAG
#define LAG 20
#endif
void rotateState(swkrls_generic_state_t * state);
#endif

#ifndef NUM_LOOPS
#define NUM_LOOPS 1000
#endif

#define REGULARIZATION_VAL 1.0E-4
#define ALD_THRESH 1.0E-2
#define SIGMA 0.6
#define ETA 0.1
#define EPS 1.0E-4
#define MU0 0.9

//Pure C test for swkrls algorithm.
int main(int argc, char *argv[]) {
    MATHTYPE_T *y;
    int Ntrain,i;
    #ifdef STATE_LAG
    swkrls_generic_state_t * state;
    MATHTYPE_T *x_train;
    #elif defined (KNLMS_TRAIN)
    knlms_state_t state;
    #elif defined (FBKRLS_TRAIN)
    fbkrls_state_t state;
    #elif defined (ALDKRLS_TRAIN)
    aldkrls_state_t state;
    #elif defined (SUBLINEAR_TRAINING)
    swkrls_generic_state_t state;
    #else
    swkrls_ts_state_t state;
    #endif
    MATHTYPE_T total_time;
    #include "mg30.h"
    #ifdef PERF_TEST
    #ifdef LP_TIMER
    struct timeval tic, toc;
    #else /*HP_TIMER*/
    struct timespec tic, toc;
    #endif /*HP_TIMER*/
    int Nloops,j;
    #endif

    #ifdef MCA_ON
    MCALIB_MODE=1;//MCALIB_IEEE  //Turn off monte carlo arithmetic for function setup.
    MCALIB_T = 53;     //Default value for double precision.
    #endif

    Ntrain = X_LENGTH; //Find the length of the series.

    y = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*X_LENGTH);

    //Initialise the swkrls state object.
    #ifdef DYN_MEM
    #ifdef KNLMS_TRAIN
    InitialiseKNLMSState(&state, WINDOW_LENGTH, EMBED_LENGTH, ETA, EPS, MU0, SIGMA);
    #elif defined (FBKRLS_TRAIN)
    InitialiseFBKRLSState(&state, WINDOW_LENGTH, EMBED_LENGTH, REGULARIZATION_VAL, SIGMA);
    #elif defined (ALDKRLS_TRAIN)
    InitialiseALDKRLSState(&state, WINDOW_LENGTH, EMBED_LENGTH, ALD_THRESH, SIGMA);
    #elif defined (SUBLINEAR_TRAINING)
    InitialiseGenericState(&state, WINDOW_LENGTH, EMBED_LENGTH, REGULARIZATION_VAL, SIGMA);
    #else
    InitialiseState(&state, WINDOW_LENGTH, EMBED_LENGTH, REGULARIZATION_VAL, SIGMA);
    #endif
    #else
    #ifdef STATE_LAG
    //Define an array of states to be used as a FIFO.
    state = (swkrls_generic_state_t *)malloc(sizeof(swkrls_generic_state_t)*LAG);
    for(i = 0; i < LAG; ++i) {
        InitialiseGenericState(&state[i], REGULARIZATION_VAL, SIGMA);
    }

    //Set up in input vector to go with the output vector, x.
    x_train = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(X_LENGTH+EMBED_LENGTH));
    for(i = 0; i < (X_LENGTH+EMBED_LENGTH); ++i) {
        if(i < EMBED_LENGTH)
            x_train[i] = 0.0;
        else //(i >= EMBED_LENGTH)
            x_train[i] = x[i-EMBED_LENGTH];
    }
    #elif defined (KNLMS_TRAIN)
    InitialiseKNLMSState(&state, ETA, EPS, MU0, SIGMA);
    #elif defined (FBKRLS_TRAIN)
    InitialiseFBKRLSState(&state, REGULARIZATION_VAL, SIGMA);
    #elif defined (ALDKRLS_TRAIN)
    InitialiseALDKRLSState(&state, ALD_THRESH, SIGMA);
    #elif defined (SUBLINEAR_TRAINING)
    InitialiseGenericState(&state, REGULARIZATION_VAL, SIGMA);
    #else
    InitialiseState(&state, REGULARIZATION_VAL, SIGMA);
    #endif
    #endif

    #ifdef MCA_ON
        #ifdef _MP
        MCALIB_MODE=3;//MCALIB_MP
        #else
        MCALIB_MODE=2;//MCALIB_MCA
        #endif
    #endif

    #ifdef PERF_TEST
    Nloops = NUM_LOOPS;

    #ifdef LP_TIMER
    gettimeofday(&tic, NULL);
    #else /*HP_TIMER*/
    clock_gettime(CLOCK_MONOTONIC, &tic);
    #endif /*HP_TIMER*/

    for(j = 0; j < Nloops; ++j) {
    #endif

    //Predict the time series using SW-KRLS with 1 step prediction.
    for(i = 0; i < Ntrain-1; ++i) {
        #ifdef STATE_LAG
        swkrls_train(&x_train[i], x[i] ,&state[0]);
        y[i+1] = swkrls_predict(&x_train[i+1], &state[0]);
        rotateState(state);
        #elif defined (KNLMS_TRAIN)
        y[i+1] = knlms_ts_wrap(x[i], &state);
        #elif defined (FBKRLS_TRAIN)
        y[i+1] = fbkrls_ts_wrap(x[i], &state);
        #elif defined (ALDKRLS_TRAIN)
        y[i+1] = aldkrls_ts_wrap(x[i], &state);
        #elif defined (SUBLINEAR_TRAINING)
        y[i+1] = swkrls_generic_ts_wrap(x[i], &state);
        #else
        //Call the function to process the input.
        y[i+1] = swkrls_ts(x[i], &state);
        #endif
    }

    #ifdef PERF_TEST
    } 

    #ifdef LP_TIMER
    gettimeofday(&toc, NULL);
    #else /*HP_TIMER*/
    clock_gettime(CLOCK_MONOTONIC, &toc);
    #endif /*HP_TIMER*/
    #endif

    #ifdef OUTPUT_TEST
    //Print output pairs.
    printf("x, \ty, \terr\n");
    for(i = 0; i < Ntrain-1; ++i) {
        printf("%7e, \t%7e, \t%7e\n", x[i], y[i], x[i]-y[i]);
    }
    #endif

    #ifdef MSE_TEST
    print_MSE(&x[X_LENGTH-500], &y[X_LENGTH-500], 500);
    #endif

    #ifdef PERF_TEST
    #ifdef LP_TIMER
    total_time = ((MATHTYPE_T)(toc.tv_usec-tic.tv_usec))/((MATHTYPE_T)1000000) + (MATHTYPE_T)(toc.tv_sec-tic.tv_sec);
    #else /*HP_TIMER*/
    total_time = ((MATHTYPE_T)(toc.tv_nsec-tic.tv_nsec))/((MATHTYPE_T)1000000000) + (MATHTYPE_T)(toc.tv_sec-tic.tv_sec);
    #endif /*HP_TIMER*/
    printf("Elapsed time(s): %e. Time per prediction(s): %e.\n", total_time, total_time/((Ntrain-1)*Nloops));
    #endif

    #ifdef DYN_MEM
    #ifdef KNLMS_TRAIN
    FreeKNLMSState(&state);
    #elif defined (FBKRLS_TRAIN)
    FreeFBKRLSState(&state);
    #elif defined (ALDKRLS_TRAIN)
    FreeALDKRLSState(&state);
    #else
    FreeState(&state);
    #endif
    #endif

    #ifdef STATE_LAG
    free(state);
    #endif

    free(y);

    return 0;
}

#ifdef MSE_TEST
//Calculate and print Mean Squared Error.
void print_MSE(MATHTYPE_T * x, MATHTYPE_T * y, int length) {
    MATHTYPE_T N,MSE,err;
    int i;
    N = 1.0/(MATHTYPE_T)length;
    MSE = 0.0;
    for(i = 0; i < length; ++i) {
        err = x[i] - y[i];
        MSE = MSE + (err*err);
    }
    MSE = N*MSE;
    #ifdef MSE_VAL_ONLY
    printf("%.16e", MSE);
    #else
    printf("Mean Squared error: %e\n", MSE);
    #endif
}
#endif

#ifdef STATE_LAG
//Don't update K_inv until later.
void rotateState(swkrls_generic_state_t * state) {
    swkrls_generic_state_t tmp;
    int i,j;

    copySwkrlsState(&state[0], &tmp);

    for(i = 0; i < (LAG-1); ++i) {
        copySwkrlsState(&state[i+1], &state[i]);
    }

    copySwkrlsState(&tmp, &state[LAG-1]);
}
#endif

