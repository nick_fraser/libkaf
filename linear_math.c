
#include "linear_math.h"

DEVICE_PREFIX FUNCTION_PREFIX MATHTYPE_T DotProductNoStride(const MATHTYPE_T * const a, const MATHTYPE_T * const b, const int N) { //Dot product between two vectors.
    MATHTYPE_T res;
    #ifdef NAIVE_BLAS
    int i;

    res = 0.0;

    for(i = 0; i < N; ++i) {
        res = res + a[i]*b[i];
    }

    return res;
    #elif defined(LAPACKE)
    const int INCA = 1;
    const int INCB = 1;
    #ifdef SINGLE_MATH
    //res = sdot_(&N, a, &INCA, b, &INCB); //Doesn't work for some reason.
    res = cblas_sdot(N, a, INCA, b, INCB);
    #endif
    #ifdef DOUBLE_MATH
    //res = ddot_(&N, a, &INCA, b, &INCB); //Doesn't work for some reason.
    res = cblas_ddot(N, a, INCA, b, INCB);
    #endif
    return res;
    #else /*defined(MWBLAS)*/
    //Use the MATLAB datatypes.
    ptrdiff_t INCAA = INCA, INCBB = INCB, NN = N;
    //INCAA = INCA;
    //INCBB = INCB;
    //NN = N;
    #ifdef SINGLE_MATH
    res = sdot(&NN, a, &INCAA, b, &INCBB);
    #endif
    #ifdef DOUBLE_MATH
    res = ddot(&NN, a, &INCAA, b, &INCBB);
    #endif
    return res;
    #endif
}

DEVICE_PREFIX FUNCTION_PREFIX MATHTYPE_T DotProduct(const MATHTYPE_T * const a, const int INCA, const MATHTYPE_T * const b, const int INCB, const int N) { //Dot product between two vectors.
    MATHTYPE_T res;
    #ifdef NAIVE_BLAS
    int i;

    res = 0.0;

    for(i = 0; i < N; ++i) {
        res = res + a[i*INCA]*b[i*INCB];
    }
    
    return res;
    #elif defined(LAPACKE)
    #ifdef SINGLE_MATH
    //res = sdot_(&N, a, &INCA, b, &INCB); //Doesn't work for some reason.
    res = cblas_sdot(N, a, INCA, b, INCB);
    #endif
    #ifdef DOUBLE_MATH
    //res = ddot_(&N, a, &INCA, b, &INCB); //Doesn't work for some reason.
    res = cblas_ddot(N, a, INCA, b, INCB);
    #endif
    return res;
    #else /*defined(MWBLAS)*/
    //Use the MATLAB datatypes.
    ptrdiff_t INCAA = INCA, INCBB = INCB, NN = N;
    //INCAA = INCA;
    //INCBB = INCB;
    //NN = N;
    #ifdef SINGLE_MATH
    res = sdot(&NN, a, &INCAA, b, &INCBB);
    #endif
    #ifdef DOUBLE_MATH
    res = ddot(&NN, a, &INCAA, b, &INCBB);
    #endif
    return res;
    #endif
}

FUNCTION_PREFIX void ElementDivision(const MATHTYPE_T * const a, const int INCA, const MATHTYPE_T * const b, const int INCB, MATHTYPE_T * const c, const int INCC, const int N) { //Element-wise division of two vectors.
    int i;
    for(i = 0; i < N; ++i) {
        c[i*INCC] = a[i*INCA]/b[i*INCB];
    }
}

FUNCTION_PREFIX void VectorAdd(const MATHTYPE_T * const a, const int INCA, const MATHTYPE_T * const b, const int INCB, MATHTYPE_T * const c, const int INCC, const int N) { //Element-wise addition of two vectors.
    int i;
    for(i = 0; i < N; ++i) {
        c[i*INCC] = a[i*INCA]+b[i*INCB];
    }
}

FUNCTION_PREFIX void MatrixVectorMult(const MATHTYPE_T * const a, const MATHTYPE_T * const b, MATHTYPE_T * const c, const int M, const int N, const mult_side_t side) { //Matrix-Vector multiply. Result is stored in c.
    #ifdef NAIVE_BLAS
    int i,j;

    if(side == VEC_RIGHT) {
        //Perform matrix vector multiplication with the vector on the left side.
        for(i = 0; i < M; ++i) {
            c[i] = 0.0;
            for(j = 0; j < N; ++j) {
                c[i] = c[i] + a[i + M*j]*b[j];
            }
        }
    }
    else /*(side == VEC_LEFT)*/ {
        //Perform matrix vector multiplication with the vector on the right side.
        for(j = 0; j < N; ++j) {
            c[j] = 0.0;
            for(i = 0; i < M; ++i) {
                c[j] = c[j] + a[i + M*j]*b[i];
            }
        }
    }
    #elif defined(LAPACKE)
    //char trans;
    enum CBLAS_TRANSPOSE trans;
    int INCB,INCC,vN;
    MATHTYPE_T alpha,beta;
    INCB = 1;
    INCC = 1;
    alpha = 1.0;
    beta = 0.0;
    if(side == VEC_RIGHT) {
        vN = N;
        trans = CblasNoTrans;
    }
    else {/*(side == VEC_LEFT)*/
        vN = M;
        trans = CblasTrans;
    }
    #ifdef SINGLE_MATH
    //sgemv_(&trans, &M, &N, &alpha, a, &vN, b, &INCB, &beta, c, &INCC);
    cblas_sgemv(CblasColMajor, trans, M, N, alpha, a, vN, b, INCB, beta, c, INCC);
    #endif
    #ifdef DOUBLE_MATH
    //dgemv_(&trans, &M, &N, &alpha, a, &vN, b, &INCB, &beta, c, &INCC);
    cblas_dgemv(CblasColMajor, trans, M, N, alpha, a, vN, b, INCB, beta, c, INCC);
    #endif
    #else /*defined(MWBLAS)*/
    ptrdiff_t MM = M, NN = N, INCC = 1, INCB = 1, vN; //Use the MATLAB dimension data type.
    MATHTYPE_T alpha = 1.0, beta = 0.0;
    char trans;
    if(side == VEC_RIGHT) {
        vN = N;
        trans = 'N';
    }
    else {/*(side == VEC_LEFT)*/
        vN = M;
        trans = 'T';
    }
    #ifdef SINGLE_MATH
    sgemv(&trans, &MM, &NN, &alpha, a, &vN, b, &INCB, &beta, c, &INCC);
    #endif
    #ifdef DOUBLE_MATH
    dgemv(&trans, &MM, &NN, &alpha, a, &vN, b, &INCB, &beta, c, &INCC);
    #endif
    #endif /*LAPACKE*/
}

FUNCTION_PREFIX void MatrixMult(const MATHTYPE_T * const l, const MATHTYPE_T * const r, MATHTYPE_T * const c, const int M, const int N, const int K) { //Matrix multiply, results stored in c.
    int i,j,k;
    #ifdef NAIVE_BLAS
    for(i = 0; i < M; ++i) {
        for(j = 0; j < N; ++j) {
            c[i + j*M] = 0.0;
            for(k = 0; k < K; ++k) {
                c[i+j*M] += l[i+k*M]*r[k + j*K];
            }
        }
    }
    #elif defined(LAPACKE)
    //SUBROUTINE SGEMM ( TRANSA, TRANSB, M, N, K, ALPHA, A, LDA, B, LDB, BETA, C, LDC )
    #ifdef SINGLE_MATH
    //sgemm_(&ta, &tb, &M, &N, &K, &alpha, l, &M, r, &K, &beta, c, &M);
    cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, M, N, K, 1.0, l, M, r, K, 0.0, c, M);
    #endif
    #ifdef DOUBLE_MATH
    //dgemm_(&ta, &tb, &M, &N, &K, &alpha, l, &M, r, &K, &beta, c, &M);
    cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, M, N, K, 1.0, l, M, r, K, 0.0, c, M);
    #endif
    #else /*defined(MWBLAS)*/
    ptrdiff_t MM = M, NN = N, KK = K; //Use the MATLAB dimension data type.
    MATHTYPE_T alpha = 1.0, beta = 0.0;
    char transa = 'N', transb = 'N';
    #ifdef SINGLE_MATH
    sgemm(&transa, &transb, &MM, &NN, &KK, &alpha, l, &MM, r, &KK, &beta, c, &MM);
    #endif
    #ifdef DOUBLE_MATH
    dgemm(&transa, &transb, &MM, &NN, &KK, &alpha, l, &MM, r, &KK, &beta, c, &MM);
    #endif
    #endif
}

FUNCTION_PREFIX void MatrixSubtract(const MATHTYPE_T * const a, const MATHTYPE_T * const b, MATHTYPE_T * const c, const int M, const int N) { //Matrix subtraction, results stored in c.
    int i,j;
    for(i = 0; i < M; ++i) {
        for(j = 0; j < N; ++j) {
            c[i+j*M] = a[i+j*M] - b[i+j*M];
        }
    }
}

FUNCTION_PREFIX void MatrixAddition(const MATHTYPE_T * const a, const MATHTYPE_T * const b, MATHTYPE_T * const c, const int M, const int N) { //Matrix addition, results stored in c.
    int i,j;
    for(i = 0; i < M; ++i) {
        for(j = 0; j < N; ++j) {
            c[i+j*M] = a[i+j*M] + b[i+j*M];
        }
    }
}

//Scale the elements of vector 'a' and store in 'b'.
DEVICE_PREFIX FUNCTION_PREFIX void ScaleVector(const MATHTYPE_T * const a, const int INCA, const MATHTYPE_T alpha, const int N, MATHTYPE_T * const b) { //Scale a vector. Result is stored in b.
    int i;

    for(i = 0; i < N; ++i) {
        b[i] = alpha*a[i*INCA];
    }
}

FUNCTION_PREFIX void ScaleMatrix(const MATHTYPE_T * const a, const MATHTYPE_T alpha, const int M, const int N, MATHTYPE_T * const b) { //Scale a matrix. Result is stored in b.
    int i,j;
    for(i = 0; i < M; ++i) {
        for(j = 0; j < N; ++j) {
            b[i+j*M] = alpha*a[i+j*M];
        }
    }
}

FUNCTION_PREFIX void SubMatrix(const MATHTYPE_T * const A, const int M_a, const int N_a, const int offset, MATHTYPE_T * const B, const int M_b, const int N_b, const int M_bg) { //Create a sub matrix of A and store in B. offset is the pointer offset with which points to the first element of the sub-matrix.
    const MATHTYPE_T * A_sub;
    int i,j;

    //Find the first element of the sub matrix.
    A_sub = A + offset;

    //Copy the submatrix.
    for(i = 0; i < M_b; ++i) {
        for(j = 0; j < N_b; ++j) {
            B[i + j*M_bg] = A_sub[i + j*M_a];
        }
    }
}

FUNCTION_PREFIX void GrowMatrix(const MATHTYPE_T * const A, const MATHTYPE_T * const v, const MATHTYPE_T x, const int M, const int N, MATHTYPE_T * const B) { //Add a row/column to a matrix. Store result in B.
    int i,j;
    for(i = 0; i < M; i++) {
        for(j = 0; j < N; j++) {
            if(i == (M-1) && j == (N-1))
                B[i + j*M] = x;
            else if(i == (M-1))
                B[i + j*M] = v[j];
            else if(j == (N-1))
                B[i + j*M] = v[i];
            else
                B[i + j*M] = A[i + j*(M-1)];
        }
    }
}

DEVICE_PREFIX FUNCTION_PREFIX MATHTYPE_T MaxValue(const MATHTYPE_T * const a, const int INCA, const int N) { //Find the maximum in a vector.
    int i;
    MATHTYPE_T max;

    for(i = 0; i < N; ++i) {
        if(i == 0)
            max = a[0];
        else if(max < a[i*INCA])
            max = a[i*INCA];
    }
    return max;
}

FUNCTION_PREFIX int MinIndex(const MATHTYPE_T * const a, const int INCA, const int N) {
    int i;
    int index;
    MATHTYPE_T min;

    for(i = 0; i < N; ++i) {
        if(i == 0) {
            min = a[0];
            index = 0;
        }
        else if(min > a[i*INCA]) {
            min = a[i*INCA];
            index = i;
        }
    }
    return index;
}

FUNCTION_PREFIX void CopyVector(const MATHTYPE_T * const a, const int INCA, MATHTYPE_T * const b, const int INCB, const int N) {
    int i;

    for(i = 0; i < N; ++i) {
        b[i*INCB] = a[i*INCA];
    }
}

FUNCTION_PREFIX void CopyMatrix(const MATHTYPE_T * const A, const int Ma, MATHTYPE_T * const B, const int Mb, const int M, const int N) { //Copy from A to B.
    int i,j;
    for(i = 0; i < M; i++) {
        for(j = 0; j < N; j++) {
            B[i + j*Mb] = A[i + j*Ma];
        }
    }
}

FUNCTION_PREFIX void RemoveRow(MATHTYPE_T * const a, const int M, const int N, const int index) { //Remove a row from a vector.
    int j;
    for(j = 0; j < N; ++j) {
        CopyVector(&a[M*j + index + 1], 1, &a[M*j + index], 1, M-index-1);
    }
}

FUNCTION_PREFIX void VectorAbs(const MATHTYPE_T * const a, const int INCA, MATHTYPE_T * const b, const int INCB, const int N) { //Scale a vector. Result is stored in b.
    int i;

    for(i = 0; i < N; ++i) {
        if(a[i*INCA] > 0.0)
            b[i*INCB] = a[i*INCA];
        else
            b[i*INCB] = -a[i*INCA];
    }
}

FUNCTION_PREFIX MATHTYPE_T ShiftVec(MATHTYPE_T * const x, const int N, MATHTYPE_T y) { //Shift elements of x one place to the left. Insert y into the final place. return x[0].
    MATHTYPE_T ret;
    int i;
    ret = x[0];
    for(i = 0; i < (N-1); ++i)
        x[i] = x[i+1];
    x[N-1] = y;
    return ret;
}

void ShiftVecIn(MATHTYPE_T * const x, const int N, const MATHTYPE_T * const y, const int M) { //Shift elements of x M places to the left. Insert y into the final places.
    int i;
    for(i = 0; i < (N-M); ++i)
        x[i] = x[i+M];
    for(i = 0; i < M; ++i)
        x[N-M+i] = y[i];
}

