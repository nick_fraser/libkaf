function [output] = ckaf_wrap(algo, x, pars, varargin)

%enum kaf_mode {
%    PREDICT_OLD = 0,
%    TRAIN_OLD,
%    PREDICT_NEW,
%    TRAIN_NEW
%};
%
%enum kaf_alg {
%    SWKRLS = 0,
%    FBKRLS,
%    KNLMS,
%    ALDKRLS
%};

vars = [];
y = [];
version = 0; %0 = old version, 1 = new version.

switch(algo)
    case 'swkrls'
        alg = 0;
    case 'fbkrls'
        alg = 1;
    case 'knlms'
        alg = 2;
    case 'aldkrls'
        alg = 3;
    otherwise
        error('Unsupported algorithm type.');
end
alg = int32(alg);

%%%% Update defaults if specified by the user.
rp = 3;
if nargin > rp
    if rem(nargin-rp,2) ~= 0
        error('nargin needs to be in ''option'',value pairs. nargin = %d.', nargin);
    end
    for i = 1:2:(nargin-rp)
        switch(varargin{i})
            case 'vars'
                vars = varargin{i+1};
            case 'y'
                y = varargin{i+1};
            case 'version'
                version = varargin{i+1};
            otherwise
                fprintf('Unrecognised command line option, %s, continuing.\n', varargin{i});
        end
    end
end

%Hack so that the new version works.
if version == 1
    new_pars = pars;
    switch(algo)
        case 'swkrls'
            pars = [];
            pars = struct('c', new_pars.c, 'M', new_pars.M);
            pars.kernel.par =  new_pars.kernelpar;
        case 'fbkrls'
            pars = [];
            pars = struct('c', new_pars.lambda, 'M', new_pars.M);
            pars.kernel.par =  new_pars.kernelpar;
        case 'knlms'
            pars = [];
            pars = struct('mu0', new_pars.mu0, 'M', 1000, 'c', new_pars.eps, 'eta', new_pars.eta);
            pars.kernel.par =  new_pars.kernelpar;
        case 'aldkrls'
            pars = [];
            pars = struct('thresh', new_pars.nu, 'M', new_pars.M);
            pars.kernel.par =  new_pars.kernelpar;
        otherwise
            error('Unsupported algorithm type.');
    end
    if length(y) == 0
        vars = struct('mem', new_pars.dict, 'alpha', new_pars.alpha);
    end
end

if length(y) == 0
    mode = int32(0);
    output = kaf_c(mode, alg, x', int32(length(vars.alpha)), pars.kernel.par, vars.mem, vars.alpha);
else
    mode = int32(1);
    switch(alg)
    case {0,1}
        [output.mem, output.alpha, output.ymem, output.K_inv] = kaf_c(mode, alg, x', int32(pars.M), pars.kernel.par, pars.c, y);
    case 2
        [output.mem, output.alpha] = kaf_c(mode, alg, x', int32(pars.M), pars.kernel.par, pars.c, pars.eta, pars.mu0, y);
    case 3
        [output.mem, output.alpha, output.K_inv, output.P] = kaf_c(mode, alg, x', int32(pars.M), pars.kernel.par, pars.thresh, y);
    end
end

%Hack so that the new version works.
if (version == 1) && (length(y) ~= 0)
    %new_pars = pars;
    switch(algo)
        case 'swkrls'
            old_out = output;
            new_pars.dict = old_out.mem;
            new_pars.alpha = old_out.alpha;
            new_pars.dicty = old_out.ymem;
            new_pars.Kinv = old_out.K_inv;
            output = swkrls(new_pars);
        case 'fbkrls'
            old_out = output;
            new_pars.dict = old_out.mem;
            new_pars.alpha = old_out.alpha;
            new_pars.dicty = old_out.ymem;
            new_pars.Kinv = old_out.K_inv;
            output = fbkrls(new_pars);
        case 'knlms'
            old_out = output;
            new_pars.dict = old_out.mem;
            new_pars.alpha = old_out.alpha;
            output = knlms(new_pars);
        case 'aldkrls'
            old_out = output;
            new_pars.dict = old_out.mem;
            new_pars.alpha = old_out.alpha;
            new_pars.P = old_out.P;
            new_pars.Kinv = old_out.K_inv;
            output = aldkrls(new_pars);
        otherwise
            error('Unsupported algorithm type.');
    end
end

end

