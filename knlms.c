
#include "knlms.h"

#ifdef DYN_MEM
void InitialiseKNLMSState(knlms_state_t * state, int wl, int el, const MATHTYPE_T eta, const MATHTYPE_T eps, const MATHTYPE_T mu0, const MATHTYPE_T sigma) { //Initialise the state object based on some parameters given in as function parameters.
#else /*STAT_MEM*/
DEVICE_PREFIX void InitialiseKNLMSState(knlms_state_t * state, const MATHTYPE_T eta, const MATHTYPE_T eps, const MATHTYPE_T mu0, const MATHTYPE_T sigma) { //Initialise the state object based on some parameters given in defines.h.
#endif
    int i,j;

    #ifdef DYN_MEM
    state->wl = wl;
    state->el = el;
    #else /*STAT_MEM*/
    int wl,el;
    wl = WINDOW_LENGTH;
    el = EMBED_LENGTH;
    #endif
    state->dict_size = 0;
    state->step = eta;
    state->mu = mu0;
    state->eps = eps;
    state->sigma = sigma;

    //Allocate memory for matrices/vectors.
    #ifdef DYN_MEM
    state->x = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*wl*el);
    state->alpha = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*wl);
    #endif

    //x and y don't need to be initialised.

    for(i = 0; i < wl; ++i) {
        state->alpha[i] = 0.0;
    }
}

#ifdef DYN_MEM
void FreeKNLMSState(knlms_state_t * state) { //Free the members of the state variable.
    free(state->x);
    free(state->alpha);
}
#endif

void copyKNLMSState(const knlms_state_t * const source, knlms_state_t * const dest) {
    int i,j;
    int wl,el;

    #ifdef DYN_MEM
    FreeKNLMSState(dest);
    wl = source->wl;
    el = source->el;
    dest->wl = wl;
    dest->el = el;
    dest->x = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(wl*el));
    dest->alpha = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*wl);
    #else /*STAT_MEM*/
    wl = WINDOW_LENGTH;
    el = EMBED_LENGTH;
    #endif
    dest->step = source->step;
    dest->dict_size = source->dict_size;
    dest->mu = source->mu;
    dest->eps = source->eps;
    dest->sigma = source->sigma;

    for(i = 0; i < source->dict_size; ++i) {
        for(j = 0; j < el; ++i) {
            dest->x[i+wl*j] = source->x[i+wl*j];
        }
    }

    for(i = 0; i < wl; ++i) {
        dest->alpha[i] = source->alpha[i];
    }

}

DEVICE_PREFIX void knlms_train(const MATHTYPE_T * const x, const MATHTYPE_T y, knlms_state_t * const state) { //Update the system state and calculate the next output in the time series.
    MATHTYPE_T err;
    int wl,i;
    int grow;
    #ifdef DYN_MEM
    MATHTYPE_T * k;
    #else /*STAT_MEM*/
    MATHTYPE_T k[WINDOW_LENGTH+1];    
    #endif

    #ifdef DYN_MEM
    wl = state->wl;
    k = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(wl+1));
    #else /*STAT_MEM*/
    wl = WINDOW_LENGTH;
    #endif

    //Find the kernel matrix for the latest inputs.
    #ifdef DYN_MEM
    GaussianKernelVar(state->x, x, k, KERNEL_UPDATE, state->dict_size+1, wl, state->el, state->sigma); //A time series version of the Guassian kernel.
    #else /*STAT_MEM*/
    GaussianKernelVar(state->x, x, k, KERNEL_UPDATE, state->dict_size+1, state->sigma); //A time series version of the Guassian kernel.
    #endif

    grow = 0;
    if(state->dict_size == 0) {
        grow = 1;
    }
    else if((MaxValue(k, 1, state->dict_size) < state->mu) && (state->dict_size < wl)) {
        grow = 1;
    }

    if(grow) {
        #ifdef DYN_MEM
        for(i = 0; i < state->el; ++i) {
        #else /*STAT_MEM*/
        for(i = 0; i < EMBED_LENGTH; ++i) {
        #endif
            state->x[state->dict_size + i*wl] = x[i];
        }
        state->dict_size++;
    }

    //MATLAB: alpha = alpha + kaf.eta / (kaf.eps + k*k') * (y - k*kaf.alpha) * k';
    err = (state->step/(state->eps + DotProductNoStride(k, k, state->dict_size))) * (y - DotProductNoStride(k, state->alpha, state->dict_size));

    ScaleVector(k, 1, err, state->dict_size, k);

    for(i = 0; i < state->dict_size; ++i) {
        state->alpha[i] += k[i];
    }

    #ifdef DYN_MEM
    free(k);
    #endif
}

DEVICE_PREFIX MATHTYPE_T knlms_predict(const MATHTYPE_T * const x, const knlms_state_t * const state) {
    MATHTYPE_T y;
    int wl;
    #ifdef DYN_MEM
    MATHTYPE_T * k;
    #else /*STAT_MEM*/
    MATHTYPE_T k[WINDOW_LENGTH];    
    #endif

    #ifdef DYN_MEM
    wl = state->wl;
    k = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*wl);
    #else /*STAT_MEM*/
    wl = WINDOW_LENGTH;
    #endif

    //Find the kernel matrix for the latest inputs.
    #ifdef DYN_MEM
    GaussianKernelVar(state->x, x, k, KERNEL_PREDICT, state->dict_size, wl, state->el, state->sigma); //A time series version of the Guassian kernel.
    #else /*STAT_MEM*/
    GaussianKernelVar(state->x, x, k, KERNEL_PREDICT, state->dict_size, state->sigma); //A time series version of the Guassian kernel.
    #endif

    //Predict the next value in the series.
    y = DotProductNoStride(state->alpha, k, state->dict_size);

    #ifdef DYN_MEM
    free(k);
    #endif

    return y;
}

MATHTYPE_T knlms_ts_wrap(const MATHTYPE_T x, knlms_state_t * const state) { //A wrapper for performing single step time series prediction.
    int i,el;
    #ifdef DYN_MEM
    #warning "knlms_ts_wrap function will cause a memory leak in dynamic mode."
    static MATHTYPE_T * x_train = NULL;
    el = state->el;
    if(x_train == NULL)
        x_train = (MATHTYPE_T *)malloc(el*sizeof(MATHTYPE_T));
    #else /*STAT_MEM*/
    static MATHTYPE_T x_train[EMBED_LENGTH] = {0.0};
    el = EMBED_LENGTH;
    #endif

    #ifdef RESET_TS_WRAP
    if(state->dict_size == 0) {
        for(i = 0; i < el; ++i)
            x_train[i] = 0.0;
    }
    #endif

    knlms_train(x_train, x ,state);

    for(i = 0; i < (el-1); ++i) {
        x_train[i] = x_train[i+1];
    }
    x_train[el-1] = x;

    return knlms_predict(x_train, &state[0]);
}

