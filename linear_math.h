
#ifndef _LINEAR_MATH_H
#define _LINEAR_MATH_H

#include "defines.h"
#ifdef LAPACKE
//#include <lapacke.h>
#include <cblas.h>
#elif defined(MWBLAS)
#include <mex.h>
#include <blas.h>
#else
#define NAIVE_BLAS
#endif

enum mult_side {VEC_LEFT, VEC_RIGHT};
typedef enum mult_side mult_side_t;

DEVICE_PREFIX FUNCTION_PREFIX MATHTYPE_T DotProductNoStride(const MATHTYPE_T * const a, const MATHTYPE_T * const b, const int N); //Dot product between two vectors.
DEVICE_PREFIX FUNCTION_PREFIX MATHTYPE_T DotProduct(const MATHTYPE_T * const a, const int INCA, const MATHTYPE_T * const b, const int INCB, const int N); //Dot product between two vectors.
FUNCTION_PREFIX void ElementDivision(const MATHTYPE_T * const a, const int INCA, const MATHTYPE_T * const b, const int INCB, MATHTYPE_T * const c, const int INCC, const int N); //Element-wise division of two vectors.
FUNCTION_PREFIX void VectorAdd(const MATHTYPE_T * const a, const int INCA, const MATHTYPE_T * const b, const int INCB, MATHTYPE_T * const c, const int INCC, const int N); //Element-wise addition of two vectors.
FUNCTION_PREFIX void MatrixVectorMult(const MATHTYPE_T * const a, const MATHTYPE_T * const b, MATHTYPE_T * const c, const int M, const int N, const mult_side_t side); //Matrix-Vector multiply. Result is stored in c.
FUNCTION_PREFIX void MatrixMult(const MATHTYPE_T * const l, const MATHTYPE_T * const r, MATHTYPE_T * const c, const int M, const int N, const int K); //Matrix multiply, results stored in c.
FUNCTION_PREFIX void MatrixSubtract(const MATHTYPE_T * const a, const MATHTYPE_T * const b, MATHTYPE_T * const c, const int M, const int N); //Matrix subtraction, results stored in c.
FUNCTION_PREFIX void MatrixAddition(const MATHTYPE_T * const a, const MATHTYPE_T * const b, MATHTYPE_T * const c, const int M, const int N); //Matrix addition, results stored in c.
DEVICE_PREFIX FUNCTION_PREFIX void ScaleVector(const MATHTYPE_T * const a, const int INCA, const MATHTYPE_T alpha, const int N, MATHTYPE_T * const b); //Scale a vector. Result is stored in b.
FUNCTION_PREFIX void ScaleMatrix(const MATHTYPE_T * const a, const MATHTYPE_T alpha, const int M, const int N, MATHTYPE_T * const b); //Scale a matrix. Result is stored in b.
FUNCTION_PREFIX void SubMatrix(const MATHTYPE_T * const A, const int M_a, const int N_a, const int offset, MATHTYPE_T * const B, const int M_b, const int N_b, const int M_bg); //Create a sub matrix of A and store in B. offset is the pointer offset with which points to the first element of the sub-matrix.
FUNCTION_PREFIX void GrowMatrix(const MATHTYPE_T * const A, const MATHTYPE_T * const v, const MATHTYPE_T x, const int M, const int N, MATHTYPE_T * const B); //Add a row/column to a matrix. Store result in B.
DEVICE_PREFIX FUNCTION_PREFIX MATHTYPE_T MaxValue(const MATHTYPE_T * const a, const int INCA, const int N); //Find the maximum in a vector.
FUNCTION_PREFIX int MinIndex(const MATHTYPE_T * const a, const int INCA, const int N); //Return the index of the minimum entry in a vector.
FUNCTION_PREFIX void CopyVector(const MATHTYPE_T * const a, const int INCA, MATHTYPE_T * const b, const int INCB, const int N); //Copy from A to B.
FUNCTION_PREFIX void CopyMatrix(const MATHTYPE_T * const A, const int Ma, MATHTYPE_T * const B, const int Mb, const int M, const int N); //Copy from A to B.
FUNCTION_PREFIX void RemoveRow(MATHTYPE_T * const a, const int M, const int N, const int index); //Remove a row from a matrix.
FUNCTION_PREFIX void VectorAbs(const MATHTYPE_T * const a, const int INCA, MATHTYPE_T * const b, const int INCB, const int N); //Scale a vector. Result is stored in b.
FUNCTION_PREFIX MATHTYPE_T ShiftVec(MATHTYPE_T * const x, const int N, MATHTYPE_T y); //Shift elements of x one place to the left. Insert y into the final place. return x[0].
FUNCTION_PREFIX void ShiftVecIn(MATHTYPE_T * const x, const int N, const MATHTYPE_T * const y, const int M); //Shift elements of x M places to the left. Insert y into the final places.

#endif /* _LINEAR_MATH_H*/

