function [output] = gen_uniform_series(length,rep,filename);
%Generate random numbers of length 'length' with 'rep' numbers repeated and print in c format to file 'filename'.

N_vals = ceil(length/rep);
vals = 1 - 2*rand(N_vals, 1);
N = length;

output = repmat(vals, 1, rep);
output = reshape(output',N_vals*rep,1);
output = output(1:N,:);

if size(filename,2) ~= 0
    fid = fopen(filename,'w');

    fprintf(fid, '\n');
    fprintf(fid, '#ifndef _X_H\n');
    fprintf(fid, '#define _X_H\n');
    fprintf(fid, '\n');
    fprintf(fid, '#include "defines.h"\n');
    fprintf(fid, '\n');
    fprintf(fid, '#define X_LENGTH %d\n', N);
    fprintf(fid, '\n');
    fprintf(fid, 'MATHTYPE_T x[X_LENGTH] = {\n');
    fprintf(fid, '\t%.20e,\n', output(1:end-1));
    fprintf(fid, '\t%.20e\n', output(end));
    fprintf(fid, '};\n');
    fprintf(fid, '\n');
    fprintf(fid, '#endif /*_X_H*/\n');
    fprintf(fid, '\n'); %newline automatically appended?

    fclose(fid);
end

end

