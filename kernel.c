
#include "kernel.h"

//Gaussian kernel for updating k matrix when a new element in the series is discovered.
//x stored as x[0] is the oldest value stored in the window. x[WINDOW_LENGTH+EMBED_LENGTH] is the newest value.
#ifdef DYN_MEM
FUNCTION_PREFIX void GaussianKernelTS(const MATHTYPE_T * const x, MATHTYPE_T * const k, const kernel_switch_t s, const int wl_1, const int el, const MATHTYPE_T sigma) {
#else /*STAT_MEM*/
DEVICE_PREFIX FUNCTION_PREFIX void GaussianKernelTS(const MATHTYPE_T * const x, MATHTYPE_T * const k, const kernel_switch_t s, const MATHTYPE_T sigma) {
#endif
    int i_end;
    int i,j;
    int wl;
    MATHTYPE_T gamma;
    #ifdef DYN_MEM
    MATHTYPE_T * norms1;
    MATHTYPE_T * x_sqr;
    #else /*STAT_MEM*/
    int el;
    int wl_1;
    _STAT MATHTYPE_T norms1[WINDOW_LENGTH + 1];
    _STAT MATHTYPE_T x_sqr[WINDOW_LENGTH + EMBED_LENGTH + 1];
    #endif

    #ifdef DYN_MEM
    norms1 = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(wl_1+1));
    x_sqr = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(wl_1+el+1));
    wl = wl_1+1;
    #else /*STAT_MEM*/
    wl = WINDOW_LENGTH+1;
    wl_1 = WINDOW_LENGTH;
    el = EMBED_LENGTH;
    #endif

    if(s == KERNEL_UPDATE)
        i_end = wl;
    else/*(s == KERNEL_PREDICT)*/
        i_end = wl-1;

    //Calculate norms1 and norms2.
    //norms1 = sum(X1.^2, 2);
    //Since x is the last WINDOW_LENGTH + EMBED_LENGTH + 1 inputs we can make some shortcuts.

    //Square the elements of x and store in x_sqr.
    for(i = 0; i < wl_1+el; ++i) {
        x_sqr[i] = x[i]*x[i];
    }

    //Sum the appropriate elements and store in norms1.
    for(i = 0; i < wl; ++i) {
        norms1[i] = 0.0;
        for(j = 0; j < el; ++j) {
            norms1[i] = norms1[i] + x_sqr[i+j];
        }
    }

    //Calculate distance matrix. distmat = mat1 + mat2 - 2*X1*X2';
    for(i = 0; i < i_end; ++i) {
        k[i] = DotProductNoStride(&x[i], &x[wl_1], el);
    }

    //Calculate final value for the distance matrix.
    for(i = 0; i < i_end; ++i) {
        k[i] = 2*k[i] - norms1[i] - norms1[wl-1];
    }

    gamma = 1/(2*sigma*sigma);
    //Calculate exponential of each element of distmat.
    for(i = 0; i < i_end; ++i) {
        #ifdef ORIG_EXP
        #ifdef SINGLE_MATH
        k[i] = expf(k[i]*(gamma)); //Exp works for single or double.
        #else /*defined (DOUBLE_MATH)*/
        k[i] = exp(k[i]*(gamma)); //Exp works for single or double.
        #endif
        #elif defined POLY_EXP
        k[i] = nexpf(k[i]*(gamma)); //Exp works for single or double.
        #else /*LUT_EXP*/
        k[i] = exp_lut(k[i]*(gamma)); //Exp works for single or double.
        #endif
    }
    #ifdef DYN_MEM
    free(norms1);
    free(x_sqr);
    #endif
}

#ifdef DYN_MEM
void GaussianKernel(const MATHTYPE_T * const x1, const MATHTYPE_T * const x2, MATHTYPE_T * const k, const kernel_switch_t s, const int wl_1, const int el, const MATHTYPE_T sigma) {
#else /*STAT_MEM*/
DEVICE_PREFIX void GaussianKernel(const MATHTYPE_T * const x1, const MATHTYPE_T * const x2, MATHTYPE_T * const k, const kernel_switch_t s, const MATHTYPE_T sigma) {
#endif
    int i_end;
    int i,j;
    int wl;
    MATHTYPE_T gamma;
    #ifdef DYN_MEM
    MATHTYPE_T * x_sub;
    #else /*STAT_MEM*/
    int el;
    int wl_1;
    MATHTYPE_T x_sub[EMBED_LENGTH];
    #endif

    #ifdef DYN_MEM
    x_sub = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*el);
    wl = wl_1+1;
    #else /*STAT_MEM*/
    wl = WINDOW_LENGTH+1;
    wl_1 = WINDOW_LENGTH;
    el = EMBED_LENGTH;
    #endif

    //if(s == KERNEL_UPDATE)
    //    i_end = wl;
    //else/*(s == KERNEL_PREDICT)*/
    //    i_end = wl-1;
    if(s == KERNEL_UPDATE)
        k[wl-1] = 1;
    i_end = wl-1;

    //k[i] = Dot(x1[i] - x2, x1[i] - x2);
    for(i = 0; i < i_end; ++i) {
        for(j = 0; j < el; ++j) {
            x_sub[j] = x1[i+i_end*j] - x2[j];
        }
        k[i] = - DotProductNoStride(x_sub, x_sub, el);
    }

    gamma = 1/(2*sigma*sigma);
    //Calculate exponential of each element of distmat.
    for(i = 0; i < i_end; ++i) {
        #ifdef ORIG_EXP
        #ifdef SINGLE_MATH
        k[i] = expf(k[i]*(gamma)); //Exp works for single or double.
        #else /*defined (DOUBLE_MATH)*/
        k[i] = exp(k[i]*(gamma)); //Exp works for single or double.
        #endif
        #elif defined POLY_EXP
        k[i] = nexpf(k[i]*(gamma)); //Exp works for single or double.
        #else /*LUT_EXP*/
        k[i] = exp_lut(k[i]*(gamma)); //Exp works for single or double.
        #endif
    }
    #ifdef DYN_MEM
    free(x_sub);
    #endif
}

#ifdef DYN_MEM
void GaussianKernelVar(const MATHTYPE_T * const x1, const MATHTYPE_T * const x2, MATHTYPE_T * const k, const kernel_switch_t s, const int dict_size, const int wl_1, const int el, const MATHTYPE_T sigma) { //A time series version of the Guassian kernel with a variable dictionary size.
#else /*STAT_MEM*/
DEVICE_PREFIX void GaussianKernelVar(const MATHTYPE_T * const x1, const MATHTYPE_T * const x2, MATHTYPE_T * const k, const kernel_switch_t s, const int dict_size, const MATHTYPE_T sigma) { //A time series version of the Guassian kernel with a variable dictionary size.
#endif
    int i_end;
    int i,j;
    int wl;
    MATHTYPE_T gamma;
    #ifdef DYN_MEM
    MATHTYPE_T * x_sub;
    #else /*STAT_MEM*/
    int el;
    int wl_1;
    MATHTYPE_T x_sub[EMBED_LENGTH];
    #endif

    #ifdef DYN_MEM
    x_sub = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*el);
    wl = wl_1+1;
    #else /*STAT_MEM*/
    wl = WINDOW_LENGTH+1;
    wl_1 = WINDOW_LENGTH;
    el = EMBED_LENGTH;
    #endif

    if(s == KERNEL_UPDATE) {
        k[dict_size-1] = 1;
        i_end = dict_size-1;
    }
    else/*(s == KERNEL_PREDICT)*/
        i_end = dict_size;

    //k[i] = Dot(x1[i] - x2, x1[i] - x2);
    for(i = 0; i < i_end; ++i) {
        for(j = 0; j < el; ++j) {
            x_sub[j] = x1[i+wl_1*j] - x2[j]; //x1 is stored in column major format with wl_1 rows.
        }
        k[i] = - DotProductNoStride(x_sub, x_sub, el);
    }

    gamma = 1/(2*sigma*sigma);
    //Calculate exponential of each element of distmat.
    for(i = 0; i < i_end; ++i) {
        #ifdef ORIG_EXP
        #ifdef SINGLE_MATH
        k[i] = expf(k[i]*(gamma)); //Exp works for single or double.
        #else /*defined (DOUBLE_MATH)*/
        k[i] = exp(k[i]*(gamma)); //Exp works for single or double.
        #endif
        #elif defined POLY_EXP
        k[i] = nexpf(k[i]*(gamma)); //Exp works for single or double.
        #else /*LUT_EXP*/
        k[i] = exp_lut(k[i]*(gamma)); //Exp works for single or double.
        #endif
    }
    #ifdef DYN_MEM
    free(x_sub);
    #endif
}

