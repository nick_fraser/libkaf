
#include "aldkrls.h"
#include <stdio.h>

#ifdef DYN_MEM
void InitialiseALDKRLSState(aldkrls_state_t * state, int wl, int el, const MATHTYPE_T thresh, const MATHTYPE_T sigma) {
    state->x = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(wl*el));
    state->K_inv = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(wl*wl));
    state->P = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(wl*wl));
    state->alpha = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*wl);
    state->dict_size = 0;
    state->thresh = thresh;
    state->sigma = sigma;
    state->wl = wl;
    state->el = el;
}

void FreeALDKRLSState(aldkrls_state_t * state) {
    free(state->alpha);
    free(state->x);
    free(state->K_inv);
    free(state->P);
}

#else /*STAT_MEM*/
void InitialiseALDKRLSState(aldkrls_state_t * state, const MATHTYPE_T thresh, const MATHTYPE_T sigma) {
    state->dict_size = 0;
    state->thresh = thresh;
    state->sigma = sigma;
}
#endif

void aldkrls_train(const MATHTYPE_T * const x, const MATHTYPE_T y, aldkrls_state_t * const state) {
    MATHTYPE_T err;
    int wl,i,N;
    MATHTYPE_T delta;
    #ifdef DYN_MEM
    MATHTYPE_T * k;
    MATHTYPE_T * at;
    MATHTYPE_T * q;
    MATHTYPE_T * z;
    MATHTYPE_T * P1;
    #else /*STAT_MEM*/
    MATHTYPE_T k[WINDOW_LENGTH+1];
    MATHTYPE_T at[WINDOW_LENGTH];
    MATHTYPE_T q[WINDOW_LENGTH];
    MATHTYPE_T z[WINDOW_LENGTH];
    MATHTYPE_T P1[WINDOW_LENGTH*WINDOW_LENGTH];
    #endif
    N = state->dict_size;

    #ifdef DYN_MEM
    wl = state->wl;
    k = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(N+1));
    at = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(N+1));
    q = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(N+1));
    z = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(N+1));
    P1 = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*(N+1)*(N+1));
    #else /*STAT_MEM*/
    wl = WINDOW_LENGTH;
    #endif

    //Find the kernel matrix for the latest inputs.
    #ifdef DYN_MEM
    GaussianKernelVar(state->x, x, k, KERNEL_UPDATE, N+1, wl, state->el, state->sigma); //A time series version of the Guassian kernel.
    #else /*STAT_MEM*/
    GaussianKernelVar(state->x, x, k, KERNEL_UPDATE, N+1, state->sigma); //A time series version of the Guassian kernel.
    #endif

    if(N == 0) {
        //Add entry to the dictionary.
        #ifdef DYN_MEM
        for(i = 0; i < state->el; ++i) {
        #else /*STAT_MEM*/
        for(i = 0; i < EMBED_LENGTH; ++i) {
        #endif
            state->x[N + i*wl] = x[i];
        }
        state->dict_size++;
        N++;

        state->K_inv[0] = 1/k[0];
        state->P[0] = 1;
        state->alpha[0] = y/k[0];
    }
    else {
        MatrixVectorMult(state->K_inv, k, at, N, N, VEC_RIGHT);
        delta = k[N] - DotProductNoStride(at, k, N);
        if((delta > state->thresh) && (N < wl)) {
            //Add entry to the dictionary.
            #ifdef DYN_MEM
            for(i = 0; i < state->el; ++i) {
            #else /*STAT_MEM*/
            for(i = 0; i < EMBED_LENGTH; ++i) {
            #endif
                state->x[N + i*wl] = x[i];
            }
            state->dict_size++;
            N++;

            //Add entry to K_inv. Kinv = 1/delta*[delta*kaf.Kinv + at*at', -at; -at', 1];
            MatrixMult(at, at, P1, N-1, N-1, 1);
            ScaleMatrix(state->K_inv, delta, N-1, N-1, state->K_inv);
            MatrixAddition(state->K_inv, P1, state->K_inv, N-1, N-1);
            ScaleVector(at, 1, -1, N-1, q);
            GrowMatrix(state->K_inv, q, 1, N, N, P1);
            ScaleMatrix(P1, 1/delta, N, N, state->K_inv);

            //Calculate kaf.P = [kaf.P zeros(); zeros() 1];
            for(i = 0; i < (N-1); ++i) q[i] = 0.0;
            GrowMatrix(state->P, q, 1, N, N, P1);
            SubMatrix(P1, N, N, 0, state->P, N, N, N); //Copy result back into state->P;

            //Caclulate ode = 1/delta*(y-kt'*kaf.alpha);
            delta = (y - DotProductNoStride(state->alpha, k, N-1))/delta;

            //Calculate alpha = [kaf.alpha - at*ode; ode];
            ScaleVector(at, 1, delta, N-1, at);
            MatrixSubtract(state->alpha, at, state->alpha, N-1, 1);
            state->alpha[N-1] = delta;
        }
        else {
            //Calculate q = P*at/(1 + at'*P*at);
            MatrixVectorMult(state->P, at, q, N, N, VEC_RIGHT);
            delta = DotProductNoStride(q, at, N);
            ScaleVector(q, 1, 1/(1+delta), N, q);

            //Calculate P = P - q*(at'*P);
            MatrixVectorMult(state->P, at, z, N, N, VEC_LEFT);
            MatrixMult(q, z, P1, N, N, 1);
            MatrixSubtract(state->P, P1, state->P, N, N);

            //Calculate alpha = alpha + Kinv*q*(y-kt'*alpha);
            delta = y - DotProductNoStride(state->alpha, k, N);
            ScaleVector(q, 1, delta, N, q);
            MatrixVectorMult(state->K_inv, q, z, N, N, VEC_RIGHT);
            VectorAdd(state->alpha, 1, z, 1, state->alpha, 1, N);
        }
    }
    #ifdef DYN_MEM
    free(z);
    free(q);
    free(P1);
    free(at);
    free(k);
    #endif
}

MATHTYPE_T aldkrls_predict(const MATHTYPE_T * const x, const aldkrls_state_t * const state) {
    MATHTYPE_T y;
    int wl;
    #ifdef DYN_MEM
    MATHTYPE_T * k;
    #else /*STAT_MEM*/
    MATHTYPE_T k[WINDOW_LENGTH];    
    #endif

    #ifdef DYN_MEM
    wl = state->wl;
    k = (MATHTYPE_T *)malloc(sizeof(MATHTYPE_T)*wl);
    #else /*STAT_MEM*/
    wl = WINDOW_LENGTH;
    #endif

    //Find the kernel matrix for the latest inputs.
    #ifdef DYN_MEM
    GaussianKernelVar(state->x, x, k, KERNEL_PREDICT, state->dict_size, wl, state->el, state->sigma); //A time series version of the Guassian kernel.
    #else /*STAT_MEM*/
    GaussianKernelVar(state->x, x, k, KERNEL_PREDICT, state->dict_size, state->sigma); //A time series version of the Guassian kernel.
    #endif

    //Predict the next value in the series.
    y = DotProductNoStride(state->alpha, k, state->dict_size);

    #ifdef DYN_MEM
    free(k);
    #endif

    return y;
}

MATHTYPE_T aldkrls_ts_wrap(const MATHTYPE_T x, aldkrls_state_t * const state) {
    int i,el;
    #ifdef DYN_MEM
    #warning "aldkrls_ts_wrap function will cause a memory leak in dynamic mode."
    static MATHTYPE_T * x_train = NULL;
    el = state->el;
    if(x_train == NULL) {
        x_train = (MATHTYPE_T *)malloc(el*sizeof(MATHTYPE_T));
        for (i = 0; i < el; ++i)
            x_train[i] = 0.0;
    }
    #else /*STAT_MEM*/
    static MATHTYPE_T x_train[EMBED_LENGTH] = {0.0};
    el = EMBED_LENGTH;
    #endif

    #ifdef SUBLINEAR_TRAINING
    static int count = 0;
    #endif

    #ifdef RESET_TS_WRAP
    #ifdef SUBLINEAR_TRAINING
    count = 0;
    #endif
    #endif /*RESET_TS_WRAP*/

    #ifdef SUBLINEAR_TRAINING
    if(count == 0)
    #endif
    aldkrls_train(x_train, x ,state);
    #ifdef SUBLINEAR_TRAINING
    ++count;
    count %= PREDICTION_TRAINING_RATIO;
    #endif

    for(i = 0; i < (el-1); ++i) {
        x_train[i] = x_train[i+1];
    }
    x_train[el-1] = x;

    return aldkrls_predict(x_train, &state[0]);
}

