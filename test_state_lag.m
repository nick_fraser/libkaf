
%Test the 'state lag' for the swkrls algorithm.

Ntrain = 3000;
embed = 7;

fprintf('Generating Mackey-Glass time series...\n');
x = gen_mg_series(Ntrain + embed);
fprintf('Mackey Glass Generation END.\n');

y_est = zeros(Ntrain+1,1);         % Initialise estimation for y.

y_est = process_swkrls(x(1:Ntrain+1));

err = x(1:(Ntrain+1),1) - y_est;
mse = mean(err(end-500:end,1).^2);
fprintf('MATLAB version: Mean square error in the last 500 samples is %d.\n', mse);

