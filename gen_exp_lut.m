function [exp_lut,exp_lut_b,exp_lut_m,lut_x] = gen_exp_lut(range, N, filename)
%Generate a lookup table for the exponential function.
%inputs:
%   range: Range of values of which the N value LUT table is generated.
%   N: Number of values to place in the LUT. Should be an int.
%   filename: filename within which the LUT should be stored.

%Example usage:
%Generate EXP LUT
%gen_exp_lut([-5 0], 4096, 'exp_lut.h');

range_n = size(range,2);

if size(N,2) ~= 2
    error('Error: Need to provide two values for N.');
end

if range_n ~= 2
    error('Error: range parameter should have 2 values.');
end

Nlu = double(int32(N(1,1)));
Nli = double(int32(N(1,2)));
fid = fopen(filename,'w');

lut_x = (range(1,2) - range(1,1))*[0:1:(Nlu-1)]/(Nlu-1)';    %Generate the LUT.
lut_x = lut_x + range(1,1);
exp_lut_b = -range(1,1);                                 %Calculate values required for quick access to the LUT.
exp_lut_m = (Nlu-1)/(range(1,2) - range(1,1));
exp_lut = exp(lut_x);

lutli_x = (range(1,2) - range(1,1))*[0:1:(Nli-1)]/(Nli-1)';    %Generate the LUT.
lutli_x = lutli_x + range(1,1);
exp_lutli_b = -range(1,1);                                 %Calculate values required for quick access to the LUT.
exp_lutli_m = (Nli-1)/(range(1,2) - range(1,1));
exp_lutli = exp(lutli_x);

fprintf(fid, '\n');
fprintf(fid, '#ifndef EXP_LUT_H\n');
fprintf(fid, '#define EXP_LUT_H\n');
fprintf(fid, '\n');
fprintf(fid, '#include "defines.h"\n');
fprintf(fid, '\n');
fprintf(fid, '#ifndef LUT_LI\n');
fprintf(fid, '#define EXP_LUT_N %d\n', Nlu);
fprintf(fid, '#define EXP_LUT_B %.20e\n', exp_lut_b);
fprintf(fid, '#define EXP_LUT_M %.20e\n', exp_lut_m);
fprintf(fid, '\n');
fprintf(fid, 'MATHTYPE_T exp_table[EXP_LUT_N] = {\n');
fprintf(fid, '\t%.20e,\n', exp_lut(1:end-1));
fprintf(fid, '\t%.20e\n', exp_lut(end));
fprintf(fid, '};\n');
fprintf(fid, '\n');
fprintf(fid, '#else /*LUT_LI*/\n');
fprintf(fid, '#define EXP_LUT_N %d\n', Nli);
fprintf(fid, '#define EXP_LUT_B %.20e\n', exp_lutli_b);
fprintf(fid, '#define EXP_LUT_M %.20e\n', exp_lutli_m);
fprintf(fid, '\n');
fprintf(fid, 'MATHTYPE_T exp_table[EXP_LUT_N] = {\n');
fprintf(fid, '\t%.20e,\n', exp_lutli(1:end-1));
fprintf(fid, '\t%.20e\n', exp_lutli(end));
fprintf(fid, '};\n');
fprintf(fid, '\n');
fprintf(fid, '#endif /*LUT_LI*/\n');
fprintf(fid, '#endif /*EXP_LUT_H*/\n');
fprintf(fid, '\n'); %newline automatically appended?

fclose(fid);

end

