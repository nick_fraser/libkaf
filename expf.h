
#ifndef _EXPF_H
#define _EXPF_H

#include "defines.h"

#define LOG2EF 1.44269504088896341
//#define MAXLOGF 0.0
//#define MINLOGF 0.0
//#define MAXNUMF 0.0

#define _C1 0.693359375
#define _C2 -2.12194440e-4

FUNCTION_PREFIX MATHTYPE_T ldpow2( const MATHTYPE_T xx, int n);
FUNCTION_PREFIX MATHTYPE_T nexpf( const MATHTYPE_T xx );
FUNCTION_PREFIX MATHTYPE_T exp_lut( const MATHTYPE_T xx );

#endif /*_EXPF_H*/

