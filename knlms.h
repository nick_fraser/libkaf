
#ifndef _KNLMS_H
#define _KNLMS_H

#include "defines.h"
#include "kernel.h"
#include "linear_math.h"

#ifdef DYN_MEM
#include <stdlib.h>
#endif

/****** STRUCTURE DEFINITIONS ******/
#ifdef DYN_MEM
struct knlms_state {
    MATHTYPE_T * x;
    MATHTYPE_T * alpha;
    MATHTYPE_T step;
    MATHTYPE_T mu;
    MATHTYPE_T eps;
    MATHTYPE_T sigma;
    int dict_size;
    int wl;
    int el;
};
#else /*STAT_MEM*/
struct knlms_state {
    MATHTYPE_T x[EMBED_LENGTH*WINDOW_LENGTH];
    MATHTYPE_T alpha[WINDOW_LENGTH];
    MATHTYPE_T step;
    MATHTYPE_T mu;
    MATHTYPE_T eps;
    MATHTYPE_T sigma;
    int dict_size;
};
#endif

typedef struct knlms_state knlms_state_t;

#ifdef DYN_MEM
void InitialiseKNLMSState(knlms_state_t * state, int wl, int el, const MATHTYPE_T eta, const MATHTYPE_T eps, const MATHTYPE_T mu0, const MATHTYPE_T sigma); //Initialise the state object based on some parameters given in as function parameters.
void FreeKNLMSState(knlms_state_t * state); //Free the members of the state variable.
#else /*STAT_MEM*/
DEVICE_PREFIX void InitialiseKNLMSState(knlms_state_t * state, const MATHTYPE_T eta, const MATHTYPE_T eps, const MATHTYPE_T mu0, const MATHTYPE_T sigma); //Initialise the state object based on some parameters given in defines.h.
#endif
void copyKNLMSState(const knlms_state_t * const source, knlms_state_t * const dest);

DEVICE_PREFIX void knlms_train(const MATHTYPE_T * const x, const MATHTYPE_T y, knlms_state_t * const state); //Update the system state and calculate the next output in the time series.
DEVICE_PREFIX MATHTYPE_T knlms_predict(const MATHTYPE_T * const x, const knlms_state_t * const state);
MATHTYPE_T knlms_ts_wrap(const MATHTYPE_T x, knlms_state_t * const state); //A wrapper for performing single step time series prediction.

#endif /*_KNLMS_H*/

