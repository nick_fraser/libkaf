function [X, Y] = setup_data(x, Ex, varargin);
%Rearrange the data to frame it as a machine learning problem.
% inputs:
%       X - Input time series.
%       Ex - The number of features used to predict the next sample.

%Initialise some default values.
step = 1;
verbose = 0;
fid = 1;

%Check if parameters are overwriten here.
%%%% Update defaults if specified by the user.
required_params = 2;
if nargin > required_params
    if rem(nargin - required_params,2) ~= 0
        error('nargin needs to be in ''option'', value pairs. nargin = %d.', nargin);
    end
    for i = 1:2:(nargin-required_params)
        switch(varargin{i})
            case 'verbose'
                verbose = varargin{i+1};
            case 'step'
                step = 1;
            case 'fid'
                fid = varargin{i+1};
            otherwise
                fprintf(fid, 'Unrecognised command line option, %s, continuing.\n', varargin{i});
        end
    end
end

%Configure inputs.
Ntrain = length(x) - Ex;
X = zeros(Ntrain,Ex);
if verbose == 1, fprintf(fid, 'Aligning data for machine learning problem.. % 6.1f%%', 0); end

for i = 1:Ntrain
    if verbose == 1
        if ~mod(i, floor(Ntrain/1000)), fprintf(fid, '\b\b\b\b\b\b\b% 6.1f%%', 100*(i/Ntrain)); end
    end
    X(i,:) = x(i:i+Ex-1);
end
if verbose == 1, fprintf(fid, '\b\b\b\b\b\b\b% 6.1f%%', 100); end
%Configure outputs.
Y = x(Ex+1:Ex+Ntrain);
if verbose == 1, fprintf(fid, '\n'); end

