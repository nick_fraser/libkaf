
#ifndef _DEFINES_H
#define _DEFINES_H

#ifdef MCA_ON
//#include <mcalib.h>

extern int MCALIB_MODE;
extern int MCALIB_T;
#endif

#ifdef DOUBLE_MATH
#define MATHTYPE_T double
#endif /*DOUBLE_MATH*/

#ifdef SINGLE_MATH
#define MATHTYPE_T float
#endif /*SINGLE_MATH*/

#ifndef EMBED_LENGTH
#define EMBED_LENGTH 7
#endif

#ifndef WINDOW_LENGTH
#define WINDOW_LENGTH 20
#endif

#ifdef INLINE_ALL
#define FUNCTION_PREFIX inline
#else
#define FUNCTION_PREFIX
#endif

#ifdef CUDA_BUILD
#define DEVICE_PREFIX __device__ __host__
#else
#define DEVICE_PREFIX
#endif

#if defined (STATIC_OBJS) || defined (_TMS320_DSP)
#define _STAT static
#else
#define _STAT
#endif

#ifdef SUBLINEAR_TRAINING
#ifndef PREDICTION_TRAINING_RATIO
#define PREDICTION_TRAINING_RATIO 128
#endif
#endif

#endif /*_DEFINES_H*/

