function [y,state] = knlms_wrap(x, state)
% Wrapper function to the SW-KRLS so that it has an interface for a time series prediction.
% Inputs:
%       x: A single input for the nth sample.
%       state: Current state for the internal variables.
% Outputs: 
%       y: A single value for the current prediction.
%       state: Next state for the internal variable.

%       state is a struct containing the following:
%       .pars: A struct containing the function constants.
%       .vars: A struct containing the function variables.
%       .x a 'TAPSx1' vector.

%       pars contains:
%

% Train based on next value in sequence.
state.vars = kaf_knlms(state.vars, state.pars, state.x, x);

% Update inputs for next prediction.
state.x = [state.x(2:end) x];

% Predict next output.
y = kaf_knlms(state.vars, state.pars, state.x);

