
#ifndef _SWKRLS_H
#define _SWKRLS_H

#include "defines.h"
#include "kernel.h"
#include "linear_math.h"

#ifdef DYN_MEM
#include <stdlib.h>
#endif

/****** STRUCTURE DEFINITIONS ******/
#ifdef DYN_MEM
struct swkrls_ts_state {
    MATHTYPE_T * x;
    MATHTYPE_T * K_inv;
    MATHTYPE_T * alpha;
    MATHTYPE_T * k;
    MATHTYPE_T sigma;
    MATHTYPE_T c;
    int wl;
    int el;
};
#else /*STAT_MEM*/
struct swkrls_ts_state {
    MATHTYPE_T x[EMBED_LENGTH+WINDOW_LENGTH];
    MATHTYPE_T K_inv[(WINDOW_LENGTH+1)*(WINDOW_LENGTH+1)];
    MATHTYPE_T alpha[WINDOW_LENGTH];
    MATHTYPE_T k[WINDOW_LENGTH];
    MATHTYPE_T sigma;
    MATHTYPE_T c;
};
#endif

#ifdef DYN_MEM
struct swkrls_generic_state {
    MATHTYPE_T * x;
    MATHTYPE_T * y;
    MATHTYPE_T * K_inv;
    MATHTYPE_T * alpha;
    MATHTYPE_T sigma;
    MATHTYPE_T c;
    int wl;
    int el;
};
#else /*STAT_MEM*/
struct swkrls_generic_state {
    MATHTYPE_T x[EMBED_LENGTH*WINDOW_LENGTH];
    MATHTYPE_T y[WINDOW_LENGTH];
    MATHTYPE_T K_inv[(WINDOW_LENGTH+1)*(WINDOW_LENGTH+1)];
    MATHTYPE_T alpha[WINDOW_LENGTH];
    MATHTYPE_T sigma;
    MATHTYPE_T c;
};
#endif

typedef struct swkrls_ts_state swkrls_ts_state_t;
typedef struct swkrls_generic_state swkrls_generic_state_t;

/****** FUNCTION DEFINITIONS ******/
#ifdef DYN_MEM
void InitialiseState(swkrls_ts_state_t * state, int wl, int el, const MATHTYPE_T c, const MATHTYPE_T sigma); //Initialise the state object based on some parameters given in as function parameters.
void FreeState(swkrls_ts_state_t * state); //Free the members of the state variable.
FUNCTION_PREFIX void InverseAddRowCol(const MATHTYPE_T * const k, const MATHTYPE_T knn, MATHTYPE_T * const K_inv, const int wl); //Add a row and a column to the K^-1 matrix.
FUNCTION_PREFIX void InverseRemoveRowCol(MATHTYPE_T * const K_inv, const int wl); //Remove a row and column from K^-1 matrix.
#else /*STAT_MEM*/
void InitialiseState(swkrls_ts_state_t * state, const MATHTYPE_T c, const MATHTYPE_T sigma); //Initialise the state object based on some parameters given in swkrls.h.
FUNCTION_PREFIX void InverseAddRowCol(const MATHTYPE_T * const k, const MATHTYPE_T knn, MATHTYPE_T * const K_inv); //Add a row and a column to the K^-1 matrix.
FUNCTION_PREFIX void InverseRemoveRowCol(MATHTYPE_T * const K_inv); //Remove a row and column from K^-1 matrix.
#endif
void copySwkrlsTsState(const swkrls_ts_state_t * const source, swkrls_ts_state_t * const dest);
MATHTYPE_T swkrls_ts(const MATHTYPE_T x, swkrls_ts_state_t * const state); //Update the system state and calculate the next output in the time series.
FUNCTION_PREFIX void CalculateAlpha(const MATHTYPE_T x, swkrls_ts_state_t * const state); //Perform the update step of the swkrls algorithm.
FUNCTION_PREFIX MATHTYPE_T PredictStep(swkrls_ts_state_t * const state); //Predict the next output.

//Functions for generic swkrls algorithm.
#ifdef DYN_MEM
void InitialiseGenericState(swkrls_generic_state_t * state, int wl, int el, const MATHTYPE_T c, const MATHTYPE_T sigma); //Initialise the state object based on some parameters given in as function parameters.
void PartialInitialiseGenericState(swkrls_generic_state_t * state, int wl, int el, const MATHTYPE_T c, const MATHTYPE_T sigma); //Initialise the state object based on some parameters given in as function parameters.
void FreeGenericState(swkrls_generic_state_t * state); //Free the members of the state variable.
#else /*STAT_MEM*/
void InitialiseGenericState(swkrls_generic_state_t * state, const MATHTYPE_T c, const MATHTYPE_T sigma); //Initialise the state object based on some parameters given in swkrls.h.
#endif
void copySwkrlsState(const swkrls_generic_state_t * const source, swkrls_generic_state_t * const dest);
void swkrls_train(const MATHTYPE_T * const x, const MATHTYPE_T y, swkrls_generic_state_t * const state); //Update the system state and calculate the next output in the time series.
MATHTYPE_T swkrls_predict(const MATHTYPE_T * const x, const swkrls_generic_state_t * const state);
MATHTYPE_T swkrls_generic_ts_wrap(const MATHTYPE_T x, swkrls_generic_state_t * const state); //A wrapper for performing single step time series prediction.

#endif /*_SWKRLS_H*/

