function out = kaf_fbkrls(vars,pars,x,y)
% KAF_FBKRLS implements the basic iteration of the fixed-budget kernel 
% recursive least-squares (FB-KRLS) algorithm. This implementation includes
% a FLOPS and BYTES count for the training procedure.
%
% Input:	- vars: structure containing the used variables
%			- pars: structure containing kernel and algorithm parameters
%			- x: matrix containing input vectors as rows
%			- y: vector containing output values
% Output:	- out: contains the new vars if the algorithm is in learning 
%			mode (when x and y are provided) and the estimated output when 
%			the algorithm is being evaluated (when only x is given).
% Dependencies: kaf_kernel.
% USAGE: out = kaf_fbkrls(vars,pars,x,y)
%
% Author: Steven Van Vaerenbergh (steven *at* gtas.dicom.unican.es), 2010.
% Id: kaf_fbkrls.m v1.2 2012/10/03
% This file is part of the Kernel Adaptive Filtering Toolbox (KAFBOX) for
% MATLAB and OCTAVE. http://sourceforge.net/p/kafbox
%
% The algorithm in this file is based on the following publication:
% S. Van Vaerenbergh, I. Santamaria, W. Liu and J. C. Principe, "Fixed-
% Budget Kernel Recursive Least-Squares", 2010 IEEE International 
% Conference on Acoustics, Speech, and Signal Processing (ICASSP 2010), 
% Dallas, Texas, U.S.A., March 2010.
%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, version 3 (http://www.gnu.org/licenses).

% read parameters
ktype = pars.kernel.type;
kpar = pars.kernel.par;
M = pars.M;
c = pars.c;	% regularization constant

if nargin<4,
	mode = 'eval';
else
	if ~isfield(vars,'basis')
		mode = 'init';
	else
		mode = 'train';
	end
end

switch mode
	case 'init'		% initialize
        % initialize algorithm
		mem = zeros(M, length(x));
		basis = zeros(1,M);
		ymem = zeros(M,1);
        k = kaf_kernel(x,x,ktype,kpar);
        knn = k + c;
		K_inv = 1/knn*eye(M);
		alpha = zeros(M,1);
        
        % flops count
        %fl_reg = kaf_flops('sum',1);
        %fl_Kinv = kaf_flops('div',1);
        %fl_alpha = kaf_flops('mult',1);
        %fl = fl_k + fl_reg + fl_Kinv + fl_alpha;
		
        % write variables
		vars.mem = mem;
		vars.basis = basis;
		vars.ymem = y;
		vars.K_inv = K_inv;
		vars.alpha = alpha;
        %vars.flops = fl;
        
        % bytes stored in memory
        %bytes_mem = kaf_bytes('double',size(x,2)); % data is assumed to be double-precision
        %bytes_ymem = kaf_bytes('double',1);
        %bytes_alpha = kaf_bytes('double',1);
        %bytes_Kinv = kaf_bytes('double',1);
        %vars.bytes = bytes_mem + bytes_ymem + bytes_alpha + bytes_Kinv;
		
		out = kaf_fbkrls(vars, pars, x, y);
	case 'eval'		% evaluate
		mem = vars.mem;
		alpha = vars.alpha;
		
		K = kaf_kernel(x,mem,ktype,kpar);
		y = K*alpha;
		
		out = y;
	case 'train'		% train
        % read variables
		mem = vars.mem;
		ymem = vars.ymem;
		K_inv = vars.K_inv;
		basis = vars.basis;
		t = vars.t;
		
        % read parameters
		mu = pars.mu;	% label update learning rate

        % algorithm
		kt = kaf_kernel(mem,x,ktype,kpar);
		k = kaf_kernel(x,x,ktype,kpar);
        ktt = k + c;
        %fl_reg = kaf_flops('sum',1);
		
		% update all stored labels
		label_err = y - ymem;
		ymem = ymem + mu*kt.*label_err;
        if mu ~= 0
            %fl_labelerr = kaf_flops('sum',1);
            %fl_ymem = kaf_flops('sum',1) + kaf_flops('mult',2);
            fl_label = fl_labelerr + fl_ymem;
        else % no update if mu = 0
            fl_label = 0;
        end

		% expand dictionary
		mem = [mem; x];	% x should contain only one sample (column)
		ymem = [ymem; y];	% store subset labels
		basis = [basis t];
		
        m = size(mem,1);
		K_inv = inverse_addrowcol(kt,ktt,K_inv);	% extend kernel matrix
		alpha = K_inv*ymem;
        %fl_alpha = kaf_flops('mult',m,m,1);
		
		if (m>M)
			% prune dictionary
			err_ap = abs(alpha)./diag(K_inv);
			[mm,discard_ind] = min(err_ap); %#ok<ASGLU>

			mem(discard_ind,:) = [];
			ymem(discard_ind) = [];
			basis(discard_ind) = [];
            m = m-1;
			
			K_inv = prune_mat(K_inv,discard_ind);
		end
		alpha = K_inv*ymem;
        %fl_alpha = fl_alpha + kaf_flops('mult',m,m,1);
		
        % flops count
        %fl = fl_kt + fl_k + fl_reg + fl_label + fl_Kinv + fl_alpha;
        
        % write variables
		vars.K_inv = K_inv;
		vars.alpha = alpha;
		vars.mem = mem;
		vars.ymem = ymem;
		vars.basis = basis;
        %vars.flops = fl;
        
        % bytes stored in memory
        %bytes_K_inv = kaf_bytes('double',[m,m],'symm');
        %bytes_alpha = kaf_bytes('double',m);
        %bytes_mem = kaf_bytes('double',[size(x,2),m]); % data is assumed to be double-precision
        %bytes_ymem = kaf_bytes('double',m);
        %vars.bytes = bytes_K_inv + bytes_alpha + bytes_mem + bytes_ymem;
			
		out = vars;
end

function K_inv = inverse_addrowcol(b,d,A_inv)
% returns the inverse matrix of K = [A b;b' d] and the flops required
% http://www.squobble.com/academic/swkrls/node15.html

g_inv = d - b'*A_inv*b;
g = 1/g_inv;
f = -A_inv*b*g;
E = A_inv - A_inv*b*f';

K_inv = [E f;f' g];

% flops count
m = size(A_inv,1);
%fl_ginv = kaf_flops('sum',1+m*(m+1)/2) + kaf_flops('mult',m*(m+1)); % multiplication is symmetric
%fl_g = kaf_flops('div',1);
%fl_f = kaf_flops('mult',m,m,1) + kaf_flops('mult',m);
%fl_E = kaf_flops('sum',m*(m+1)/2) + kaf_flops('mult',m*(m+1)/2); % multiplication is only for f, and mxm matrices are symmetric
%fl = fl_ginv + fl_g + fl_f + fl_E;


function K_inv = prune_mat(K_inv,p)
% return the inverse of a matrix whose p'th row and column are removed,
% given the original reverse

ip = 1:size(K_inv,1);
ip(p) = [];		% remove p'th element
ip = [p ip];	% put p'th element in front
K_inv = inverse_remove1strowcol(K_inv(ip,ip));	% inverse of new kernel matrices

function D_inv = inverse_remove1strowcol(K_inv)
% calculates the inverse of D given K_inv = [a b';b D]^-1
% http://www.squobble.com/academic/swkrls/node16.html

m = size(K_inv,1);

G = K_inv(2:m,2:m);
f = K_inv(2:m,1);
e = K_inv(1,1);

D_inv = G - f*f'/e;

% flops count
%fl = kaf_flops('sum',m*(m+1)/2) + kaf_flops('mult',m*(m-1)/2) + kaf_flops('div',m-1); % G is symmetric
